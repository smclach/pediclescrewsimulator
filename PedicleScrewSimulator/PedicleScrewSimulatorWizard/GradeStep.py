from __main__ import qt, ctk, vtk, slicer

from PedicleScrewSimulatorStep import *
from Helper import *
import math
import struct
import csv
import os
import LoginStep as LoginStepModule
import ScreenCapture

class GradeStep(PedicleScrewSimulatorStep):
    
    def __init__( self, stepid ):
      self.initialize( stepid )
      self.setName( 'Grade' )
      self.setDescription( 'Grading Step' )
      self.fiduciallist = []
      self.__corticalMin = 350
      self.__corticalMax = 1500
      self.__cancellousMin = 100
      self.__cancellousMax = 350
      self.fiduciallist = []
      self.itemsqtcoP = []
      self.itemsqtcaP = []
      self.itemsqtotP = []
      self.pointsArray = []
      self.screwContact = []
      self.screwCount = 0
      self.screwChoice = []
      self.screwLength = []
      
      self.arrayColors = []
      
      self.savePath = None
      
      #self.greenTransform = None
      #self.redTransform = None
      #self.yellowTransform = None
      
      self.oldX = 0
      self.oldY = 0
      self.oldZ = 0
      

      self.__parent = super( GradeStep, self )
    
    def killButton(self):
      # hide useless button
      bl = slicer.util.findChildren(text='Final')
      if len(bl):
        bl[0].hide()

    def createUserInterface( self ):
      
      self.__layout = self.__parent.createUserInterface()  
      
      ln = slicer.util.getNode(pattern='vtkMRMLLayoutNode*')
      ln.SetViewArrangement(24)
      
      modLabel = qt.QLabel('Select Screw at Point:')
      '''
      self.__modelSelector = slicer.qMRMLNodeComboBox()
      self.__modelSelector.nodeTypes = ( ("vtkMRMLAnnotationFiducialNode"), "" )
      self.__modelSelector.addEnabled = False
      self.__modelSelector.removeEnabled = False
      self.__modelSelector.setMRMLScene( slicer.mrmlScene )
      self.__layout.addRow( modLabel, self.__modelSelector )
      '''
      # Paint Screw Button
      self.__selectScrewButton = qt.QPushButton("Grade Current Sizes")
      self.__layout.addWidget(self.__selectScrewButton)
      self.__selectScrewButton.connect('clicked(bool)', self.gradeScrews)
      self.__selectScrewButton.connect('clicked(bool)', self.saveResults)
	  
      self.warningText = qt.QLabel("Please do not select the following combinations: 45x45, 45x50, 75x25, 75x30.")
      self.__layout.addRow(self.warningText)
	  
      # Paint Screw Button
      #self.__gradeSizesButton = qt.QPushButton("Grade All Sizes")
      #self.__layout.addWidget(self.__gradeSizesButton)
      #self.__gradeSizesButton.connect('clicked(bool)', self.gradeScrewsLoop)
      #self.__selectScrewButton.connect('clicked(bool)', self.gradeScrews)
      #self.__selectScrewButton.connect('clicked(bool)', self.saveResults)
      
      '''
      #Opacity Slider
      self.transformSlider3 = ctk.ctkSliderWidget()
      self.transformSlider3.minimum = 0
      self.transformSlider3.maximum = 1
      self.transformSlider3.connect('valueChanged(double)', self.transformSlider3ValueChanged)
      self.__layout.addRow("Volume Opacity", self.transformSlider3)
      '''
      self.fiducial = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode4')
      self.fidNumber = self.fiducial.GetNumberOfFiducials()
      
      # Screw Table
      horizontalHeaders = ["Screw At","Entry Point\n Location","Trajectory","Screw\n Diameter (mm)","% Screw\n Length (mm)","Screw Size" ]
      self.screwTable = qt.QTableWidget(self.fidNumber, 6)
      self.screwTable.sortingEnabled = False
      self.screwTable.setEditTriggers(1)
      #self.screwTable.setMinimumHeight(self.screwTable.verticalHeader().length())
      self.screwTable.setMinimumHeight(500)
      try:
        self.screwTable.horizontalHeader().setResizeMode(qt.QHeaderView.Stretch) # < Qt 5.10
      except:
        self.screwTable.horizontalHeader().setSectionResizeMode(qt.QHeaderView.Stretch) # >= Qt 5.10
      self.screwTable.setSizePolicy (qt.QSizePolicy.MinimumExpanding, qt.QSizePolicy.Preferred)
      self.screwTable.itemSelectionChanged.connect(self.onTableCellClicked)
      self.__layout.addWidget(self.screwTable)

      self.screwTable.setHorizontalHeaderLabels(horizontalHeaders)
      
      
      
      self.updateWidgetFromParameters(self.parameterNode())
      qt.QTimer.singleShot(0, self.killButton)
      
      self.updateTable()
	
    #def transformSlider3ValueChanged(self, value):
    #    #print(value)
    #    self.vrUpdate(value)
                
    def onTableCellClicked(self):
      print "onTableCellClicked"
      lm = slicer.app.layoutManager()
      lm.setLayout(slicer.vtkMRMLLayoutNode.SlicerLayoutSideBySideView)

      if self.screwTable.currentColumn() == 0:
          #print self.screwTable.currentRow()
          self.currentFid = self.screwTable.currentRow()
          position = [0,0,0]
          self.fiducial = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode4')
          self.fiducial.GetNthFiducialPosition(self.currentFid,position)
          fidName = self.fidNode.GetNthFiducialLabel(self.currentFid)
          self.cameraFocus(position)
          self.sliceChange()

          collectionS = slicer.mrmlScene.GetNodesByName('Grade model-%s' % fidName)
          screwModel = collectionS.GetItemAsObject(0)
         
          modelDisplay = screwModel.GetDisplayNode()
          modelDisplay.SetColor(0.12,0.73,0.41)
      
      elif self.screwTable.currentColumn() == 1:
          self.screwChoice = []
          x = self.fidNode.GetNumberOfFiducials()
          for i in range(0,x):       
            #self.currentFid = self.screwTable.currentRow()
            width = self.screwTable.cellWidget(i,1).currentText
            length = self.screwTable.cellWidget(i,2).currentText
            screw = str(width) + "x" + str(length)
            self.screwChoice.append(screw)    
          print self.screwChoice
      elif self.screwTable.currentColumn() == 2:
          self.screwChoice = []
          x = self.fidNode.GetNumberOfFiducials()
          for i in range(0,x):       
            #self.currentFid = self.screwTable.currentRow()
            screw = str(self.widthCombo[i]) + "x" + str(self.lengthCombo[i])
            self.screwChoice.append(screw) 
          print self.screwChoice
                
      vr = slicer.mrmlScene.GetNodesByName('LabeledEntryPoints')
      c = vr.GetItemAsObject(0)
      screwLabel = c.GetNthFiducialLabel(self.screwTable.currentRow())
      
      volumeName = slicer.modules.PedicleScrewSimulatorWidget.loginStep.savedVolumeName
      saveImagePath = os.path.join(os.path.dirname(__file__), os.path.pardir, 'Output/Images/' + volumeName + '_' + screwLabel  + '.png')
      cap = ScreenCapture.ScreenCaptureLogic()      
      cap.showViewControllers(False)
      cap.captureImageFromView(None, saveImagePath)
      cap.showViewControllers(True)
 
                    
    def cameraFocus(self, position):  
      camera = slicer.mrmlScene.GetNodeByID('vtkMRMLCameraNode1')
      camera.SetFocalPoint(*position)
      #camera.SetPosition(position[0],-400,position[2])
      camera.SetPosition(position[0],position[1],position[2]+200)
      #camera.SetFocalPoint(*position)
      #camera.SetPosition(position[0],position[1],75)
      #camera.SetViewUp([0,0,1])
      camera.SetViewUp([0,1,0])    
      camera.ResetClippingRange()      
    
    def updateTable(self):
      self.itemsLoc = []
      self.itemsLen = []
      self.itemsSize = []
      self.itemsPos = []
      self.itemsTraj = []
      self.lengthCombo = []
      self.widthCombo = []
      
      
      self.screwList = slicer.modules.PedicleScrewSimulatorWidget.screwStep.screwList
      self.screwTrajectoryList = slicer.modules.PedicleScrewSimulatorWidget.screwStep.screwTrajectoryList
      self.screwNumber = len(self.screwList)
      self.screwTable.setRowCount(self.screwNumber)
      
      #print self.screwList
    
      for i in range(0,len(self.screwList)):
          currentScrew = self.screwList[i]
          screwLoc = str(currentScrew)
          currentTrajectory = self.screwTrajectoryList[i]
          trajectoryLoc = str(currentTrajectory)
          
          #screwLen = "6.5 x 45"
          
          self.lengthCombo.insert(i,qt.QComboBox())
          self.widthCombo.insert(i,qt.QComboBox())
          self.lengthCombo[i].addItem("25")
          self.lengthCombo[i].addItem("30")
          self.lengthCombo[i].addItem("35")
          self.lengthCombo[i].addItem("40")
          self.lengthCombo[i].addItem("45")
          self.lengthCombo[i].addItem("50")
          self.widthCombo[i].addItem("45")
          self.widthCombo[i].addItem("55")
          self.widthCombo[i].addItem("65")
          self.widthCombo[i].addItem("75")
          
          screwSize = str(self.widthCombo[i].currentText) + " x " + str(self.lengthCombo[i].currentText)
                              
          qtscrewLoc = qt.QTableWidgetItem(screwLoc)
          qtscrewTraj = qt.QTableWidgetItem(trajectoryLoc)
          qtscrewSize = qt.QTableWidgetItem(screwSize)
          
          self.itemsLoc.append(qtscrewLoc)
          self.itemsTraj.append(qtscrewTraj)
          self.itemsSize.append(qtscrewSize)
          
          position = [0,0,0]
          fiducial = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode4')
          fiducial.GetNthFiducialPosition(i,position)
          screwPos = str(round(position[0],1)) + ',' + str(round(position[1],1)) + ',' + str(round(position[2],1))
          qtscrewPos = qt.QTableWidgetItem(screwPos)
          self.itemsPos.append(qtscrewPos)
          
          self.screwTable.setItem(i, 0, qtscrewLoc)
          self.screwTable.setItem(i, 1, qtscrewPos)
          self.screwTable.setItem(i, 2, qtscrewTraj)
          self.screwTable.setCellWidget(i,3, self.widthCombo[i])
          self.screwTable.setCellWidget(i,4, self.lengthCombo[i])
          self.screwTable.setItem(i, 5, qtscrewSize)
     

    def replaceModels(self):
      self.screwChoice = []
      #self.screwLength = []
      x = self.fidNode.GetNumberOfFiducials()
      for i in range(0,x):       
        #self.currentFid = self.screwTable.currentRow()
        width = self.screwTable.cellWidget(i,3).currentText
        length = self.screwTable.cellWidget(i,4).currentText
        screw = str(width) + "x" + str(length)
        self.screwChoice.append(screw)
        qtscrewSize = qt.QTableWidgetItem(screw)
        self.screwTable.setItem(i, 5, qtscrewSize)
        if len(self.screwLength) < x:
            self.screwLength.append(int(length))
                   
      for i in range(self.fidNode.GetNumberOfFiducials()):
        fidName = self.fidNode.GetNthFiducialLabel(i)
        print fidName
        collectionT = slicer.mrmlScene.GetNodesByName('Transform-%s' % fidName)
        transformFid = collectionT.GetItemAsObject(0)
            
        collectionS = slicer.mrmlScene.GetNodesByName('probe model-%s' % fidName)
        probeModel = collectionS.GetItemAsObject(0)
        
        collectionS = slicer.mrmlScene.GetNodesByName('Screw model-%s' % fidName)
        screwModel = collectionS.GetItemAsObject(0)
        
        slicer.mrmlScene.RemoveNode(probeModel)
        slicer.mrmlScene.RemoveNode(screwModel)
        
        
        
        
        if self.screwChoice != []:
            print self.screwChoice[i]
            screwModelPath = os.path.join(os.path.dirname(__file__), os.path.pardir, 'Resources/ScrewModels/' + self.screwChoice[i]  + '.vtk')
            #screwModel2 = slicer.modules.models.logic().AddModel("C:\\Users\\Stewart\\Dropbox\\Research\\SB_2013_Slicer Pedicle Screw Sim\\Code\\Slicer 4_5\PedicleScrewSimulatorWizard_v4\\screws\\" +  + ".vtk")
            screwModel2 = slicer.modules.models.logic().AddModel(screwModelPath)
        else:
            screwModelPath = os.path.join(os.path.dirname(__file__), os.path.pardir, 'Resources/ScrewModels/65x50.vtk')
            #screwModel2 = slicer.modules.models.logic().AddModel("C:\\Users\\Stewart\\Dropbox\\Research\\SB_2013_Slicer Pedicle Screw Sim\\Code\\Slicer 4_5\PedicleScrewSimulatorWizard_v4\\screws\\allscrews\\65x50.vtk")
            screwModel2 = slicer.modules.models.logic().AddModel(screwModelPath)
        screwModel2.SetName('Screw model-%s' % fidName)
        screwModel2.SetAndObserveTransformNodeID(transformFid.GetID())
        
        matrixScrew = vtk.vtkMatrix4x4()
        matrixScrew = transformFid.GetMatrixTransformToParent()
    
        #newVal = 1 - ((50 - int(length))/50)
        length = self.screwTable.cellWidget(i,4).currentText
        
        if int(length) != self.screwLength[i]:
            newVal = self.screwLength[i] - int(length)
            print(newVal)
            self.screwLength[i] = int(length)
        
            drive1 = matrixScrew.GetElement(0,1)
            drive2 = matrixScrew.GetElement(1,1)
            drive3 = matrixScrew.GetElement(2,1)
        
            coord1 = drive1 * newVal + matrixScrew.GetElement(0,3)
            coord2 = drive2 * newVal + matrixScrew.GetElement(1,3)
            coord3 = drive3 * newVal + matrixScrew.GetElement(2,3)
        
            matrixScrew.SetElement(0,3,coord1)
            matrixScrew.SetElement(1,3,coord2)
            matrixScrew.SetElement(2,3,coord3)
            
            transformFid.SetAndObserveMatrixTransformToParent(matrixScrew)
            

        modelDisplay = screwModel2.GetDisplayNode()
        modelDisplay.SetColor(0.12,0.73,0.91)
        modelDisplay.SetDiffuse(0.90)
        modelDisplay.SetAmbient(0.10)
        modelDisplay.SetSpecular(0.20)
        modelDisplay.SetPower(10.0)
        modelDisplay.SetSliceIntersectionVisibility(True)
        screwModel2.SetAndObserveDisplayNodeID(modelDisplay.GetID())
        
        n =  self.__baselineVolume
        #n =  slicer.util.getNode('Sacrum-T5-CT')
        for color in ['Red', 'Yellow', 'Green']:
          a = slicer.app.layoutManager().sliceWidget(color).sliceLogic().GetSliceNode().GetFieldOfView()
          slicer.app.layoutManager().sliceWidget(color).sliceLogic().GetSliceCompositeNode().SetBackgroundVolumeID(n.GetID())
          #slicer.app.layoutManager().sliceWidget(color).sliceLogic().GetSliceNode().SetFieldOfView(a[0],a[1],a[2])
          slicer.app.layoutManager().sliceWidget(color).sliceLogic().GetSliceNode().SetFieldOfView(175,175,a[2])
        
        print self.screwLength
    
    def validate( self, desiredBranchId ):
      '''
      '''
      self.__parent.validate( desiredBranchId )
      self.__parent.validationSucceeded(desiredBranchId)
      
    def onEntry(self, comingFrom, transitionType):

      super(GradeStep, self).onEntry(comingFrom, transitionType)
      
      ln = slicer.util.getNode(pattern='vtkMRMLLayoutNode*')
      ln.SetViewArrangement(24)
      
      pNode = self.parameterNode()
      baselineVolume = Helper.getNodeByID(pNode.GetParameter('baselineVolumeID'))
      self.__baselineVolume = baselineVolume

      fidCollection = slicer.mrmlScene.GetNodesByClass('vtkMRMLAnnotationFiducialNode')
      fidCount = fidCollection.GetNumberOfItems()
      for i in range(0, fidCount):
          fidCollection.GetItemAsObject(i).GetAnnotationPointDisplayNode().SetOpacity(0.0)
          
      self.fidNode = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode4')
      self.fidNode.SetLocked(1)
      
      a = slicer.modules.markups.logic()
      b = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode1')
      a.SetAllMarkupsVisibility(b,0)

      lNode = slicer.mrmlScene.GetNodesByName('InsertionLandmarks')
      landmarks = lNode.GetItemAsObject(0)
      landmarks.SetDisplayVisibility(0)
      #self.fiducial = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode1')
      numberLandmarks = landmarks.GetNumberOfFiducials()
                
      markup = slicer.modules.markups.logic()
      markup.AddNewFiducialNode('LabeledEntryPoints')
      
      vr = slicer.mrmlScene.GetNodesByName('LabeledEntryPoints')
      c = vr.GetItemAsObject(0)
      dNode = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsDisplayNode5')
      dNode.SetSelectedColor(0, 0.2, 1)
      dNode.SetGlyphScale(3)
      dNode.SetTextScale(3)
       
      

      for x in range (0,self.fidNode.GetNumberOfFiducials()):
        #print x
        position = [0.0,0.0,0.0]
        labels = ['Left-L1','Right-L1','Left-L2','Right-L2','Left-L3','Right-L3','Left-L4','Right-L4','Left-L5','Right-L5']
        label = self.fidNode.GetNthFiducialLabel(x)
        self.fidNode.GetNthFiducialPosition(x,position)
        print position
        slicer.modules.markups.logic().AddFiducial(position[0],position[1],position[2])
        c.SetNthFiducialLabel(x,labels[x])
        
        #level = slicer.modules.PedicleScrewSimulator_v3Widget.landmarksStep.table2.cellWidget(x,1).currentText
        #side = slicer.modules.PedicleScrewSimulator_v3Widget.landmarksStep.table2.cellWidget(x,2).currentText
        self.fiduciallist.append(label)
        #print self.fiduciallist    
      '''
      slicer.mrmlScene.GetNodesByName('Left').GetItemAsObject(0).SetDisplayVisibility(0)
      slicer.mrmlScene.GetNodesByName('Right').GetItemAsObject(0).SetDisplayVisibility(0)
      slicer.mrmlScene.GetNodesByName('Top').GetItemAsObject(0).SetDisplayVisibility(0)
      slicer.mrmlScene.GetNodesByName('Bottom').GetItemAsObject(0).SetDisplayVisibility(0)
      slicer.mrmlScene.GetNodesByName('Front').GetItemAsObject(0).SetDisplayVisibility(0)
      '''
      #slicer.mrmlScene.GetNodesByName('Sacrum-T5-Model2').GetItemAsObject(0).SetDisplayVisibility(1)
      slicer.mrmlScene.GetNodesByName('Model').GetItemAsObject(0).SetDisplayVisibility(0)
      
      slicer.mrmlScene.GetNodesByName('GPURayCastVolumeRendering').GetItemAsObject(0).SetVisibility(0)
      
      '''
      # update the label volume accordingly
      thresh = vtk.vtkImageThreshold()
      thresh.SetInputDataObject(Helper.getNodeByID(pNode.GetParameter('croppedBaselineVolumeID')))
      thresh.ThresholdBetween(150, 1500)
      thresh.SetInValue(10)
      thresh.SetOutValue(0)
      thresh.ReplaceOutOn()
      thresh.ReplaceInOn()
      thresh.Update()
      '''
      #self.vrUpdate(0.03)
      #print "update slice view"
      slicer.app.applicationLogic().PropagateVolumeSelection(1) 
      
      n =  self.__baselineVolume
      #n =  slicer.util.getNode('Sacrum-T5-CT')
      #n =  slicer.util.getNode('CTA-cardio')
      for color in ['Red', 'Yellow', 'Green']:
          a = slicer.app.layoutManager().sliceWidget(color).sliceLogic().GetSliceNode().GetFieldOfView()
          slicer.app.layoutManager().sliceWidget(color).sliceLogic().GetSliceCompositeNode().SetBackgroundVolumeID(n.GetID())
          #slicer.app.layoutManager().sliceWidget(color).sliceLogic().GetSliceNode().SetFieldOfView(a[0],a[1],a[2])
          slicer.app.layoutManager().sliceWidget(color).sliceLogic().GetSliceNode().SetFieldOfView(175,175,a[2])
      #n.GetScalarVolumeDisplayNode().SetAutoWindowLevel(0)
      #n.GetScalarVolumeDisplayNode().SetWindow(1000)
      #n.GetScalarVolumeDisplayNode().SetLevel(400)
      #slicer.app.applicationLogic().PropagateVolumeSelection(1)
      
      self.redTransform = vtk.vtkTransform()
      viewSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeRed')
      self.redTransform.SetMatrix(viewSlice.GetSliceToRAS())
      #print "Red Transform"
      #print self.redTransform
      self.yellowTransform = vtk.vtkTransform()
      viewSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeYellow')
      self.yellowTransform.SetMatrix(viewSlice.GetSliceToRAS())
      #print "Yellow Transform"
      #print self.yellowTransform
      self.greenTransform = vtk.vtkTransform()
      viewSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeGreen')
      self.greenTransform.SetMatrix(viewSlice.GetSliceToRAS())
      #print "Green Transform"
      #print self.greenTransform
      '''
      a = slicer.mrmlScene.GetNodeByID('vtkMRMLModelNode6')
      b = a.GetDisplayNode()
      b.SetClipping(1)
      clip = slicer.util.getNode(pattern='vtkMRMLClipModelsNodevtkMRMLClipModelsNode')
      #clip.SetRedSliceClipState(0)
      clip.SetRedSliceClipState(2)
      '''
      x = self.fidNode.GetNumberOfFiducials()
      for i in range(0,x):       
        self.screwChoice.append("65x500")
        self.screwTable.cellWidget(i,3).setCurrentIndex(2)
        self.screwTable.cellWidget(i,4).setCurrentIndex(5) 
      
      
      #self.screwTable.cellWidget(1,1).setCurrentIndex(2)
      #self.screwTable.cellWidget(1,2).setCurrentIndex(4)
      
      self.replaceModels()
      
      # Enable Slice Intersections
      viewNodes = slicer.mrmlScene.GetNodesByClass('vtkMRMLSliceCompositeNode')
      viewNodes.UnRegister(slicer.mrmlScene)
      viewNodes.InitTraversal()
      viewNode = viewNodes.GetNextItemAsObject()
      while viewNode:
          viewNode.SetSliceIntersectionVisibility(1)
          viewNode = viewNodes.GetNextItemAsObject()
                      
      qt.QTimer.singleShot(0, self.killButton)
      self.loadEntrySize()
      self.replaceModels()
      
    def loadEntrySize(self):
      if slicer.modules.PedicleScrewSimulatorWidget.loginStep.savedWorkflow == 1 and slicer.modules.PedicleScrewSimulatorWidget.loginStep.savedEntryPointScrewSizes:
          screwSizes = slicer.modules.PedicleScrewSimulatorWidget.loginStep.savedEntryPointScrewSizes
          i, self.screwChoice = 0, []
          for screwSize in screwSizes:
              size = screwSize.split("x")
              self.screwTable.cellWidget(i,3).setCurrentIndex(self.screwTable.cellWidget(i,3).findText(size[0]))
              self.screwTable.cellWidget(i,4).setCurrentIndex(self.screwTable.cellWidget(i,4).findText(size[1]))
              screw = str(size[0]) + "x" + str(size[1])
              self.screwChoice.append(screw)
              qtscrewSize = qt.QTableWidgetItem(screw)
              self.screwTable.setItem(i, 5, qtscrewSize)
              i = i + 1
      
    def onExit(self, goingTo, transitionType):

      login = LoginStepModule.LoginStep(PedicleScrewSimulatorStep)
      filePath = login.saveFilePath
      if filePath != '':
        login.transposeSaveFile(filePath)
        login.addToSaveFile(filePath,self.screwChoice,7)
        login.transposeSaveFile(filePath)

      if goingTo.id() == 'Screw':
          self.clearGrade()
          fidCollection = slicer.mrmlScene.GetNodesByClass('vtkMRMLAnnotationFiducialNode')
          fidCount = fidCollection.GetNumberOfItems()
          for i in range(0, fidCount):
            fidCollection.GetItemAsObject(i).GetAnnotationPointDisplayNode().SetOpacity(1.0)
      
      self.fidNode.SetLocked(0)
      
      self.vrUpdate(1.0)
      
      super(GradeStep, self).onExit(goingTo, transitionType)
      
    def updateWidgetFromParameters(self, pNode):
        '''
        self.__corticalMin = float(pNode.GetParameter('corticalMin'))
        self.__corticalMax = float(pNode.GetParameter('corticalMax'))
        self.__cancellousMin = float(pNode.GetParameter('cancellousMin'))
        self.__cancellousMax = float(pNode.GetParameter('cancellousMax'))
        '''
        
        
    def doStepProcessing(self):
        print('Done')
        
           
    def sliceChange(self):
        
        '''
        Dumping ground for this potential DRR code
        sliceNode = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeRed')
        appLogic = slicer.app.applicationLogic()
        sliceLogic = appLogic.GetSliceLogic(sliceNode)
        sliceLayerLogic = sliceLogic.GetBackgroundLayer()
        reslice = sliceLayerLogic.GetReslice()
        reslice.SetSlabModeToMax()
        reslice.SetSlabModeToSum()
        reslice.SetSlabNumberOfSlices(600) # use a large number of slices (600) to cover the entire volume
        reslice.SetSlabSliceSpacingFraction(0.5) # spacing between slices are 0.5 pixel (supersampling is useful to reduce interpolation artifacts)
        sliceNode.Modified()
        '''
        
        coords = [0,0,0]
        self.fiducial.GetNthFiducialPosition(self.currentFid,coords)
        
        lm = slicer.app.layoutManager()
        redWidget = lm.sliceWidget('Red')
        redController = redWidget.sliceController()
        
        yellowWidget = lm.sliceWidget('Yellow')
        yellowController = yellowWidget.sliceController()
        
        greenWidget = lm.sliceWidget('Green')
        greenController = greenWidget.sliceController()
        
        yellowController.setSliceOffsetValue(coords[0])
        greenController.setSliceOffsetValue(coords[1])
        redController.setSliceOffsetValue(coords[2])
        
        
        fidName = self.fiducial.GetNthFiducialLabel(self.currentFid)
        #self.fiducial.GetNthFiducialPosition(self.currentFidIndex,self.coords)
        collectionT = slicer.mrmlScene.GetNodesByName('Transform-%s' % fidName)
        screwTransform = collectionT.GetItemAsObject(0)
        
        matrixScrew = vtk.vtkMatrix4x4()
        matrixScrew = screwTransform.GetMatrixTransformToParent()
        
        x = math.atan2(matrixScrew.GetElement(2,1), matrixScrew.GetElement(2,2))
        y = math.atan2(-matrixScrew.GetElement(2,0), math.sqrt((matrixScrew.GetElement(2,1) * matrixScrew.GetElement(2,1)) + (matrixScrew.GetElement(2,2) * matrixScrew.GetElement(2,2))))
        z = math.atan2(matrixScrew.GetElement(1,0), matrixScrew.GetElement(0,0))
        
        
      		
        # The above ranges will be between pi.
        # Convert them to degrees.
      		
        x = 180 - x * (180.0 / math.pi) # -pi to +pi.
        y = y * (180.0 / math.pi) # -(pi / 2.0) to +(pi / 2.0).
        z = z * (180.0 / math.pi) # -pi to +pi.
        
        print (self.oldX,self.oldY,self.oldZ)
        print (x,y,z)
        
        for i in range(0,3):
            transform = vtk.vtkTransform()
            transform2 = vtk.vtkMatrix4x4()
            #transform2 = screwTransform.GetMatrixTransformToParent()
                     
            #print "Fiducial Transform"
            #print transform
            if i == 0:
                viewSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeRed')
                transform.SetMatrix(viewSlice.GetSliceToRAS())
                transform.RotateX(-x + self.oldX)
                transform2.DeepCopy(transform.GetMatrix())
                transform2.SetElement(0,3,coords[0])
                transform2.SetElement(1,3,coords[1])
                transform2.SetElement(2,3,coords[2])
                transform.SetMatrix(transform2)

            elif i == 1:
                viewSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeYellow')
                transform.SetMatrix(viewSlice.GetSliceToRAS())
                transform.RotateY(-z + self.oldZ)
                transform2.DeepCopy(transform.GetMatrix())
                transform2.SetElement(0,3,coords[0])
                transform2.SetElement(1,3,coords[1])
                transform2.SetElement(2,3,coords[2])
                transform.SetMatrix(transform2)
                
            elif i == 2:
                viewSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeGreen')
                transform.SetMatrix(viewSlice.GetSliceToRAS())
                transform.RotateY(z - self.oldZ)
                transform.RotateX(x - self.oldX)
                transform2.DeepCopy(transform.GetMatrix())
                transform2.SetElement(0,3,coords[0])
                transform2.SetElement(1,3,coords[1])
                transform2.SetElement(2,3,coords[2])
                transform.SetMatrix(transform2)
                
            #print "Slice Transform" 
            #print viewSlice.GetSliceToRAS()
            
            #transform.Translate(value - self.oldPosition)
            viewSlice.GetSliceToRAS().DeepCopy(transform.GetMatrix())
            viewSlice.UpdateMatrices()
            
        self.oldX = x
        self.oldY = y
        self.oldZ = z
        
        n =  self.__baselineVolume
        #n =  slicer.util.getNode('Sacrum-T5-CT')
        for color in ['Red', 'Yellow', 'Green']:
          a = slicer.app.layoutManager().sliceWidget(color).sliceLogic().GetSliceNode().GetFieldOfView()
          slicer.app.layoutManager().sliceWidget(color).sliceLogic().GetSliceCompositeNode().SetBackgroundVolumeID(n.GetID())
          #slicer.app.layoutManager().sliceWidget(color).sliceLogic().GetSliceNode().SetFieldOfView(a[0],a[1],a[2])
          slicer.app.layoutManager().sliceWidget(color).sliceLogic().GetSliceNode().SetFieldOfView(150,150,a[2])
        
        #sliceMatrix = viewSlice.GetSliceToRAS() #sliceMatrix = vtk.vtkMatrix4x4()
        #newMatrix = vtk.vtkMatrix3x3()
        #oldMatrix = vtk.vtkMatrix3x3()
        #outputMatrix = vtk.vtkMatrix3x3()
        '''
        newMatrix.SetElement(0,0,matrixScrew.GetElement(0,0))
        newMatrix.SetElement(0,1,matrixScrew.GetElement(0,1))
        newMatrix.SetElement(0,2,matrixScrew.GetElement(0,2))
        
        newMatrix.SetElement(1,0,matrixScrew.GetElement(1,0))
        newMatrix.SetElement(1,1,matrixScrew.GetElement(1,1))
        newMatrix.SetElement(1,2,matrixScrew.GetElement(1,2))
        
        newMatrix.SetElement(2,0,matrixScrew.GetElement(2,0))
        newMatrix.SetElement(2,1,matrixScrew.GetElement(2,1))
        newMatrix.SetElement(2,2,matrixScrew.GetElement(2,2))
        
        oldMatrix.SetElement(0,0,sliceMatrix.GetElement(0,0))
        oldMatrix.SetElement(0,1,sliceMatrix.GetElement(0,1))
        oldMatrix.SetElement(0,2,sliceMatrix.GetElement(0,2))
        
        oldMatrix.SetElement(1,0,sliceMatrix.GetElement(1,0))
        oldMatrix.SetElement(1,1,sliceMatrix.GetElement(1,1))
        oldMatrix.SetElement(1,2,sliceMatrix.GetElement(1,2))
        
        oldMatrix.SetElement(2,0,sliceMatrix.GetElement(2,0))
        oldMatrix.SetElement(2,1,sliceMatrix.GetElement(2,1))
        oldMatrix.SetElement(2,2,sliceMatrix.GetElement(2,2))
        
        newMatrix.Invert()
        '''
        

        
        #vtk.vtkMatrix3x3.Multiply3x3(newMatrix, oldMatrix, outputMatrix)
        #outputMatrix = vtk.vtkMatrix3x3()
        '''
        matrixScrew.SetElement(0,0,outputMatrix.GetElement(0,0))
        matrixScrew.SetElement(0,1,outputMatrix.GetElement(0,1))
        matrixScrew.SetElement(0,2,outputMatrix.GetElement(0,2))
        matrixScrew.SetElement(0,3,coords[0])
        
        matrixScrew.SetElement(1,0,outputMatrix.GetElement(1,0))
        matrixScrew.SetElement(1,1,outputMatrix.GetElement(1,1))
        matrixScrew.SetElement(1,2,outputMatrix.GetElement(1,2))
        matrixScrew.SetElement(1,3,coords[1])
        
        matrixScrew.SetElement(2,0,outputMatrix.GetElement(2,0))
        matrixScrew.SetElement(2,1,outputMatrix.GetElement(2,1))
        matrixScrew.SetElement(2,2,outputMatrix.GetElement(2,2))
        matrixScrew.SetElement(2,3,coords[2])
        '''
            
            
        '''
        yellowSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeYellow')
        #transform.SetMatrix(redSlice.GetSliceToRAS())
        #transform.RotateX(value - self.oldPosition)
        #transform.Translate(value - self.oldPosition)
        print "Yellow Transform" 
        print yellowSlice.GetSliceToRAS()
        yellowSlice.GetSliceToRAS().DeepCopy(transform.GetMatrix())
        yellowSlice.UpdateMatrices()
        
        greenSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeGreen')
        #transform.SetMatrix(redSlice.GetSliceToRAS())
        #transform.RotateX(value - self.oldPosition)
        #transform.Translate(value - self.oldPosition)
        print "Green Transform" 
        print greenSlice.GetSliceToRAS()
        greenSlice.GetSliceToRAS().DeepCopy(transform.GetMatrix())
        greenSlice.UpdateMatrices()
        '''
    
    def gradeScrewsLoop(self):
        diameters = [45,55,65,75]
        lengthShort = [25,30,35,40]
        lengthMed = [25,30,35,40,45,50]
        lengthLong = [35,40,45,50]
        for d in diameters:
            
            if d == 45:
                index = 0
                for l in lengthShort:
                    x = self.fidNode.GetNumberOfFiducials()
                    for i in range(0,x): 
                        self.screwTable.cellWidget(i,3).setCurrentIndex(0)
                        self.screwTable.cellWidget(i,4).setCurrentIndex(index)
                    self.gradeScrews()
                    self.saveResults()
                    index += 1
            elif d == 55:
                index = 0
                for l in lengthMed:
                    x = self.fidNode.GetNumberOfFiducials()
                    for i in range(0,x): 
                        self.screwTable.cellWidget(i,3).setCurrentIndex(1)
                        self.screwTable.cellWidget(i,4).setCurrentIndex(index)
                    self.gradeScrews()
                    self.saveResults()
                    index += 1
            elif d == 65:
                index = 0
                for l in lengthMed:
                    x = self.fidNode.GetNumberOfFiducials()
                    for i in range(0,x): 
                        self.screwTable.cellWidget(i,3).setCurrentIndex(2)
                        self.screwTable.cellWidget(i,4).setCurrentIndex(index)
                    self.gradeScrews()
                    self.saveResults()
                    index += 1
            elif d == 75:
                index = 2
                for l in lengthLong:
                    x = self.fidNode.GetNumberOfFiducials()
                    for i in range(0,x): 
                        self.screwTable.cellWidget(i,3).setCurrentIndex(3)
                        self.screwTable.cellWidget(i,4).setCurrentIndex(index)
                    self.gradeScrews()
                    self.saveResults()
                    index += 1
            
                    
    
    def gradeScrews(self):
        #print self.fiduciallist[0]
        #self.fid = self.__modelSelector.currentNode()
        #self.sliceChange()
        
        self.replaceModels()
        
        pNode = self.parameterNode()
        
        #self.__inputScalarVol = Helper.getNodeByID(pNode.GetParameter('croppedBaselineVolumeID'))
        self.__inputScalarVol = Helper.getNodeByID(pNode.GetParameter('baselineVolumeID'))
        #print self.__inputScalarVol
        #print self.screwList
        #print self.fiduciallist
        for x in range(0, len(self.screwList)):
            fidName = self.fiduciallist[x]
            print "current screw" + str(fidName)
            collectionT = slicer.mrmlScene.GetNodesByName('Transform-%s' % fidName)
            transformFid = collectionT.GetItemAsObject(0)
            
            collectionS = slicer.mrmlScene.GetNodesByName('Screw model-%s' % fidName)
            screwModel = collectionS.GetItemAsObject(0)

            collectionG = slicer.mrmlScene.GetNodesByName('Grade model-%s' % fidName)
            gradeModel = collectionG.GetItemAsObject(0)

            screwIndex = x
            
            if screwModel != None and gradeModel == None:
                self.gradeScrew(screwModel, transformFid, fidName, screwIndex)
                #self.screwCount += 1
                #print "yes screw"
            elif screwModel != None and gradeModel != None:
                self.clearGrade()
                self.gradeScrew(screwModel, transformFid, fidName, screwIndex)
                #self.screwCount += 1
            else:
                print "no screw"
                return
            '''
            
            '''
           
        self.chartContact(self.screwNumber)
        
    #Crops out head of the screw    
    def cropScrew(self,input, area):
        #Get bounds of screw 
        bounds = input.GetPolyData().GetBounds()
        #print bounds
        
        #Define bounds for cropping out the head or shaft of the screw respectively
        if area == 'head':
            i = bounds[2]
            j = bounds[3]-16.5
        elif area == 'shaft':
            i = bounds[3]-16.5
            j = bounds[3]

        #Create a box with bounds equal to that of the screw minus the head (-17)
        cropBox = vtk.vtkBox()
        cropBox.SetBounds(bounds[0],bounds[1],i,j,bounds[4],bounds[5])
        
        #Crop out head of screw
        extract = vtk.vtkExtractPolyDataGeometry()
        extract.SetImplicitFunction(cropBox)
        extract.SetInputData(input.GetPolyData())
        extract.Update()
        
        #PolyData of cropped screw
        output = extract.GetOutput()
        return output
    
    #Select all points in the shaft of the screw for grading
    def cropPoints(self, input):
        #Get bounds of screw 
        bounds = input.GetPolyData().GetBounds()
        
        #Create a cube with bounds equal to that of the screw minus the head (-17)
        cropCube = vtk.vtkCubeSource()
        cropCube.SetBounds(bounds[0],bounds[1],bounds[2],bounds[3]-16.5,bounds[4],bounds[5])
        
        #Select points on screw within cube
        select = vtk.vtkSelectEnclosedPoints()
        select.SetInputData(input.GetPolyData())
        #select.SetSurface(cropCube.GetOutput())
        #select.SetSurfaceData(cropCube.GetOutput())
        select.SetSurfaceConnection(cropCube.GetOutputPort())
        select.Update()
        
        return select
        
        
    def gradeScrew(self, screwModel, transformFid, fidName, screwIndex):
        #Reset screws
        
        
        #Crop out head of screw
        croppedScrew = self.cropScrew(screwModel, 'head')
        
        #Clone screw model poly data
        inputModel = slicer.vtkMRMLModelNode()
        inputModel.SetAndObservePolyData(croppedScrew)
        inputModel.SetAndObserveTransformNodeID(transformFid.GetID())
        slicer.mrmlScene.AddNode(inputModel)
        slicer.vtkSlicerTransformLogic.hardenTransform(inputModel)
        
        #Create new model for output    
        output = slicer.vtkMRMLModelNode()
        output.SetName('Grade model-%s' % fidName)
        slicer.mrmlScene.AddNode(output)
        #output.SetSliceIntersectionVisibility(True)
                  
        #Parameters for ProbeVolumeWithModel
        parameters = {}
        parameters["InputVolume"] = self.__inputScalarVol.GetID()
        parameters["InputModel"] = inputModel.GetID()
        parameters["OutputModel"] = output.GetID()
        
        probe = slicer.modules.probevolumewithmodel
        slicer.cli.run(probe, None, parameters, wait_for_completion=True)
        
        outputDisplayNode = output.GetDisplayNodeID()
        outputDisplay = slicer.mrmlScene.GetNodeByID(outputDisplayNode)
        outputDisplay.SetSliceIntersectionVisibility(True)
        
        
        #Hide original screw
        modelDisplay = screwModel.GetDisplayNode()
        modelDisplay.SetColor(0,1,0)
        modelDisplay.VisibilityOff()
        
        #Highlight screwh head
        headModel = slicer.vtkMRMLModelNode()
        headModel.SetName('Head %s' % fidName)
        headModel.SetAndObservePolyData(self.cropScrew(screwModel, 'shaft'))
        headModel.SetAndObserveTransformNodeID(transformFid.GetID())
        slicer.mrmlScene.AddNode(headModel)
        
        headDisplay = slicer.vtkMRMLModelDisplayNode()
        colorNode = slicer.util.getNode('Labels')
        #headDisplay.SetAndObserveColorNodeID(colorNode.GetID())
        '''
        d = [0,0,0]
        if screwIndex == 0:
            #cn.SetProperty(fidName, 'color', '#')
            b = 'd4252b'
            c = struct.unpack('BBB',b.decode('hex'))
            d[0] = c[0] / 255.0
            d[1] = c[1] / 255.0
            d[2] = c[2] / 255.0
            headDisplay.SetColor(c)
        elif screwIndex == 1:
            #cn.SetProperty(fidName, 'color', '#')
            b = '0065af'
            c = struct.unpack('BBB',b.decode('hex'))
            d[0] = c[0] / 255.0
            d[1] = c[1] / 255.0
            d[2] = c[2] / 255.0
            headDisplay.SetColor(c)
        elif screwIndex == 2:
            #cn.SetProperty(fidName, 'color', '#')
            b = 'e6dc46'
            c = struct.unpack('BBB',b.decode('hex'))
            d[0] = c[0] / 255.0
            d[1] = c[1] / 255.0
            d[2] = c[2] / 255.0
            headDisplay.SetColor(c)
        elif screwIndex == 3:
            cn.SetProperty(fidName, 'color', '#')
            b = '8c3186'
            c = struct.unpack('BBB',b.decode('hex'))
            d[0] = c[0] / 255.0
            d[1] = c[1] / 255.0
            d[2] = c[2] / 255.0
            headDisplay.SetColor(c)
        elif screwIndex == 4:
            #cn.SetProperty(fidName, 'color', '#')
            b = '339e33'
            c = struct.unpack('BBB',b.decode('hex'))
            d[0] = c[0] / 255.0
            d[1] = c[1] / 255.0
            d[2] = c[2] / 255.0
            headDisplay.SetColor(c)
        elif screwIndex == 5:
            #cn.SetProperty(fidName, 'color', '#')
            b = 'fe7d20'
            c = struct.unpack('BBB',b.decode('hex'))
            d[0] = c[0] / 255.0
            d[1] = c[1] / 255.0
            d[2] = c[2] / 255.0
            headDisplay.SetColor(c)
        elif screwIndex == 6:
            #cn.SetProperty(fidName, 'color', '#')
            b = '27b7b2'
            c = struct.unpack('BBB',b.decode('hex'))
            d[0] = c[0] / 255.0
            d[1] = c[1] / 255.0
            d[2] = c[2] / 255.0
            headDisplay.SetColor(c)
        elif screwIndex == 7:
            #cn.SetProperty(fidName, 'color', '#')
            b = '3a3a8c'
            c = struct.unpack('BBB',b.decode('hex'))
            d[0] = c[0] / 255.0
            d[1] = c[1] / 255.0
            d[2] = c[2] / 255.0
            headDisplay.SetColor(c)
        else:
            #cn.SetProperty(fidName, 'color', '#')
            b = 'ff0000'
            c = struct.unpack('BBB',b.decode('hex'))
            d[0] = c[0] / 255.0
            d[1] = c[1] / 255.0
            d[2] = c[2] / 255.0
            headDisplay.SetColor(c)
        '''
        if screwIndex == 0:
            a = colorNode.GetLookupTable().GetTableValue(151)
            headDisplay.SetColor(a[0],a[1],a[2])
        elif screwIndex == 1:
            #headDisplay.SetColor(0,101,175)
            a = colorNode.GetLookupTable().GetTableValue(152)
            headDisplay.SetColor(a[0],a[1],a[2])
        elif screwIndex == 2:
            #headDisplay.SetColor(230,220,70)
            a = colorNode.GetLookupTable().GetTableValue(153)
            headDisplay.SetColor(a[0],a[1],a[2])
        elif screwIndex == 3:
            #headDisplay.SetColor(140,49,134)
            a = colorNode.GetLookupTable().GetTableValue(154)
            headDisplay.SetColor(a[0],a[1],a[2])
        elif screwIndex == 4:
            #headDisplay.SetColor(51,158,51)
            a = colorNode.GetLookupTable().GetTableValue(155)
            headDisplay.SetColor(a[0],a[1],a[2])
        elif screwIndex == 5:
            #headDisplay.SetColor(254,125,32)
            a = colorNode.GetLookupTable().GetTableValue(156)
            headDisplay.SetColor(a[0],a[1],a[2])
        else:
            #headDisplay.SetColor(0,0,1)
            a = colorNode.GetLookupTable().GetTableValue(157)
            headDisplay.SetColor(a[0],a[1],a[2])
          
            
        slicer.mrmlScene.AddNode(headDisplay)
        headModel.SetAndObserveDisplayNodeID(headDisplay.GetID())
        headDisplay.SetSliceIntersectionVisibility(True)

        #Remove clone    
        slicer.mrmlScene.RemoveNode(inputModel)
        
        labelsColorNode = slicer.modules.colors.logic().GetColorTableNodeID(35)  # Color Warm Tint 3
        output.GetDisplayNode().SetAndObserveColorNodeID(labelsColorNode)
        polyData = output.GetPolyData()
        numberOfPoints = polyData.GetNumberOfPoints()
        scalars = vtk.vtkFloatArray()
        scalars = polyData.GetPointData().GetScalars( "NRRDImage" )
        ranger = [0] * 2 
        numberOfComponents = scalars.GetNumberOfComponents()
        for i in xrange ( numberOfComponents ):
            scalars.GetRange(ranger, i)
        #lowerThreshold = self.range[0]
        #upperThreshold = self.range[1]
        lowerThreshold = -1000
        upperThreshold = 3000
        
        newScalars = vtk.vtkFloatArray()
        newScalars.SetNumberOfComponents( scalars.GetNumberOfComponents() )
        newScalars.SetNumberOfValues ( numberOfPoints )
        newScalars.SetName( "NewScalars" + str(screwIndex) )
        # Set values of new scalars according to threshold
        for i in xrange( numberOfPoints ):
            value = scalars.GetValue( i )
            if value < lowerThreshold:
                newScalars.SetValue( i, 0 )				# might have to check this 0
            elif value > upperThreshold:
                newScalars.SetValue( i, 3000 )
            else:
                newScalars.SetValue( i, value )			# might have to check this (value - 0)
        # Update the polydata with the new scalars
        polyData.GetPointData().AddArray( newScalars )
        polyData.GetPointData().RemoveArray( "NRRDImage" )
        polyData.Modified()
        output.SetAndObservePolyData( polyData )
        output.GetDisplayNode().SetScalarVisibility( 1 )
        output.GetDisplayNode().SetActiveScalarName( "NewScalars" + str(screwIndex) )
        
        #Grade and chart screw
   
        #Get points in shaft of screw
        #insidePoints = self.cropPoints(screwModel)
        #print insidePoints
        
        #Get scalars to array
        #scalarsArray = input.GetPolyData().GetPointData().GetScalars("NRRDImage")
        #scalarsArray = self.polyData.GetPointData().GetScalars("NewScalars")
        #self.pointsArray = screwModel.GetPolyData()
        self.pointsArray = output.GetPolyData()
        scalarsArray = self.pointsArray.GetPointData().GetScalars("NewScalars" + str(screwIndex))
        numberOfPoints = self.pointsArray.GetNumberOfPoints()
        #Get total number of tuples/points
        #value = scalarsArray.GetNumberOfTuples()
        #print value
        
        #modelTransform = transformFid.GetID()
        matrixScrew = vtk.vtkMatrix4x4()
        matrixScrew = transformFid.GetMatrixTransformToParent()
        #print "Matrix Screw"
        #print matrixScrew
        matrixScrew.Invert()
        #print "matrix Screw inverted"
        #print matrixScrew
        
        
        
        #Reset variables
        point = [0]
        point2 = [0,0,0]
        corticalCount = 0
        cancellousCount = 0
        totalCount = 0
        bounds = [0]
        shaftBounds = 0
        count00 = 0
        points00 = 0
        avg00 = 0.0
        count10 = 0
        points10 = 0
        avg10 = 0.0
        count20 = 0
        points20 = 0
        avg20 = 0.0
        count30 = 0
        points30 = 0
        avg30 = 0.0
        count40 = 0
        points40 = 0
        avg40 = 0.0 
        count50 = 0
        points50 = 0
        avg50 = 0.0 
        count60 = 0
        points60 = 0
        avg60 = 0.0 
        count70 = 0
        points70 = 0
        avg70 = 0.0 
        count80 = 0
        points80 = 0
        avg80 = 0.0 
        count90 = 0
        points90 = 0
        avg90 = 0.0
        avgTotal = []
        sumTotal = []
        avgQuad1 = []
        avgQuad2 = []
        avgQuad3 = []
        avgQuad4 = []
        countQ1_00 = 0.0
        pointsQ1_00 = 0.0
        countQ2_00 = 0.0
        pointsQ2_00 = 0.0
        countQ3_00 = 0.0
        pointsQ3_00 = 0.0
        countQ4_00 = 0.0
        pointsQ4_00 = 0.0
        countQ1_10 = 0.0
        pointsQ1_10 = 0.0
        countQ2_10 = 0.0
        pointsQ2_10 = 0.0
        countQ3_10 = 0.0
        pointsQ3_10 = 0.0
        countQ4_10 = 0.0
        pointsQ4_10 = 0.0
        countQ1_20 = 0.0
        pointsQ1_20 = 0.0
        countQ2_20 = 0.0
        pointsQ2_20 = 0.0
        countQ3_20 = 0.0
        pointsQ3_20 = 0.0
        countQ4_20 = 0.0
        pointsQ4_20 = 0.0
        countQ1_30 = 0.0
        pointsQ1_30 = 0.0
        countQ2_30 = 0.0
        pointsQ2_30 = 0.0
        countQ3_30 = 0.0
        pointsQ3_30 = 0.0
        countQ4_30 = 0.0
        pointsQ4_30 = 0.0
        countQ1_40 = 0.0
        pointsQ1_40 = 0.0
        countQ2_40 = 0.0
        pointsQ2_40 = 0.0
        countQ3_40 = 0.0
        pointsQ3_40 = 0.0
        countQ4_40 = 0.0
        pointsQ4_40 = 0.0
        countQ1_50 = 0.0
        pointsQ1_50 = 0.0
        countQ2_50 = 0.0
        pointsQ2_50 = 0.0
        countQ3_50 = 0.0
        pointsQ3_50 = 0.0
        countQ4_50 = 0.0
        pointsQ4_50 = 0.0
        countQ1_60 = 0.0
        pointsQ1_60 = 0.0
        countQ2_60 = 0.0
        pointsQ2_60 = 0.0
        countQ3_60 = 0.0
        pointsQ3_60 = 0.0
        countQ4_60 = 0.0
        pointsQ4_60 = 0.0
        countQ1_70 = 0.0
        pointsQ1_70 = 0.0
        countQ2_70 = 0.0
        pointsQ2_70 = 0.0
        countQ3_70 = 0.0
        pointsQ3_70 = 0.0
        countQ4_70 = 0.0
        pointsQ4_70 = 0.0
        countQ1_80 = 0.0
        pointsQ1_80 = 0.0
        countQ2_80 = 0.0
        pointsQ2_80 = 0.0
        countQ3_80 = 0.0
        pointsQ3_80 = 0.0
        countQ4_80 = 0.0
        pointsQ4_80 = 0.0
        countQ1_90 = 0.0
        pointsQ1_90 = 0.0
        countQ2_90 = 0.0
        pointsQ2_90 = 0.0
        countQ3_90 = 0.0
        pointsQ3_90 = 0.0
        countQ4_90 = 0.0
        pointsQ4_90 = 0.0
        
        
        xCenter = 0
        zCenter = 0         
        
        bounds = self.pointsArray.GetPoints().GetBounds()
        lowerBound = bounds[2]
        shaftBounds = bounds[3]-bounds[2] # FOR NOW
        #print bounds
        xCenter = (bounds[0] + bounds[1])/2
        zCenter = (bounds[4] + bounds[5])/2
        
        #For each point in the screw model...
        for i in range(0, numberOfPoints):
            #If the point is in the shaft of the screw...
            #print insidePoints.IsInside(i)
            #if insidePoints.IsInside(i) == 1:  
              totalCount += 1
              #Read scalar value at point to "point" array
              scalarsArray.GetTuple(i, point)
              
              self.pointsArray.GetPoints().GetPoint(i,point2)
              #print point2
              point2.append(1)
              matrixScrew.MultiplyPoint(point2,point2)
              #print point2
              point2.remove(1)
              lowerBound = 0
              xCenter = 0
              zCenter = 0 
              
              #print "Location: " + str(point2[1]) + ", HU: " + str(point)
              #if point2[1] >= lowerBound and point2[1] < (lowerBound + (shaftBounds*0.1)):
              if point2[1] >= lowerBound and point2[1] < (lowerBound + 5):
                  #print "00%"
                  count00 = count00 + point[0]
                  points00 += 1
                  #print xCenter
                  #print point2[0]
                  #print zCenter
                  #print point2[2]  
                  if point2[0] < xCenter and point2[2] >= zCenter:
                      #print "Quadrant 1"
                      countQ1_00 = countQ1_00 + point[0]
                      pointsQ1_00 += 1
                  elif point2[0] >= xCenter and point2[2] >= zCenter:
                      #print "Quadrant 2"
                      countQ2_00 = countQ2_00 + point[0]
                      pointsQ2_00 += 1
                  elif point2[0] < xCenter and point2[2] < zCenter:
                      #print "Quadrant 3"
                      countQ3_00 = countQ3_00 + point[0]
                      pointsQ3_00 += 1
                  else:
                      #print "Quadrant 4"
                      countQ4_00 = countQ4_00 + point[0]
                      pointsQ4_00 += 1    
              #elif point2[1] >= (lowerBound + (shaftBounds*0.1)) and point2[1] < (lowerBound + (shaftBounds*0.2)):
              elif point2[1] >= (lowerBound + 5) and point2[1] < (lowerBound + 10):  
                  #print "10%"
                  count10 = count10 + point[0]
                  points10 += 1
                  #print xCenter
                  #print point2[0]
                  #print zCenter
                  #print point2[2]    
                  if point2[0] < xCenter and point2[2] >= zCenter:
                      #print "Quadrant 1"
                      countQ1_10 = countQ1_10 + point[0]
                      pointsQ1_10 += 1
                  elif point2[0] >= xCenter and point2[2] >= zCenter:
                      #print "Quadrant 2"
                      countQ2_10 = countQ2_10 + point[0]
                      pointsQ2_10 += 1
                  elif point2[0] < xCenter and point2[2] < zCenter:
                      #print "Quadrant 3"
                      countQ3_10 = countQ3_10 + point[0]
                      pointsQ3_10 += 1
                  else:
                      #print "Quadrant 4"
                      countQ4_10 = countQ4_10 + point[0]
                      pointsQ4_10 += 1  
              #elif point2[1] >= (lowerBound + (shaftBounds*0.2)) and point2[1] < (lowerBound + (shaftBounds*0.3)):
              elif point2[1] >= (lowerBound + 10) and point2[1] < (lowerBound + 15):
                  #print "20%"
                  count20 = count20 + point[0]
                  points20 += 1
                  if point2[0] < xCenter and point2[2] >= zCenter:
                      #print "Quadrant 1"
                      countQ1_20 = countQ1_20 + point[0]
                      pointsQ1_20 += 1
                  elif point2[0] >= xCenter and point2[2] >= zCenter:
                      #print "Quadrant 2"
                      countQ2_20 = countQ2_20 + point[0]
                      pointsQ2_20 += 1
                  elif point2[0] < xCenter and point2[2] < zCenter:
                      #print "Quadrant 3"
                      countQ3_20 = countQ3_20 + point[0]
                      pointsQ3_20 += 1
                  else:
                      #print "Quadrant 4"
                      countQ4_20 = countQ4_20 + point[0]
                      pointsQ4_20 += 1      
              #elif point2[1] >= (lowerBound + (shaftBounds*0.3)) and point2[1] < (lowerBound + (shaftBounds*0.4)):
              elif point2[1] >= (lowerBound + 15) and point2[1] < (lowerBound + 20):
                  #print "30%"
                  count30 = count30 + point[0]
                  points30 += 1
                  if point2[0] < xCenter and point2[2] >= zCenter:
                      #print "Quadrant 1"
                      countQ1_30 = countQ1_30 + point[0]
                      pointsQ1_30 += 1
                  elif point2[0] >= xCenter and point2[2] >= zCenter:
                      #print "Quadrant 2"
                      countQ2_30 = countQ2_30 + point[0]
                      pointsQ2_30 += 1
                  elif point2[0] < xCenter and point2[2] < zCenter:
                      #print "Quadrant 3"
                      countQ3_30 = countQ3_30 + point[0]
                      pointsQ3_30 += 1
                  else:
                      #print "Quadrant 4"  
                      countQ4_30 = countQ4_30 + point[0]
                      pointsQ4_30 += 1    
              #elif point2[1] >= (lowerBound + (shaftBounds*0.4)) and point2[1] < (lowerBound + (shaftBounds*0.5)):
              elif point2[1] >= (lowerBound + 20) and point2[1] < (lowerBound + 25):
                  #print "40%"
                  count40 = count40 + point[0]
                  points40 += 1
                  if point2[0] < xCenter and point2[2] >= zCenter:
                      #print "Quadrant 1"
                      countQ1_40 = countQ1_40 + point[0]
                      pointsQ1_40 += 1
                  elif point2[0] >= xCenter and point2[2] >= zCenter:
                      #print "Quadrant 2"
                      countQ2_40 = countQ2_40 + point[0]
                      pointsQ2_40 += 1
                  elif point2[0] < xCenter and point2[2] < zCenter:
                      #print "Quadrant 3"
                      countQ3_40 = countQ3_40 + point[0]
                      pointsQ3_40 += 1
                  else:
                      #print "Quadrant 4"  
                      countQ4_40 = countQ4_40 + point[0]
                      pointsQ4_40 += 1    
              #elif point2[1] >= (lowerBound + (shaftBounds*0.5)) and point2[1] < (lowerBound + (shaftBounds*0.6)):
              elif point2[1] >= (lowerBound + 25) and point2[1] < (lowerBound + 30):
                  #print "50%"
                  count50 = count50 + point[0]
                  points50 += 1
                  if point2[0] < xCenter and point2[2] >= zCenter:
                      #print "Quadrant 1"
                      countQ1_50 = countQ1_50 + point[0]
                      pointsQ1_50 += 1 
                  elif point2[0] >= xCenter and point2[2] >= zCenter:
                      #print "Quadrant 2"
                      countQ2_50 = countQ2_50 + point[0]
                      pointsQ2_50 += 1
                  elif point2[0] < xCenter and point2[2] < zCenter:
                      #print "Quadrant 3"
                      countQ3_50 = countQ3_50 + point[0]
                      pointsQ3_50 += 1
                  else:
                      #print "Quadrant 4" 
                      countQ4_50 = countQ4_50 + point[0]
                      pointsQ4_50 += 1     
              #elif point2[1] >= (lowerBound + (shaftBounds*0.6)) and point2[1] < (lowerBound + (shaftBounds*0.7)):
              elif point2[1] >= (lowerBound + 30) and point2[1] < (lowerBound + 35):
                  #print "60%"
                  count60 = count60 + point[0]
                  points60 += 1
                  if point2[0] < xCenter and point2[2] >= zCenter:
                      #print "Quadrant 1"
                      countQ1_60 = countQ1_60 + point[0]
                      pointsQ1_60 += 1
                  elif point2[0] >= xCenter and point2[2] >= zCenter:
                      #print "Quadrant 2"
                      countQ2_60 = countQ2_60 + point[0]
                      pointsQ2_60 += 1
                  elif point2[0] < xCenter and point2[2] < zCenter:
                      #print "Quadrant 3"
                      countQ3_60 = countQ3_60 + point[0]
                      pointsQ3_60 += 1
                  else:
                      #print "Quadrant 4"  
                      countQ4_60 = countQ4_60 + point[0]
                      pointsQ4_60 += 1    
              #elif point2[1] >= (lowerBound + (shaftBounds*0.7)) and point2[1] < (lowerBound + (shaftBounds*0.8)):
              elif point2[1] >= (lowerBound + 35) and point2[1] < (lowerBound + 40):
                  #print "70%"
                  count70 = count70 + point[0]
                  points70 += 1
                  if point2[0] < xCenter and point2[2] >= zCenter:
                      #print "Quadrant 1" 
                      countQ1_70 = countQ1_70 + point[0]
                      pointsQ1_70 += 1 
                  elif point2[0] >= xCenter and point2[2] >= zCenter:
                      #print "Quadrant 2"
                      countQ2_70 = countQ2_70 + point[0]
                      pointsQ2_70 += 1 
                  elif point2[0] < xCenter and point2[2] < zCenter:
                      #print "Quadrant 3"
                      countQ3_70 = countQ3_70 + point[0]
                      pointsQ3_70 += 1 
                  else:
                      #print "Quadrant 4"  
                      countQ4_70 = countQ4_70 + point[0]
                      pointsQ4_70 += 1     
              #elif point2[1] >= (lowerBound + (shaftBounds*0.8)) and point2[1] < (lowerBound + (shaftBounds*0.9)):
              elif point2[1] >= (lowerBound + 40) and point2[1] < (lowerBound + 45):
                  #print "80%"
                  count80 = count80 + point[0]
                  points80 += 1
                  if point2[0] < xCenter and point2[2] >= zCenter:
                      #print "Quadrant 1"
                      countQ1_80 = countQ1_80 + point[0]
                      pointsQ1_80 += 1
                  elif point2[0] >= xCenter and point2[2] >= zCenter:
                      #print "Quadrant 2"
                      countQ2_80 = countQ2_80 + point[0]
                      pointsQ2_80 += 1
                  elif point2[0] < xCenter and point2[2] < zCenter:
                      #print "Quadrant 3"
                      countQ3_80 = countQ3_80 + point[0]
                      pointsQ3_80 += 1
                  else:
                      #print "Quadrant 4" 
                      countQ4_80 = countQ4_80 + point[0]
                      pointsQ4_80 += 1     
              #elif point2[1] >= (lowerBound + (shaftBounds*0.9)) and point2[1] < (lowerBound + (shaftBounds*1)):
              elif point2[1] >= (lowerBound + 45) and point2[1] < (lowerBound + 50):
                  #print "90%"
                  count90 = count90 + point[0]
                  points90 += 1
                  if point2[0] < xCenter and point2[2] >= zCenter:
                      #print "Quadrant 1"
                      countQ1_90 = countQ1_90 + point[0]
                      pointsQ1_90 += 1
                  elif point2[0] >= xCenter and point2[2] >= zCenter:
                      #print "Quadrant 2"
                      countQ2_90 = countQ2_90 + point[0]
                      pointsQ2_90 += 1
                  elif point2[0] < xCenter and point2[2] < zCenter:
                      #print "Quadrant 3"
                      countQ3_90 = countQ3_90 + point[0]
                      pointsQ3_90 += 1
                  else:
                      #print "Quadrant 4" 
                      countQ4_90 = countQ4_90 + point[0]
                      pointsQ4_90 += 1        
              else:
                  print "no" 
                 
              #print totalCount
              #Keep track of number of points that fall into cortical threshold and cancellous threshold respectively
              if point[0] >= self.__corticalMin:
                  corticalCount += 1
              elif point[0] < self.__corticalMin and point[0] >= self.__cancellousMin:
                  cancellousCount += 1
        '''
        print totalCount
        print points00
        print points10
        print points20
        print points30
        print points40
        print points50
        print points60
        print points70
        print points80
        print points90
        '''
        #Calculate averages
        #print str([float(countQ1_00 / pointsQ1_00), float(countQ1_10 / pointsQ1_10), float(countQ1_20 / pointsQ1_20), float(countQ1_30 / pointsQ1_30), float(countQ1_40 / pointsQ1_40), float(countQ1_50 / pointsQ1_50), float(countQ1_60 / pointsQ1_60), float(countQ1_70 / pointsQ1_70), float(countQ1_80 / pointsQ1_80), float(countQ1_90 / pointsQ1_90)])
        #print str([float(countQ2_00 / pointsQ2_00), float(countQ2_10 / pointsQ2_10), float(countQ2_20 / pointsQ2_20), float(countQ2_30 / pointsQ2_30), float(countQ2_40 / pointsQ2_40), float(countQ2_50 / pointsQ2_50), float(countQ2_60 / pointsQ2_60), float(countQ2_70 / pointsQ2_70), float(countQ2_80 / pointsQ2_80), float(countQ2_90 / pointsQ2_90)])
        #print str([float(countQ3_00 / pointsQ3_00), float(countQ3_10 / pointsQ3_10), float(countQ3_20 / pointsQ3_20), float(countQ3_30 / pointsQ3_30), float(countQ3_40 / pointsQ3_40), float(countQ3_50 / pointsQ3_50), float(countQ3_60 / pointsQ3_60), float(countQ3_70 / pointsQ3_70), float(countQ3_80 / pointsQ3_80), float(countQ3_90 / pointsQ3_90)])
        #print str([float(countQ4_00 / pointsQ4_00), float(countQ4_10 / pointsQ4_10), float(countQ4_20 / pointsQ4_20), float(countQ4_30 / pointsQ4_30), float(countQ4_40 / pointsQ4_40), float(countQ4_50 / pointsQ4_50), float(countQ4_60 / pointsQ4_60), float(countQ4_70 / pointsQ4_70), float(countQ4_80 / pointsQ4_80), float(countQ4_90 / pointsQ4_90)])
        
        #avgQuad1.extend([float(countQ1_00 / pointsQ1_00), float(countQ1_10 / pointsQ1_10), float(countQ1_20 / pointsQ1_20), float(countQ1_30 / pointsQ1_30), float(countQ1_40 / pointsQ1_40), float(countQ1_50 / pointsQ1_50), float(countQ1_60 / pointsQ1_60), float(countQ1_70 / pointsQ1_70), float(countQ1_80 / pointsQ1_80), float(countQ1_90 / pointsQ1_90)])
        #avgQuad2.extend([float(countQ2_00 / pointsQ2_00), float(countQ2_10 / pointsQ2_10), float(countQ2_20 / pointsQ2_20), float(countQ2_30 / pointsQ2_30), float(countQ2_40 / pointsQ2_40), float(countQ2_50 / pointsQ2_50), float(countQ2_60 / pointsQ2_60), float(countQ2_70 / pointsQ2_70), float(countQ2_80 / pointsQ2_80), float(countQ2_90 / pointsQ2_90)])
        #avgQuad3.extend([float(countQ3_00 / pointsQ3_00), float(countQ3_10 / pointsQ3_10), float(countQ3_20 / pointsQ3_20), float(countQ3_30 / pointsQ3_30), float(countQ3_40 / pointsQ3_40), float(countQ3_50 / pointsQ3_50), float(countQ3_60 / pointsQ3_60), float(countQ3_70 / pointsQ3_70), float(countQ3_80 / pointsQ3_80), float(countQ3_90 / pointsQ3_90)])
        #avgQuad4.extend([float(countQ4_00 / pointsQ4_00), float(countQ4_10 / pointsQ4_10), float(countQ4_20 / pointsQ4_20), float(countQ4_30 / pointsQ4_30), float(countQ4_40 / pointsQ4_40), float(countQ4_50 / pointsQ4_50), float(countQ4_60 / pointsQ4_60), float(countQ4_70 / pointsQ4_70), float(countQ4_80 / pointsQ4_80), float(countQ4_90 / pointsQ4_90)])
        #print avgQuad1
        #print avgQuad2
        #print avgQuad3
        #print avgQuad4
        
        if points00 != 0:
            avg00 = count00 / points00
            avgTotal.append(avg00)
            sumTotal.append(count00)
        else:
            avg00 = 0
            avgTotal.append(avg00)
        if points10 != 0:
            avg10 = count10 / points10
            avgTotal.append(avg10)
            sumTotal.append(count10)
        else:
            avg10 = 0
            avgTotal.append(avg10)
        if points20 != 0:
            avg20 = count20 / points20
            avgTotal.append(avg20)
            sumTotal.append(count20)
        else:
            avg20 = 0
            avgTotal.append(avg20)
        if points30 != 0:
            avg30 = count30 / points30
            avgTotal.append(avg30)
            sumTotal.append(count30)
        else:
            avg30 = 0
            avgTotal.append(avg30)
        if points40 != 0:
            avg40 = count40 / points40
            avgTotal.append(avg40)
            sumTotal.append(count40)
        else:
            avg40 = 0
            avgTotal.append(avg40)
        if points50 != 0:
            avg50 = count50 / points50
            avgTotal.append(avg50)
            sumTotal.append(count50)
            #print pointsQ1_50 / points50
            #print pointsQ2_50 / points50
            #print pointsQ3_50 / points50
            #print pointsQ4_50 / points50
        else:
            avg50 = 0
            avgTotal.append(avg50)
        if points60 != 0:
            avg60 = count60 / points60
            avgTotal.append(avg60)
            sumTotal.append(count60)
        else:
            avg60 = 0
            avgTotal.append(avg60)
        if points70 != 0:
            avg70 = count70 / points70
            avgTotal.append(avg70)
            sumTotal.append(count70)
        else:
            avg70 = 0
            avgTotal.append(avg70)
        if points80 != 0:
            avg80 = count80 / points80
            avgTotal.append(avg80)
            sumTotal.append(count80)
        else:
            avg80 = 0
            avgTotal.append(avg80)
        if points90 != 0:
            avg90 = count90 / points90
            avgTotal.append(avg90)
            sumTotal.append(count90)
        else:
            avg90 = 0
            avgTotal.append(avg90)  

        avgTotal.reverse()
        sumTotal.reverse()
        '''
        while 0 in avgTotal:
            avgTotal.remove(0)
            sumTotal.remove(0)
        '''
        '''
        print avg00
        print points00
        print avg10
        print points10
        print avg20
        print points20
        print avg30
        print points30
        print avg40
        print points40
        print avg50
        print points50
        print avg60
        print points60
        print avg70
        print points70
        print avg80
        print points80
        print avg90
        print points90
        '''
        totalpoints = points00 + points10 + points20 + points30 + points40 + points50 + points60 + points70 + points80 + points90
        #print "total points found: " + str(totalpoints)
        
        '''
              
        '''
        #Calculate percentages 
        corticalPercent = float(corticalCount) / float(totalCount) * 100
        cancellousPercent = float(cancellousCount) / float(totalCount) *100
        coP = str("%.0f" % corticalPercent)
        caP = str("%.0f" % cancellousPercent)
        percents = []
        percents.append(coP)
        percents.append(caP)
        
        totalData = avgTotal + sumTotal + percents
        self.screwContact.insert(screwIndex, totalData)
        
        #otherPercent = 100 - corticalPercent - cancellousPercent
        
        #print "cortical percent" + str(corticalPercent)
        #print "cancellous percent" + str(cancellousPercent)
        
        coP = str("%.0f" % corticalPercent)
        caP = str("%.0f" % cancellousPercent)
        #otP = str("%.0f" % otherPercent)
          
        qtcoP = qt.QTableWidgetItem(coP)
        qtcap = qt.QTableWidgetItem(caP)
        #qtotP = qt.QTableWidgetItem(otP)
          
        self.itemsqtcoP.append(qtcoP)
        self.itemsqtcaP.append(qtcap)
        #self.itemsqtotP.append(qtotP)
        
        #print screwIndex    
        #self.screwTable.setItem(screwIndex, 4, qtcoP)
        #self.screwTable.setItem(screwIndex, 3, qtcap)
        #self.screwTable.setItem(screwIndex, 2, qtotP)
        
        
        
    def chartContact(self, screwCount):
        # Get the Chart View Node
        cvns = slicer.mrmlScene.GetNodesByClass('vtkMRMLChartViewNode')
        cvns.InitTraversal()
        self.cvn = cvns.GetNextItemAsObject()
        cn = slicer.mrmlScene.AddNode(slicer.vtkMRMLChartNode())
        
        arrayNodes = []
        
        #print self.screwContact
        
        for i in range(0,screwCount):
            # Create an Array Node and add some data
            dn = slicer.mrmlScene.AddNode(slicer.vtkMRMLDoubleArrayNode())
            fidName = self.fidNode.GetNthFiducialLabel(i)
            screwSize = self.screwChoice[i]
            dn.SetName(fidName+'-'+screwSize)
            arrayNodes.insert(i,dn)
            
            screwLength = int(screwSize[3:5])
            sections = screwLength / 5
            
            a = dn.GetArray()
                        
            a.SetNumberOfTuples(sections)
            x = range(0, sections)
            screwValues = self.screwContact[i]
            for j in range(len(x)):
                #if screwValues[j] != 0:
                a.SetComponent(j, 0, (j * 10) + 5)
                a.SetComponent(j, 1, screwValues[j])
                a.SetComponent(j, 2, 0)
                #print j
                #print screwValues[j]
            
            #cn.AddArray('Screw %s' % i, dn.GetID())
            cn.AddArray(fidName, dn.GetID())
            #collectionG = slicer.mrmlScene.GetNodesByName('Head %s' % fidName)
            #headModel = collectionG.GetItemAsObject(0)
            #print fidName
            #print gradeModel
            #cn.SetProperty(fidName, 'color', headModel.GetDisplayNode().GetColorNodeID())
            
            if i == 0:
                cn.SetProperty(fidName, 'color', '#3380cc')
            elif i == 1:
                cn.SetProperty(fidName, 'color', '#33cc80')
            elif i == 2:
                cn.SetProperty(fidName, 'color', '#cc8033')
            elif i == 3:
                cn.SetProperty(fidName, 'color', '#cc3380')
            elif i == 4:
                cn.SetProperty(fidName, 'color', '#8033cc')
            elif i == 5:
                cn.SetProperty(fidName, 'color', '#80cc33')
            elif i == 6:
                cn.SetProperty(fidName, 'color', '#31b1cc')
            elif i == 7:
                cn.SetProperty(fidName, 'color', '#c3cc33')
            else:
                cn.SetProperty(fidName, 'color', '#ff0000') 
              
        dnCort = slicer.mrmlScene.AddNode(slicer.vtkMRMLDoubleArrayNode())
        dnCanc = slicer.mrmlScene.AddNode(slicer.vtkMRMLDoubleArrayNode())
        
        a1 = dnCort.GetArray()
        a2 = dnCanc.GetArray()
        a1.SetNumberOfTuples(2)
        a2.SetNumberOfTuples(2)
        a1.SetComponent(0, 0, 0)
        a1.SetComponent(0, 1, 350)
        a1.SetComponent(0, 2, 0)
        a1.SetComponent(1, 0, 100)
        a1.SetComponent(1, 1, 350)
        a1.SetComponent(1, 2, 0)
        a2.SetComponent(0, 0, 0)
        a2.SetComponent(0, 1, 130)
        a2.SetComponent(0, 2, 0)
        a2.SetComponent(1, 0, 100)
        a2.SetComponent(1, 1, 130)
        a2.SetComponent(1, 2, 0)
        
        cn.AddArray('Cortical Bone', dnCort.GetID())
        cn.AddArray('Cancellous Bone', dnCanc.GetID())

        #cn.SetProperty('default', 'title', 'Information for Screw at point %s' % fidName)
        cn.SetProperty('default', 'title', 'Screw - Bone Contact')
        cn.SetProperty('default', 'xAxisLabel', 'Screw Percentile (Head - Tip)')
        #cn.SetProperty('default', 'xAxisType', 'categorical')
        cn.SetProperty('default', 'yAxisLabel', 'Average HU Contact')
        cn.SetProperty('default', 'showLegend', 'on')
        cn.SetProperty('default', 'type', 'Line')
        cn.SetProperty('default', 'xAxisPad', '0')
        #cn.SetProperty('Volumes', 'lookupTable', slicer.util.getNode('Labels').GetID() )
        #cn.SetProperty('default', 'xAxisPad', '0')
               
        self.cvn.SetChartNodeID(cn.GetID())
        '''
        for i in range(0,screwCount):
            fidName = self.fidNode.GetNthFiducialLabel(i)
            a = cn.GetProperty(fidName, 'color')
            b = a[1:7]
            c = struct.unpack('BBB',b.decode('hex'))
            self.arrayColors.append(c)
        print self.arrayColors
        '''    
    
    def saveResults(self):
	    if self.savePath == None:
	       self.savePath = qt.QFileDialog.getSaveFileName()
            
            with open(unicode(self.savePath), 'ab') as stream:
                writer = csv.writer(stream)
                for row in range(self.screwTable.rowCount):
                    rowdata = []
                    for column in range(self.screwTable.columnCount):
                        item = self.screwTable.item(row, column)
                        #print item
                        if item is not None:
                            rowdata.append(unicode(item.text()).encode('utf8'))
                        else:
                            rowdata.append('')
                    #x = range(0, 10)
                    screwValues = self.screwContact[row]
                    for j in range(len(self.screwContact[row])):
                        rowdata.append(unicode(int(screwValues[j])).encode('utf8'))
                    writer.writerow(rowdata)              
                            
    def clearGrade(self):
        #Clear chart
        self.cvn.SetChartNodeID(None)
        self.screwContact = []

        
        #For each fiducial, restore original screw model and remove graded screw model
        #fidCollection = slicer.mrmlScene.GetNodesByClass('vtkMRMLAnnotationFiducialNode')
        #fidCount = fidCollection.GetNumberOfItems()
        
        for i in range(0, len(self.screwList)):
          #fidCollection.GetItemAsObject(i).GetAnnotationPointDisplayNode().SetOpacity(0.0)
          fidName = self.fiduciallist[i]  
          #fidName = fidCollection.GetItemAsObject(i).GetName()
          collectionS = slicer.mrmlScene.GetNodesByName('Screw model-%s' % fidName)
          screwModel = collectionS.GetItemAsObject(0)
          if screwModel != None:
              modelDisplay = screwModel.GetDisplayNode()
              modelDisplay.SetColor(0.12,0.73,0.91)
              modelDisplay.VisibilityOn()
            
          collectionG = slicer.mrmlScene.GetNodesByName('Grade model-%s' % fidName)
          gradeModel = collectionG.GetItemAsObject(0)
          if gradeModel != None:
              slicer.mrmlScene.RemoveNode(gradeModel)
          
          collectionH = slicer.mrmlScene.GetNodesByName('Head %s' % fidName)
          headModel = collectionH.GetItemAsObject(0)
          if headModel != None:
              slicer.mrmlScene.RemoveNode(headModel)

    def vrUpdate(self, opacity):
      pNode = self.parameterNode()
      vrDisplayNode = Helper.getNodeByID(pNode.GetParameter('vrDisplayNodeID'))
      vrOpacityMap = vrDisplayNode.GetVolumePropertyNode().GetVolumeProperty().GetScalarOpacity()
      vrOpacityMap.RemoveAllPoints()
      vrOpacityMap.AddPoint(0,0)
      vrOpacityMap.AddPoint(self.__cancellousMin-1,0)
      vrOpacityMap.AddPoint(self.__cancellousMin,opacity)
      vrOpacityMap.AddPoint(self.__corticalMax,opacity)
      vrOpacityMap.AddPoint(self.__corticalMax+1,0)
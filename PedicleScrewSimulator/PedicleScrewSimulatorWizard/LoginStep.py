from __main__ import qt, ctk, vtk, slicer

from PedicleScrewSimulatorStep import *
from Helper import *
import DICOM
import os
import math
import csv
from itertools import izip_longest # just zip_longest for python 3.X
import SampleData
from collections import defaultdict

class LoginStep(PedicleScrewSimulatorStep):

    saveFilePath = ''
    saveFileCreated = False
	
    def __init__( self, stepid ):
      self.initialize( stepid )
      self.setName( 'Step 1. User and Case Details' )
      self.setDescription( "Welcome to the OBL pedicle screw simulator for entry point and trajectory selection. To begin, please enter your training details. CT imaging is loaded from the 'Saved Case Library' button. Once loaded, click next to move to the next screen to review the imaging." )

      self.__parent = super( LoginStep, self )
      
      self.bodyModel = None
      self.spineModel = None
      self.caseSelection = None
      self.savedCasePath = ""
      self.savedWorkflow = 0
      self.savedVolumeName = ""
      self.savedThreshold = 0
      self.savedVertebraLabels = []
      self.savedVertebraPositions = []
      self.savedReferenceLabels = []
      self.savedReferencePositions = []
      self.savedEntryPointLabels = []
      self.savedEntryPointPositions = [] 
      self.savedEntryPointTrajectories = []      
      self.savedEntryPointScrewSizes = [] 
      
          
    def killButton(self):
      # hide useless button
      bl = slicer.util.findChildren(text='Final')
      if len(bl):
        bl[0].hide()
    
    def createUserInterface( self ):

      self.__layout = self.__parent.createUserInterface()

	  # User Info group box
      userInfoGroupBox = qt.QGroupBox()
      userInfoGroupBox.title = "User Details"
      self.__layout.addWidget(userInfoGroupBox)
      userInfoFormLayout = qt.QFormLayout(userInfoGroupBox)
      userInfoFormLayout.setContentsMargins(10,10,10,10)

	  # Username
      nText = qt.QLabel("Username:  ")
      self.username = qt.QLineEdit()
      userInfoFormLayout.addRow(nText,self.username)

	  # Training Level
      tText = qt.QLabel("Training Level:  ")
      self.traininglevel = qt.QComboBox()
      self.tLevels = ('-','Resident','Fellow', 'Staff')
      self.traininglevel.addItems(self.tLevels)
      userInfoFormLayout.addRow(tText,self.traininglevel)

	  # Spine Cases Attended
      cText = qt.QLabel("Spine Cases Attended:  ")
      self.cases = qt.QComboBox()
      self.tCases = ('-','0','1-5','5-10','10-20','20+')
      self.cases.addItems(self.tCases)
      userInfoFormLayout.addRow(cText,self.cases)

	  # Save Case group box
      saveCaseGroupBox = qt.QGroupBox()
      saveCaseGroupBox.title = "Option 1: Save New Case"
      saveCaseGroupBox.setToolTip("If you are starting a new case, please indicate where you would like the save file (.csv) written to.")
      self.__layout.addWidget(saveCaseGroupBox)
      saveCaseFormLayout = qt.QFormLayout(saveCaseGroupBox)
      saveCaseFormLayout.setContentsMargins(10,10,10,10)

	  # Save File directory selector
      self.writeToDirSelector = ctk.ctkPathLineEdit()
      self.writeToDirSelector.filters = ctk.ctkPathLineEdit.Dirs
      self.writeToDirSelector.settingKey = "dir"
      saveCaseFormLayout.addRow("CSV File Write Directory:", self.writeToDirSelector)

	  # Load Case group box
      loadCaseGroupBox = qt.QGroupBox()
      loadCaseGroupBox.title = "Option 2: Load Previous Case"
      loadCaseGroupBox.setToolTip("If you have previously saved case data, please indicate the location of the volumes (.nrrd) and select the previously saved case data.")
      self.__layout.addWidget(loadCaseGroupBox)
      loadCaseFormLayout = qt.QFormLayout(loadCaseGroupBox)
      loadCaseFormLayout.setContentsMargins(10,10,10,10)

	  # Load NRRD directory selector
      self.volumeDirSelector = ctk.ctkPathLineEdit()
      self.volumeDirSelector.filters = ctk.ctkPathLineEdit.Dirs
      self.volumeDirSelector.settingKey = "dir"
      loadCaseFormLayout.addRow("NRRD Volumes Directory:", self.volumeDirSelector)

	  # Load Case CSV selector
      self.loadCaseSelector = ctk.ctkPathLineEdit()
      self.loadCaseSelector.filters = ctk.ctkPathLineEdit.Files
      loadCaseFormLayout.addRow("Previously Saved Case:", self.loadCaseSelector)
      self.loadCaseSelector.connect('currentPathChanged(const QString)', self.loadSavedCase)

	  # Load sample data
      self.__sampleCaseButton = qt.QPushButton("CTA Abdomen (sample data)")
      self.__layout.addWidget(self.__sampleCaseButton)
      self.__sampleCaseButton.connect('clicked(bool)', self.loadSampleVolume)

	  # Active Volume Data group box
      activeVolumeGroupBox = qt.QGroupBox()
      activeVolumeGroupBox.title = "Active Volume Data"
      self.__layout.addWidget(activeVolumeGroupBox)
      activeVolumeFormLayout = qt.QFormLayout(activeVolumeGroupBox)
      activeVolumeFormLayout.setContentsMargins(10,10,10,10)

      # select volume
      # creates combobox and populates it with all vtkMRMLScalarVolumeNodes in the scene
      self.__inputSelector = slicer.qMRMLNodeComboBox()
      self.__inputSelector.nodeTypes = ( ("vtkMRMLScalarVolumeNode"), "" )
      self.__inputSelector.addEnabled = False
      self.__inputSelector.removeEnabled = False
      self.__inputSelector.setMRMLScene( slicer.mrmlScene )
      activeVolumeFormLayout.addRow(self.__inputSelector )
      
      # Case Details
      self.libraryCollapsibleButton = ctk.ctkCollapsibleButton()
      self.libraryCollapsibleButton.text = "Saved Case Details"
      activeVolumeFormLayout.addRow(self.libraryCollapsibleButton)
      self.libraryCollapsibleButton.collapsed = True
      
      self.libraryLayout = qt.QFormLayout(self.libraryCollapsibleButton)
      self.libraryDetailsLabel = qt.QLabel('Case Details Here')
      self.libraryLayout.addWidget(self.libraryDetailsLabel)

      #clones DICOM scriptable module
      #self.__dicomWidget = slicer.modules.dicom.widgetRepresentation()
      
      #extract button that launches DICOM browser from widget
      #colButtons = self.__dicomWidget.findChildren('ctkCollapsibleButton')
      #dicomButton = colButtons[1].findChild('QPushButton')
      #dicomButton.setText('CT-DICOM Image Folder')
      #dicomButton.enabled = False
      #self.__layout.addRow(dicomButton)
      
      #open load data dialog for adding case library files
      #self.__loadCaseButton = qt.QPushButton("Saved Case Library")
      #self.__layout.addRow(self.__loadCaseButton)
      #self.__loadCaseButton.connect('clicked(bool)', self.loadCaseVolume)
      '''
      self.anglesButton = qt.QPushButton("Previously Saved Volume")
      self.libraryLayout.addRow(self.anglesButton)
      self.anglesButton.connect('clicked(bool)', self.calculateAngles)
      self.anglesButton.enabled = True
      '''
      #a = qt.QDesktopServices()
      #b = qt.QUrl("www.google.ca")
      #a.openUrl(b)
      
      # self.updateWidgetFromParameters(self.parameterNode())
      qt.QTimer.singleShot(0, self.killButton)

      transform = slicer.vtkMRMLLinearTransformNode()
      transform.SetName("Camera Transform")
      slicer.mrmlScene.AddNode(transform)

      cam = slicer.mrmlScene.GetNodeByID('vtkMRMLCameraNode1')
      cam.SetAndObserveTransformNodeID('vtkMRMLLinearTransformNode4')
      
      self.stopWatchTimer = qt.QTimer();
      self.stopWatchTimer.connect('timeout()',self.stopWatchTimerCount)
      self.timerCount = 0
      self.timerFreq = 100
      '''
      self.clipROI = slicer.vtkMRMLAnnotationROINode()
      slicer.mrmlScene.AddNode(self.clipROI)
      self.clipROI.SetXYZ([10,70,-1350])
      self.clipROI.SetRadiusXYZ([44,22,64])
      '''
      #self.loadBodyModel()
      #self.buildWalls(slicer.mrmlScene.GetNodeByID('vtkMRMLModelNode4'))
               
    def loadSavedCase(self):
        
        #slicer.util.openAddDataDialog()
        #a = slicer.mrmlScene.GetNodesByName('InsertionLandmarks')
        #b = a.GetItemAsObject(0)
        #b.RemoveAllMarkups()
        self.savedWorkflow = 1
        #self.savedCasePath = qt.QFileDialog.getOpenFileName(self, "Open Data File", "", "CSV data files (*.csv)")
        #path = "D:\\Images\\SpineChallenge\\case1-2\\case1_lm2.csv"
        #imageName = self.__baselineVolume.GetName()
        #caseLandmarksPath = os.path.join(os.path.dirname(__file__), 'cases/' + imageName + "_lm.csv") 
        #print os.path.isfile(caseLandmarksPath)
        #path = "E:\\Research\\Imaging\\case1-2\\" + imageName + "_lm.csv"
        rownum = 0
        #self.highCoord = 0
        #self.lowCoord = 0     
        self.savedCasePath = self.loadCaseSelector.currentPath	
        if os.path.isfile(self.savedCasePath) != False: 
            
            columns = defaultdict(list)
                                                                                                
            with open(self.savedCasePath) as stream:
                reader = csv.reader(stream)
                #reader = csv.DictReader(stream)
                reader.next()
                for row in reader:
                    #rowdata = []
                    #print i
                    #print row
                    for (i,v) in enumerate(row):
                        columns[i].append(v)
                    
            for column in columns:
                a = columns[column]
                try:
                    #a.remove('')
                    a = [x for x in a if x != '']
                except ValueError:
                    pass
                columns[column] = a
            b = columns[0]
            self.savedVolumeName = b[0]
            c = columns[1]
            try:
              self.savedThreshold = c[0]					# on exit of this step 
            except:
              pass
            self.savedVertebraLabels = columns[2]			# on exit of this step
            self.savedVertebraPositions = columns[3]		# on exit of this step
            #self.savedReferenceLabels = columns[4]			# unneeded
            #self.savedReferencePositions = columns[5]		# unnneded
            self.savedEntryPointLabels = columns[4]			# on exit of screw step
            self.savedEntryPointPositions = columns[5]		# on exit of screw step
            self.savedEntryPointTrajectories = columns[6]	# on exit of screw step
            self.savedEntryPointScrewSizes = columns[7]		# on exit of screw step
            
            #self.caseSelection = os.path.join(os.path.dirname(__file__), 'cases/' + self.savedVolumeName)
            self.caseSelection = os.path.join(self.volumeDirSelector.currentPath + '/' + self.savedVolumeName + '.nrrd')
            #vol = os.path.join(os.path.dirname(__file__), caseSelection + '.mhd')
            #if self.savedVolumeName[0] == 'c':
            #    self.caseSelection = self.caseSelection + '.mhd'
            #else:
            #    self.caseSelection = self.caseSelection + '.nrrd'    
            #self.volumePath = "D:\\Images\\SpineChallenge\\case1-2\\" + vol
            #self.volumePath = "E:\\Research\\Imaging\\case1-2\\" + vol
            #print self.volumePath
            #slicer.util.loadVolume(self.volumePath)
            print(self.caseSelection)
            slicer.util.loadVolume(self.caseSelection)
            self.volume = slicer.mrmlScene.GetNodeByID('vtkMRMLScalarVolumeNode1')  # on desktop change this back to 1
            self.libraryDetailsLabel.setText(self.savedCasePath)
            '''
            print self.savedVolumeName
            print self.savedThreshold
            print self.savedVertebraLabels
            print self.savedVertebraPositions
            print self.savedReferenceLabels
            print self.savedReferencePositions
            print self.savedEntryPointLabels 
            print self.savedEntryPointPositions 
            print self.savedEntryPointTrajectories      
            print self.savedEntryPointScrewSizes 
            '''

    def loadSampleVolume(self):
        sampleDataLogic = SampleData.SampleDataLogic()
        sampleDataLogic.downloadAbdominalCTVolume()

    def loadCaseVolume(self):   
        #self.libraryCollapsibleButton.collapsed = False 
        self.volumeDialog()
    
    def volumeDialog(self):
        #result = qt.QMessageBox.question(slicer.util.mainWindow(),
        #                'Case Library Selection', 'Select the desired case volume.',
        #                qt.QMessageBox.Ok)
        #msgBox = qt.QMessageBox()
        #msgBox.setText('Select the desired case volume.')
        qW = qt.QWidget()
        msgTwo = qt.QInputDialog(qW)
        caseSelection = msgTwo.getItem(qW,"Case Selection","Choose the case from the list.",["case1","case2","case3","case4","case5","case6","case7","case8","case9","case10"],0)
        #case1Button = msgBox.addButton("Case 1: Normal T5-Sacrum", qt.QMessageBox.ActionRole)
        #case2Button = msgBox.addButton("Nothing Happens", qt.QMessageBox.RejectRole)
        #result = msgBox.exec_()
        result = 0
        if result == 0 and slicer.mrmlScene.GetNodesByName(caseSelection).GetItemAsObject(0) == None:
            self.caseSelection = os.path.join(os.path.dirname(__file__), 'cases/' + caseSelection)
            #vol = os.path.join(os.path.dirname(__file__), caseSelection + '.mhd')
            if caseSelection[0] == 'c':
                self.caseSelection = self.caseSelection + '.mhd'
            else:
                self.caseSelection = self.caseSelection + '.nrrd'    
            #self.volumePath = "D:\\Images\\SpineChallenge\\case1-2\\" + vol
            #self.volumePath = "E:\\Research\\Imaging\\case1-2\\" + vol
            #print self.volumePath
            #slicer.util.loadVolume(self.volumePath)
            slicer.util.loadVolume(self.caseSelection)
            self.volume = slicer.mrmlScene.GetNodeByID('vtkMRMLScalarVolumeNode1')  # on desktop change this back to 1
            self.libraryDetailsLabel.setText(caseSelection)
            #self.loadBodyModel()
            #self.buildTable()
        
        #camera = slicer.mrmlScene.GetNodeByID('vtkMRMLCameraNode1')
        #camera.SetFocalPoint([0.0, 39.0, 1240.0])
        #camera.SetPosition([0.0, -1250, 1240.0])
        #camera.SetViewUp([0,0,1])
    #def loadCaseVolumeSet(self, casenumber):
        #if casenumber == 1:
            #vol='C:\Users\Stewart\Dropbox\Research\SB_2013_Slicer Pedicle Screw Sim\Sacrum-T5-CT.nrrd'
            #slicer.util.loadVolume(vol)
            #self.volume = slicer.mrmlScene.GetNodeByID('vtkMRMLScalarVolumeNode1')  # on desktop change this back to 1
        
     
    def loadBodyModel(self):
        #self.bodyPath = os.path.join(os.path.dirname(__file__), 'body_oriented.stl')
        self.bodyPath = os.path.join(os.path.dirname(__file__), 'chest5.vtk')
        print(self.bodyPath)
        self.bodyModel = slicer.modules.models.logic().AddModel(self.bodyPath)
        bodyModelDisplay = self.bodyModel.GetDisplayNode()
        bodyModelDisplay.SetColor(0.69, 0.47, 0.39)

        self.spinePath = os.path.join(os.path.dirname(__file__), 'Sacrum-T5-Model2.vtk')
        self.spineModel = slicer.modules.models.logic().AddModel(self.spinePath)
        spineModelDisplay = self.spineModel.GetDisplayNode()
        spineModelDisplay.SetColor(1, 0.97, 0.79)
        
        #vol='C:\Users\Stewart\Dropbox\Research\SB_2013_Slicer Pedicle Screw Sim\CTA-cardio.nrrd'
        
        
        #bodyModelDisplay.SetVisibility(0)
        '''
        #Highlight screwh head
        swModel = slicer.vtkMRMLModelNode()
        swModel.SetName('surg_window')
        swModel.SetAndObservePolyData(self.cropBody(self.bodyModel, 'chest'))
        #headModel.SetAndObserveTransformNodeID(transformFid.GetID())
        slicer.mrmlScene.AddNode(swModel)
        
        swDisplay = slicer.vtkMRMLModelDisplayNode()
        swDisplay.SetColor(0.5, 0.5, 0.5)
        slicer.mrmlScene.AddNode(swDisplay)
        swModel.SetAndObserveDisplayNodeID(swDisplay.GetID())
        swDisplay.SetSliceIntersectionVisibility(True)
        '''
        

        matrix = vtk.vtkMatrix4x4()
        '''
        matrix.DeepCopy((1, 0, 0, 30,
                        0, 1, 0, 90,
                        0, 0, 1, 1250,
                        0, 0, 0, 1))
        '''
        matrix.DeepCopy((1, 0, 0, 5,
                        0, 1, 0, -70,
                        0, 0, 1, 1760,
                        0, 0, 0, 1))
        self.transformBody = slicer.vtkMRMLLinearTransformNode()
        self.transformBody.SetName("Transform-volume")
        slicer.mrmlScene.AddNode(self.transformBody)
        self.transformBody.ApplyTransformMatrix(matrix)
        #self.bodyModel.SetAndObserveTransformNodeID(self.transformBody.GetID())
        self.volume.SetAndObserveTransformNodeID(self.transformBody.GetID())
        slicer.vtkSlicerTransformLogic.hardenTransform(self.volume)
        
        self.transformSpine = slicer.vtkMRMLLinearTransformNode()
        self.transformSpine.SetName("Transform-spine")
        slicer.mrmlScene.AddNode(self.transformSpine)
        self.transformSpine.ApplyTransformMatrix(matrix)
        #self.bodyModel.SetAndObserveTransformNodeID(self.transformBody.GetID())
        #self.spineModel.SetAndObserveTransformNodeID(self.transformSpine.GetID())
        
        #swModel.SetAndObserveTransformNodeID(self.transformBody.GetID())
        slicer.app.applicationLogic().PropagateVolumeSelection(1)
        
        
        
    #def transformBody(self):
    
      
     #Crops out head of the screw    
    def cropBody(self,input, area):
        #Get bounds of screw 
        bounds = input.GetPolyData().GetBounds()
        print bounds
        
        #Define bounds for cropping out the head or shaft of the screw respectively
        if area == 'chest':
            '''
            a = bounds[0]+190
            b = bounds[1]-190
            c = bounds[2]
            d = bounds[3]
            e = bounds[4]+460
            f = bounds[5]-110
            '''
            a = bounds[0]+30
            b = bounds[1]-30
            c = bounds[2]
            d = bounds[3]
            e = bounds[4]
            f = bounds[5]
        #Create a box with bounds equal to that of the screw minus the head (-17)
        cropBox = vtk.vtkBox()
        cropBox.SetBounds(a,b,c,d,e,f)
        
        #Crop body
        extract = vtk.vtkExtractPolyDataGeometry()
        extract.SetImplicitFunction(cropBox)
        extract.SetInputData(input.GetPolyData())
        extract.Update()
        
        #PolyData of cropped screw
        output = extract.GetOutput()
        return output    
        
    def buildTable(self):
      self.addWall('Tabletop', 100, 500, 10, 0, 0, 0)
      self.addWall('Tablebottom', 50, 50, 50, 0, 0, -50)  
    
    def buildWalls(self, input):
      bounds = input.GetPolyData().GetBounds()
      roiCenter = [(bounds[1]+bounds[0])/2,(bounds[3]+bounds[2])/2,(bounds[5]+bounds[4])/2]
      roiRadius = [(bounds[1]-bounds[0])/2,(bounds[3]-bounds[2])/2,(bounds[5]-bounds[4])/2]   
      roiXLength = bounds[1] - bounds[0]   
      roiYLength = bounds[3] - bounds[2] 
      roiZLength = bounds[5] - bounds[4] 
      self.addWall('Left', 15, roiYLength, roiZLength, roiCenter[0]-roiRadius[0]+2, roiCenter[1], roiCenter[2])
      self.addWall('Right', 15, roiYLength, roiZLength, roiCenter[0]+roiRadius[0]-2, roiCenter[1], roiCenter[2])
      self.addWall('Top', roiXLength, roiYLength, 5, roiCenter[0], roiCenter[1], roiCenter[2]+roiRadius[2])
      self.addWall('Bottom', roiXLength, roiYLength, 5, roiCenter[0], roiCenter[1], roiCenter[2]-roiRadius[2])
      self.addWall('Front', roiXLength, 5, roiZLength, roiCenter[0], roiCenter[1]+roiRadius[1], roiCenter[2])
        
    def addWall(self, name, xSize, ySize, zSize, xLocation, yLocation, zLocation):
      cube = vtk.vtkCubeSource()
      cube.SetXLength( xSize )
      cube.SetYLength( ySize )
      cube.SetZLength( zSize )
      cube.Update()
    
      # Add the needle poly data to the scene as a model
    
      slicerScene = slicer.mrmlScene
    
      modelNode = slicer.vtkMRMLModelNode()
      modelNode.SetScene( slicerScene )
      modelNode.SetName( name )
      modelNode.SetAndObservePolyData( cube.GetOutput() )

      displayNode = slicer.vtkMRMLModelDisplayNode()
      displayNode.SetScene( slicerScene )
      displayNode.SetName( name + "Display" )
      #displayNode.SetColor(0.43, 0.72, 0.82)
      displayNode.SetColor(0.53, 0.0, 0.01)  
      #if name == 'Front':
      #    displayNode.SetColor(0.53, 0.0, 0.01)
      slicerScene.AddNode( displayNode )
      
      matrix = vtk.vtkMatrix4x4()
      matrix.DeepCopy((1, 0, 0, xLocation,
                       0, 1, 0, yLocation,
                       0, 0, 1, zLocation,
                       0, 0, 0, 1))
      
      transformWall = slicer.vtkMRMLLinearTransformNode()
      transformWall.SetName("Transform-%s" % name)
      slicer.mrmlScene.AddNode(transformWall)
      transformWall.ApplyTransformMatrix(matrix)
            
      modelNode.SetAndObserveTransformNodeID(transformWall.GetID())
      #transformWall.SetAndObserveTransformNodeID(self.transformBody.GetID())
    
      modelNode.SetAndObserveDisplayNodeID( displayNode.GetID() )
      #displayNode.SetPolyData( cube.GetOutput() )
    
      slicerScene.AddNode( modelNode ) 

    def calculateAngles(self):
        print "angles"
        self.angles = []
        self.savePath = None
        a = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode1')
        b = a.GetNumberOfFiducials()
        c = b/2
        for i in range (0,c):
            p1 = [0,0,0]
            p2 = [0,0,0]
            a.GetNthFiducialPosition(i,p1)
            a.GetNthFiducialPosition(i+1,p2)
            slope = (p2[2]-p1[2])/(p2[1]-p1[1])
            angle = math.atan2(slope,1)
            angle = angle * (180/math.pi)
            self.angles.append(angle)
        self.saveResults()
            
    
    def saveResults(self):
	    if self.savePath == None:
	       self.savePath = qt.QFileDialog.getSaveFileName()
    
            with open(unicode(self.savePath), 'ab') as stream:
                writer = csv.writer(stream)
                for row in range(len(self.angles)):
                    rowdata = []
                    rowdata.append(unicode(int(self.angles[row])).encode('utf8'))
                    writer.writerow(rowdata)     
    
    def stopWatchTimerCount(self):
    
        self.timerCount = self.timerCount + 1
    
    #called when entering step
    def onEntry(self, comingFrom, transitionType):

      super(LoginStep, self).onEntry(comingFrom, transitionType)

      # setup the interface
      lm = slicer.app.layoutManager()
      if lm == None: 
        return 
      #lm.setLayout(slicer.vtkMRMLLayoutNode.SlicerLayoutOneUp3DView)
      lm.setLayout(slicer.vtkMRMLLayoutNode.SlicerLayoutConventionalWidescreenView)
      
            
      #camera = slicer.mrmlScene.GetNodeByID('vtkMRMLCameraNode1')
      #camera.SetFocalPoint([0.0, 39.0, 1240.0])
      #camera.SetPosition([0.0, -1250, 1240.0])
      #camera.SetViewUp([0,0,1])
      
      #if self.bodyModel == None:
      #  self.loadBodyModel()
     #   self.buildWalls(slicer.mrmlScene.GetNodeByID('vtkMRMLModelNode4'))
      '''
      qt.QMessageBox.question(slicer.util.mainWindow(),
                        'Welcome to the Pedicle Screw Simulator', 'Choose a saved library case or locate a DICOM CT folder to begin.',
                        qt.QMessageBox.Ok)
      '''
      
      '''
      newName = qt.QInputDialog.getText(
                    slicer.util.mainWindow(), "Welcome to the Pedicle Screw Simulator",
                    "Please enter your name and complete the details on the login screen." )
      self.username.setText(newName)
      '''
      self.stopWatchTimer.start(self.timerFreq)
      
      qt.QTimer.singleShot(0, self.killButton)
      
    
    #check that conditions have been met before proceeding to next step
    def validate( self, desiredBranchId ):

      self.__parent.validate( desiredBranchId )
      
      #read current scalar volume node
      self.__baseline = self.__inputSelector.currentNode()  
     
      #if scalar volume exists proceed to next step and save node ID as 'baselineVolumeID'
      pNode = self.parameterNode()
      if self.__baseline != None:
        baselineID = self.__baseline.GetID()
        if baselineID != '':
          pNode = self.parameterNode()
          pNode.SetParameter('baselineVolumeID', baselineID)
          if self.writeToDirSelector.currentPath == "" and not os.path.isfile(self.savedCasePath):
            self.__parent.validationFailed(desiredBranchId, 'Error','Please select a "CSV File Write Directory", or a "Previously Saved Case" CSV file beore proceeding')
          else:
            self.__parent.validationSucceeded(desiredBranchId)
      else:
        self.__parent.validationFailed(desiredBranchId, 'Error','Please load a volume before proceeding')

        
    #called when exiting step         
    def onExit(self, goingTo, transitionType):
      if not os.path.isfile(self.savedCasePath) and not LoginStep.saveFileCreated:
        self.setSaveFilePath(self.writeToDirSelector.currentPath)
        self.addToSaveFile(LoginStep.saveFilePath, [self.__baseline.GetName()], 0)
        self.transposeSaveFile(LoginStep.saveFilePath)
        LoginStep.saveFileCreated = True
      else:
        LoginStep.saveFilePath = self.savedCasePath

      #check to make sure going to correct step
      if goingTo.id() == 'Approach':
        self.doStepProcessing();
      
      if goingTo.id() != 'Approach':
        return
      
      print "login time " + str(float(self.timerCount/10.0)) + "s"
      self.stopWatchTimer.stop()
      
      super(LoginStep, self).onExit(goingTo, transitionType)


    def setSaveFilePath(self, dirPath):
      caseNum = 1
      filePath = dirPath + '/case' + str(caseNum) + '_'+ str(self.__baseline.GetName()) + '.csv'
      fileExists = False
      while(not fileExists):
        if os.path.isfile(filePath):
          filePath = dirPath + '/case' + str(caseNum) + '_' + str(self.__baseline.GetName()) + '.csv'
          caseNum = caseNum + 1
        else:
          fileExists = True
      LoginStep.saveFilePath = filePath


    def addToSaveFile(self, filePath, listOfItems, rowNum):
      if len(listOfItems) != 0:
        rowCount = 0
        if rowNum != 0: # this section finds the current number of rows in the save file
          with open(filePath,"r") as f:
            reader = csv.reader(f,delimiter = ",")
            data = list(reader)
            rowCount = len(data)
        if rowCount > rowNum: # this section removes pre-existing parameters - might need to check VolumeName, not sure if that parameter updates
          rowCounter = 0
          with open(filePath, 'rb') as f:
            data = list(csv.reader(f))
          with open(filePath, 'wb') as f:
            writer = csv.writer(f)
            for row in data:
              if rowCounter < rowNum:
                writer.writerow(row)
                rowCounter = rowCounter +1
          rowCount = rowNum
        if rowCount == rowNum: # this section adds the new parameter 
          headers = ['VolumeName', 'ThresholdValue', 'VertebraLabelName', 'VertebraLabelPosition', 'EntryPointName', 'EntryPointPosition', 'EntryPointTrajectory', 'EntryPointSize']
          listOfItems.insert(0, headers[rowNum])
          with open(filePath, 'ab') as csvfile:
            filewriter = csv.writer(csvfile, delimiter=',')
            filewriter.writerow(listOfItems)
      else:
        slicer.util.errorDisplay('No parameters to be saved.')

    def transposeSaveFile(self, filePath):
      transposed = izip_longest(*csv.reader(open(filePath, "rb")),fillvalue='') # just zip_longest for python 3.X
      csv.writer(open(filePath, "wb")).writerows(transposed)


    def doStepProcessing(self):
        '''
        #transforms center of imported volume to world origin
        coords = [0,0,0]
        coords = self.__baseline.GetOrigin()
        
        transformVolmat = vtk.vtkMatrix4x4()
        transformVolmat.SetElement(0,3,coords[0]*-1)
        transformVolmat.SetElement(1,3,coords[1]*-1)
        transformVolmat.SetElement(2,3,coords[2]*-1)
        
        transformVol = slicer.vtkMRMLLinearTransformNode()
        slicer.mrmlScene.AddNode(transformVol)
        transformVol.ApplyTransformMatrix(transformVolmat)
        
        #harden transform
        self.__baseline.SetAndObserveTransformNodeID(transformVol.GetID())
        slicer.vtkSlicerTransformLogic.hardenTransform(self.__baseline)
        
        #offsets volume so its center is registered to world origin
        newCoords = [0,0,0,0,0,0]
        self.__baseline.GetRASBounds(newCoords)
        print newCoords
        shift = [0,0,0]
        shift[0] = 0.5*(newCoords[1] - newCoords[0])
        shift[1] = 0.5*(newCoords[3] - newCoords[2])
        shift[2] = 0.5*(newCoords[4] - newCoords[5])
        
        transformVolmat2 = vtk.vtkMatrix4x4()
        transformVolmat2.SetElement(0,3,shift[0])
        transformVolmat2.SetElement(1,3,shift[1])
        transformVolmat2.SetElement(2,3,shift[2])
        
        transformVol2 = slicer.vtkMRMLLinearTransformNode()
        slicer.mrmlScene.AddNode(transformVol2)
        transformVol2.ApplyTransformMatrix(transformVolmat2)
        
        #harden transform
        self.__baseline.SetAndObserveTransformNodeID(transformVol2.GetID())
        slicer.vtkSlicerTransformLogic.hardenTransform(self.__baseline)
        
        #remove transformations from scene
        slicer.mrmlScene.RemoveNode(transformVol)
        slicer.mrmlScene.RemoveNode(transformVol2)
        '''
        print('Done')
        
      

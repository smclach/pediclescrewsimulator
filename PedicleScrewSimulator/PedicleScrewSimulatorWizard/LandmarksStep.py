from __main__ import qt, ctk, vtk, slicer

from PedicleScrewSimulatorStep import *
from Helper import *
import PythonQt
import os

class LandmarksStep( PedicleScrewSimulatorStep ):
    
    def __init__( self, stepid ):
      self.initialize( stepid )
      self.setName( '3. Identify Reference Landmarks' )
      self.setDescription( 'Add fiducial reference landmarks & define measurements.' )
      
      self.__parent = super( LandmarksStep, self )
      qt.QTimer.singleShot(0, self.killButton)
      self.levels = ("C1","C2","C3","C4","C5","C6","C7","T1","T2","T3","T4","T5","T6","T7","T8","T9","T10","T11","T12","L1", "L2", "L3", "L4", "L5","S1")
      self.fidList = []
      self.startCount = 0
      self.addCount = 0
      self.modCount = 0
      self.fidObserve = None
      self.coords = [0,0,0]
          
    def killButton(self):
      # hide useless button
      bl = slicer.util.findChildren(text='Final')
      if len(bl):
        bl[0].hide()

    def begin(self):
      selectionNode = slicer.mrmlScene.GetNodeByID("vtkMRMLSelectionNodeSingleton")
      # place rulers
      selectionNode.SetReferenceActivePlaceNodeClassName("vtkMRMLMarkupsFiducialNode")
      # to place ROIs use the class name vtkMRMLAnnotationROINode
      interactionNode = slicer.mrmlScene.GetNodeByID("vtkMRMLInteractionNodeSingleton")
      placeModePersistence = 1
      interactionNode.SetPlaceModePersistence(placeModePersistence)
      # mode 1 is Place, can also be accessed via slicer.vtkMRMLInteractionNode().Place
      interactionNode.SetCurrentInteractionMode(1)

    def stop(self):
      selectionNode = slicer.mrmlScene.GetNodeByID("vtkMRMLSelectionNodeSingleton")
      # place rulers
      selectionNode.SetReferenceActivePlaceNodeClassName("vtkMRMLMarkupsFiducialNode")
      # to place ROIs use the class name vtkMRMLAnnotationROINode
      interactionNode = slicer.mrmlScene.GetNodeByID("vtkMRMLInteractionNodeSingleton")
      placeModePersistence = 1
      interactionNode.SetPlaceModePersistence(placeModePersistence)
      # mode 1 is Place, can also be accessed via slicer.vtkMRMLInteractionNode().Place
      interactionNode.SetCurrentInteractionMode(2)
      
    def cameraFocus(self, position):  
      camera = slicer.mrmlScene.GetNodeByID('vtkMRMLCameraNode1')
      camera.SetFocalPoint(*position)
      camera.SetPosition(position[0],-200,position[2])
      camera.SetViewUp([0,0,1])           
      camera.ResetClippingRange()
      
    def onTableCellClicked(self):
      #if self.table2.currentColumn() == 0 or :
        print self.table2.currentRow()
        currentFid = self.table2.currentRow()
        position = [0,0,0]
        
        #self.fiducial = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode1')
        self.fiducial.GetNthFiducialPosition(currentFid,position)
        print position
        self.cameraFocus(position)
        self.sliceChange(currentFid)
		  
    
    def sliceChange(self, fidIndex):
      print "sliceChange"
      coords = [0,0,0]
      if self.fiducial != None:
          self.fiducial.GetNthFiducialPosition(fidIndex,coords)
          print coords
               
          lm = slicer.app.layoutManager()
          redWidget = lm.sliceWidget('Red')
          redController = redWidget.sliceController()
        
          yellowWidget = lm.sliceWidget('Yellow')
          yellowController = yellowWidget.sliceController()
        
          greenWidget = lm.sliceWidget('Green')
          greenController = greenWidget.sliceController()
        
          yellowController.setSliceOffsetValue(coords[0])
          greenController.setSliceOffsetValue(coords[1])
          redController.setSliceOffsetValue(coords[2])
          #print pos[0]
          #print pos[1]
          #print pos[2]
          self.fiducial.UpdateScene(slicer.mrmlScene)
	
        
    def updateTable(self):
      #print pNode.GetParameter('vertebrae')
      #self.fiducial = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode1')
      self.fidNumber = self.fiducial.GetNumberOfFiducials()
      print self.fidNumber
      self.fidLabels = []
      self.items = []
      self.Label = qt.QTableWidgetItem()

      self.table2.setRowCount(self.fidNumber)
      
      
      for i in range(0,self.fidNumber):
          self.Label = qt.QTableWidgetItem(self.fiducial.GetNthFiducialLabel(i))
          self.items.append(self.Label)
          self.table2.setItem(i, 0, self.Label)
          self.comboLevel = qt.QComboBox()
          self.comboLevel.addItems(self.levelselection)
          self.comboSide = qt.QComboBox()
          self.comboSide.addItems(["Left","Right"])
          self.table2.setCellWidget(i,1, self.comboLevel)
          self.table2.setCellWidget(i,2, self.comboSide)
          print "update" + str(i)
      print self.Label.text()

    def deleteFiducial(self):   
      if self.table2.currentColumn() == 0:
          item = self.table2.currentItem()
          self.fidNumber = self.fiducial.GetNumberOfFiducials()
          #self.fiducial = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode1')
          for i in range(0,self.fidNumber):
              if item.text() == self.fiducial.GetNthFiducialLabel(i):
                  deleteIndex = i
          self.fiducial.RemoveMarkup(deleteIndex)
          deleteIndex = -1

          print self.table2.currentRow()
          row = self.table2.currentRow()
          self.table2.removeRow(row)
          
    def lockFiducials(self):
      fidNode = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode2')                   
      slicer.modules.markups.logic().SetAllMarkupsLocked(fidNode,True)
      
    def addFiducials(self):
      if self.startCount == 0:
        self.begin()
        self.startCount = 1
        self.startMeasurements.setText("Stop Placing")
      elif self.startCount == 1:
        self.stop()
        self.startCount = 0
        self.startMeasurements.setText("Start Placing")
        
    def addFiducialToTable(self, observer, event):
      self.modCount += 1
      print "Modcount " + str(self.modCount)
      
      if self.modCount == 0 or self.modCount == 2:
        print event
        print "MODIFIED"
        
        #self.fiducial = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode1')
        
        self.landmarks = slicer.mrmlScene.GetNodesByName('Reference Landmarks')
        self.fiducial = self.landmarks.GetItemAsObject(0)
        
        self.fidNumber = self.fiducial.GetNumberOfFiducials()
        #slicer.modules.markups.logic().SetAllMarkupsVisibility(self.fiducial,1)
        print self.fidNumber
	self.sliceChange(self.fidNumber)
        
        self.fidLabels = []
        self.items = []
        self.Label = qt.QTableWidgetItem()
    
        self.table2.setRowCount(self.fidNumber)
        
        
        self.nameLandmark(len(self.fidList))
        self.fidList.append(self.fiducial.GetNthFiducialLabel(len(self.fidList)))
        
        for i in range(0,self.fidNumber):
          print "add" + str(i)
          #self.fiducial.SetNthFiducialLabel(i, str(i))
          
            
          self.Label = qt.QTableWidgetItem(self.fiducial.GetNthFiducialLabel(i))
          c = qt.QCheckBox()
            
          self.items.append(self.Label)
          self.table2.setCellWidget(i,0,c) 
          self.table2.cellWidget(i,0).isChecked()
          self.table2.setItem(i, 1, self.Label)
          '''  
          self.comboLevel = qt.QComboBox()
          self.comboLevel.addItems(self.levelselection)
          self.comboSide = qt.QComboBox()
          self.comboSide.addItems(["Left","Right"])
          self.table2.setCellWidget(i,1, self.comboLevel)
          self.table2.setCellWidget(i,2, self.comboSide)
            
          if i == 0 or i == 1:
            self.table2.cellWidget(i,1).setCurrentIndex(0)
            if i == 1:
              self.table2.cellWidget(i,2).setCurrentIndex(1)
          elif i == 2 or i == 3:
            self.table2.cellWidget(i,1).setCurrentIndex(1)
            if i == 3:
              self.table2.cellWidget(i,2).setCurrentIndex(1)
          elif i == 4 or i == 5:
            self.table2.cellWidget(i,1).setCurrentIndex(2)
            if i == 5:
              self.table2.cellWidget(i,2).setCurrentIndex(1)    
          '''  
        #self.addCount = self.addCount + 1
        self.modCount = 0    
      else:
          
          print "no"      
    
    
    def nameLandmark(self, fidIndex):
        print "name landmark"
        vertPosition = [0,0,0]
        vertUpperPosition = [0,0,0]
        vertLowerPosition = [0,0,0]
        fidPosition = [0,0,0]
        #fidIndex = self.fidCount - 1
        vertPositions = []
        vertText = ""
        #verts = 0
        vertLabels = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode1')
        verts = vertLabels.GetNumberOfFiducials()
        #self.fidNode = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode2')
        self.fiducial.GetNthFiducialPosition(fidIndex,fidPosition)
        #print self.coords
        #print self.fidNode.GetNthFiducialLabel(self.fidCount - 1)
        #print str(fidIndex) + " " + str(fidPosition)
        for j in range(0,verts):
            #print j
            #print "vertPosition" + str(vertPosition[2]) + "fidPos" + str(fidPosition[2])
            vertText = vertLabels.GetNthFiducialLabel(j)
            if j == 0:
                vertLabels.GetNthFiducialPosition(j,vertPosition)
                vertLabels.GetNthFiducialPosition(j+1,vertLowerPosition)
                vertRange = [vertPosition[2]+15, (vertPosition[2]+vertLowerPosition[2])/2]
            elif j == verts-1:
                vertLabels.GetNthFiducialPosition(j,vertPosition)
                vertLabels.GetNthFiducialPosition(j-1,vertUpperPosition)
                vertRange = [(vertPosition[2]+vertUpperPosition[2])/2, vertPosition[2]]
            else:
                vertLabels.GetNthFiducialPosition(j,vertPosition)
                vertLabels.GetNthFiducialPosition(j+1,vertLowerPosition)
                vertLabels.GetNthFiducialPosition(j-1,vertUpperPosition)
                vertRange = [(vertPosition[2]+vertUpperPosition[2])/2, (vertPosition[2]+vertLowerPosition[2])/2]                   
            
            #print vertRange
            #print fidPosition[2]
            if vertRange[1] < fidPosition[2] <= vertRange[0]:
                #print "yes"
                nameCount = 0
                if fidPosition[0] > vertPosition[0]:
                    for items in self.fidList:
                        nameCount += items.count("Right-" + vertText)
                    #nameCount = self.screwList.count("Right-" + vertText)
                    if nameCount == 0:
                        self.fiducial.SetNthFiducialLabel(fidIndex,"Right-" + vertText)
                        self.currentFidLabel = "Right-" + vertText
                    else:
                        self.fiducial.SetNthFiducialLabel(fidIndex,"Right-" + vertText + str(nameCount))
                        self.currentFidLabel = "Right-" + vertText + str(nameCount)
                    
                    #self.cameraSide('Right')
                else:
                    for items in self.fidList:
                        nameCount += items.count("Left-" + vertText)
                    #nameCount = self.screwList.count("Left-" + vertText)
                    if nameCount == 0:
                        self.fiducial.SetNthFiducialLabel(fidIndex,"Left-" + vertText)
                        self.currentFidLabel = "Left-" + vertText
                    else:
                        self.fiducial.SetNthFiducialLabel(fidIndex,"Left-" + vertText + str(nameCount))
                        self.currentFidLabel = "Left-" + vertText + str(nameCount)
                    
                    #self.cameraSide('Left')
    
    def createUserInterface( self ):
      
      
      
      self.__layout = self.__parent.createUserInterface()
      self.startMeasurements = qt.QPushButton("Start Placing")
      self.startMeasurements.connect('clicked(bool)', self.addFiducials)
      #self.__layout.addWidget(self.startMeasurements)  
      
      #self.stopMeasurements = qt.QPushButton("Stop Placing")
      #self.stopMeasurements.connect('clicked(bool)', self.stop)
      #self.__layout.addWidget(self.stopMeasurements)

      #self.updateTable2 = qt.QPushButton("Update Table")
      #self.updateTable2.connect('clicked(bool)', self.updateTable)
      #self.__layout.addWidget(self.updateTable2)
      
      buttonLayout = qt.QHBoxLayout()
      #buttonLayout.addWidget(self.startMeasurements) 
      #buttonLayout.addWidget(self.stopMeasurements)
      #buttonLayout.addWidget(self.updateTable2)
      self.__layout.addRow(buttonLayout)

      '''       
      # Table Output of Fiducial List (Slicer 4.3 Markups)  
      tableDescription = qt.QLabel('List of Points Added to Scene:')
      self.__layout.addRow(tableDescription)
      self.__markupWidget = slicer.modules.markups.createNewWidgetRepresentation()
      slicer.modules.markups.logic().AddNewFiducialNode()
      self.__markupWidget.onActiveMarkupMRMLNodeChanged(0)
      self.table = self.__markupWidget.findChild('QTableWidget')
      self.table.hideColumn(0)
      self.table.hideColumn(1)
      self.table.hideColumn(2)
      self.table.setMinimumHeight(100)
      self.table.setMaximumHeight(170)
      self.table.setMinimumWidth(400)
      self.table.setMaximumWidth(447)
      self.table.resize(400,170)
      self.__layout.addWidget(self.table)
      '''
      self.table2 = qt.QTableWidget()
      self.table2.setRowCount(1)
      self.table2.setColumnCount(3)
      #self.table2.horizontalHeader().setResizeMode(qt.QHeaderView.Stretch)
      self.table2.horizontalHeader().setStretchLastSection(True)
      self.table2.setSizePolicy (qt.QSizePolicy.MinimumExpanding, qt.QSizePolicy.Preferred)
      #self.table2.setMinimumWidth(400)
      self.table2.setMinimumHeight(215)
      self.table2.setMaximumHeight(215)
      self.table2.setColumnWidth(0,20)
      horizontalHeaders = ["","Fiducial Name","Description"]
      self.table2.setHorizontalHeaderLabels(horizontalHeaders)
      self.table2.itemSelectionChanged.connect(self.onTableCellClicked)
      self.__layout.addWidget(self.table2)
      

      self.deleteFid = qt.QPushButton("Remove Selected Fiducial")
      self.deleteFid.connect('clicked(bool)', self.deleteFiducial)
      self.__layout.addWidget(self.deleteFid)

      # Measurements
      
      measureTab = ctk.ctkCollapsibleButton()
      measureTab.text = "Measurements"
      measureTab.collapsed = True
      self.__layout.addWidget(measureTab)
      #transCam.collapsed = True
      measureLayout = qt.QFormLayout(measureTab)
      
            
      self.calcLength = qt.QPushButton("Calculate Length")
      self.calcLength.connect('clicked(bool)', self.lengthMeasurement)
      
      
      self.calcAngles = qt.QPushButton("Calculate Angles")
      self.calcAngles.connect('clicked(bool)', self.anglesMeasurement)
        
      
      self.QHBox4 = qt.QHBoxLayout()
      self.QHBox4.addWidget(self.calcLength)
      self.QHBox4.addWidget(self.self.calcAngles)
      self.measureLayout.addRow(self.QHBox4)    
      
      
      self.table3 = qt.QTableWidget()
      self.table3.setRowCount(1)
      self.table3.setColumnCount(3)
      #self.table2.horizontalHeader().setResizeMode(qt.QHeaderView.Stretch)
      self.table3.horizontalHeader().setStretchLastSection(True)
      self.table3.setSizePolicy (qt.QSizePolicy.MinimumExpanding, qt.QSizePolicy.Preferred)
      #self.table2.setMinimumWidth(400)
      self.table3.setMinimumHeight(215)
      self.table3.setMaximumHeight(215)
      self.table3.setColumnWidth(0,20)
      horizontalHeaders = ["Fiducials","Type","Measurement"]
      self.table3.setHorizontalHeaderLabels(horizontalHeaders)
      self.table3.itemSelectionChanged.connect(self.onTableCellClicked)
      self.measureLayout.addWidget(self.table3)
      

      # Camera Transform Sliders
      
      transCam = ctk.ctkCollapsibleButton()
      transCam.text = "Shift Camera Position"
      transCam.collapsed = True
      self.__layout.addWidget(transCam)
      #transCam.collapsed = True
      camLayout = qt.QFormLayout(transCam)

      a = PythonQt.qMRMLWidgets.qMRMLTransformSliders()
      a.setMRMLTransformNode(slicer.mrmlScene.GetNodeByID('vtkMRMLLinearTransformNode4'))
      #transWidget = slicer.modules.transforms.createNewWidgetRepresentation()
      #transSelector = transWidget.findChild('qMRMLNodeComboBox')
      #transWidgetPart = transWidget.findChild('ctkCollapsibleButton')
      #transformSliders = transWidgetPart.findChildren('qMRMLTransformSliders')
      camLayout.addRow(a)
            
                   
      qt.QTimer.singleShot(0, self.killButton)
       
    
    def lengthMeasurement(self):
        print "calculating length"
        
    def anglesMeasurement(self):
        print "calculating angles"    
    
    def onEntry(self, comingFrom, transitionType):

      super(LandmarksStep, self).onEntry(comingFrom, transitionType)
      
      qt.QTimer.singleShot(0, self.killButton)
      
      lm = slicer.app.layoutManager()
      if lm == None: 
        return 
      lm.setLayout(slicer.vtkMRMLLayoutNode.SlicerLayoutFourUpView)
      
      lNode = slicer.mrmlScene.GetNodesByName('Vertebrae Labels')
      labels = lNode.GetItemAsObject(0)
      #self.fiducial = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode1')
      labelsNumber = labels.GetNumberOfFiducials()
      
      markup = slicer.modules.markups.logic()
      markup.AddNewFiducialNode('Reference Landmarks')
      
    
      a = slicer.mrmlScene.GetNodesByName('Reference Landmarks')
      self.fiducial = a.GetItemAsObject(0)
      dNode = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsDisplayNode3')
      dNode.SetSelectedColor(0, 1, 0.3)
      dNode.SetGlyphScale(5)
      dNode.SetTextScale(5)
      
      self.fidObserve = self.fiducial.AddObserver('ModifiedEvent', self.addFiducialToTable)
      
      slicer.modules.markups.logic().AddFiducial()
        
      #position = [0,0,0]
      for i in range(labelsNumber):
          if labels.GetNthFiducialLabel(i) == 'L3':
              self.fiducial.SetNthFiducialLabel(0,"L3")
              labels.GetNthFiducialPosition(i,self.coords) 
              self.fiducial.SetNthFiducialPosition(0,self.coords[0]+5,self.coords[1]-50,self.coords[2]-5)
      '''    
      pNode = self.parameterNode()
      print pNode
      self.levelselection = []
      self.vertebra = str(pNode.GetParameter('vertebra'))
      self.inst_length = str(pNode.GetParameter('inst_length'))
      self.approach = str(pNode.GetParameter('approach'))
      for i in range(self.levels.index(self.vertebra),self.levels.index(self.vertebra)+int(self.inst_length)):
          print self.levels[i]
          self.levelselection.append(self.levels[i])
      print self.levelselection
      
      camera = slicer.mrmlScene.GetNodeByID('vtkMRMLCameraNode1')
      if self.approach == 'Posterior':
          print "posterior"
          camera.SetPosition(0,-600,0)
          camera.SetViewUp([0,0,1])
      elif self.approach == 'Anterior':
          print "Anterior"
          camera.SetPosition(0,600,0)
          camera.SetViewUp([0,0,1])
      elif self.approach == 'Left':
          print "Left"
          camera.SetPosition(-600,0,0)
          camera.SetViewUp([0,0,1])
      elif self.approach == 'Right':
          print "Right"
          camera.SetPosition(600,0,0)
          camera.SetViewUp([0,0,1])
      camera.ResetClippingRange()
      #pNode = self.parameterNode()
      #pNode.SetParameter('currentStep', self.stepid)
      '''  
      '''
      #roiVolume = Helper.getNodeByID(pNode.GetParameter('croppedBaselineVolumeID'))
      selectionNode = slicer.mrmlScene.GetNodeByID("vtkMRMLSelectionNodeSingleton")
      # place rulers
      selectionNode.SetReferenceActivePlaceNodeClassName("vtkMRMLMarkupsFiducialNode")
      # to place ROIs use the class name vtkMRMLAnnotationROINode
      interactionNode = slicer.mrmlScene.GetNodeByID("vtkMRMLInteractionNodeSingleton")
      placeModePersistence = 1
      interactionNode.SetPlaceModePersistence(placeModePersistence)
      # mode 1 is Place, can also be accessed via slicer.vtkMRMLInteractionNode().Place
      interactionNode.SetCurrentInteractionMode(1)
      '''

      #fiducialNode = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode1')
      
      
      slicer.modules.models.logic().SetAllModelsVisibility(1)
      a = slicer.mrmlScene.GetNodesByName('clipModel').GetItemAsObject(0).SetDisplayVisibility(0)
      b = slicer.mrmlScene.GetNodesByName('clipModel').GetItemAsObject(1).SetDisplayVisibility(0)
      
      if comingFrom.id() == 'DefineROI':
          self.updateTable() 
        
           
     
    def onExit(self, goingTo, transitionType):
      
      if goingTo.id() == 'Screw':
          self.stop()
          slicer.modules.markups.logic().SetAllMarkupsVisibility(self.fiducial,1)
          #fiducialNode = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode1')
          self.fiducial.RemoveObserver(self.fidObserve)
          self.doStepProcessing()
          #print self.table2.cellWidget(0,1).currentText
      
      #if goingTo.id() == 'Threshold':
          #slicer.mrmlScene.RemoveNode(self.__outModel)     
      '''
      if goingTo.id() != 'DefineROI':
          print "here 2"  
          return
      '''
      super(LandmarksStep, self).onExit(goingTo, transitionType)
      '''
      selectionNode = slicer.mrmlScene.GetNodeByID("vtkMRMLSelectionNodeSingleton")
      # place rulers
      selectionNode.SetReferenceActivePlaceNodeClassName("vtkMRMLMarkupsFiducialNode")
      # to place ROIs use the class name vtkMRMLAnnotationROINode
      interactionNode = slicer.mrmlScene.GetNodeByID("vtkMRMLInteractionNodeSingleton")
      placeModePersistence = 1
      interactionNode.SetPlaceModePersistence(placeModePersistence)
      # mode 1 is Place, can also be accessed via slicer.vtkMRMLInteractionNode().Place
      interactionNode.SetCurrentInteractionMode(2)
      '''
    def validate( self, desiredBranchId ):

      self.__parent.validate( desiredBranchId )
      self.__parent.validationSucceeded(desiredBranchId)
      
      #self.inputFiducialsNodeSelector.update()
      #fid = self.inputFiducialsNodeSelector.currentNode() 
      fidNumber = self.fiducial.GetNumberOfFiducials() 
      ''' 
      #pNode = self.parameterNode()
      if fidNumber != 0:
      #  fidID = fid.GetID()
      #  if fidID != '':
      #    pNode = self.parameterNode()
      #    pNode.SetParameter('fiducialID', fidID)
          self.__parent.validationSucceeded(desiredBranchId)
      else:
          self.__parent.validationFailed(desiredBranchId, 'Error','Please place at least one fiducial on the model before proceeding')
      '''
    def doStepProcessing(self):
      #list = ['a','b','c']
      #listNode = self.parameterNode()
      #listNode.SetParameter = ('list', list)  
      print('Done')
      self.lockFiducials()  

from __main__ import qt, ctk, slicer

from PedicleScrewSimulatorStep import *
from Helper import *
import PythonQt
import string
import vtkITK
import VolumeClipWithModel
#import RemoveIslandsEffect
import csv
import os
import LoginStep as LoginStepModule

class ApproachStep( PedicleScrewSimulatorStep ) :

  def __init__( self, stepid ):
    self.initialize( stepid )
    self.setName( 'Step 2. Review Imaging' )
    #self.setDescription( "Please take the time to review the case imaging. The dropdown box below can be used to jump to specific levels. The slice intersections are shown by the crossing lines in the images. The slice angle can be adjusted." )

    self.__parent = super( ApproachStep, self )
    self.startCount = 0
    self.idenCount = 0
    self.levelIndex = 0
    self.highCoord = [0,0,0]
    self.lowCoord = [0,0,0]
    self.highIndex = 0
    self.lowIndex = 0
    self.modCount = 0
    self.labels = 0
    self.lmOn = 0
    self.labelOn = 0
    self.boneThreshold = 150
    self.entry = 1
    self.adjustCount2 = 0
    self.levels = ("C1","C2","C3","C4","C5","C6","C7","T1","T2","T3","T4","T5","T6","T7","T8","T9","T10","T11","T12","L1", "L2", "L3", "L4", "L5","S1")
    self.fiducial = None
    self.oldRedPosition = 0
    self.oldYellowPosition = 0
    self.oldGreenPosition = 0
    self.lThresh = 1000
    self.uThresh = 0
    self.reset()

    qt.QTimer.singleShot(0, self.killButton)

  def reset(self):
    self.__vrDisplayNode = None

    self.__roiTransformNode = None
    self.__baselineVolume = None

    self.__roi = None
    self.__roiObserverTag = None

  def killButton(self):
    # hide useless button
    bl = slicer.util.findChildren(text='Final')
    if len(bl):
      bl[0].hide()

  def createUserInterface( self ):

    self.__layout = self.__parent.createUserInterface()

    fileText = qt.QLabel("Label File:  ")
    self.labelFile = qt.QLineEdit()

    #label for ROI selector
    vertLabel = qt.QLabel( 'Vertebrae Labels:' )
    font = vertLabel.font
    font.setBold(True)
    vertLabel.setFont(font)

    labelText = qt.QLabel("Add Labels:  ")
    self.identify = qt.QPushButton("Click to Identify")
    self.identify.connect('clicked(bool)', self.addFiducials)
    self.identify.setMaximumWidth(170)

    #label for ROI selector
    viewLabel = qt.QLabel( 'Viewpoint:' )
    font = viewLabel.font
    font.setBold(True)
    viewLabel.setFont(font)

    regionText = qt.QLabel("Surgical Region of Interest:  ")
    vText = qt.QLabel("Jump to Vertebral Level:  ")
    iText = qt.QLabel("# to Instrument:  ")
    #aText = qt.QLabel("Approach Direction:  ")
    self.rSelector = qt.QComboBox()
    self.rSelector.setMaximumWidth(120)
    self.regions = ('-','Upper Thoracic','Lower Thoracic', 'Lumbar', 'Sacral')
    self.rSelector.addItems(self.regions)
    self.connect(self.rSelector, PythonQt.QtCore.SIGNAL('activated(QString)'), self.region_chosen)

    self.vSelector = qt.QComboBox()
    self.vSelector.setMaximumWidth(120)
    #self.levels = ("T6","T7","T8","T9","T10","T11","T12","L1", "L2", "L3", "L4", "L5", "Sacrum")
    #self.vSelector.addItems(self.levels)
    self.connect(self.vSelector, PythonQt.QtCore.SIGNAL('activated(QString)'), self.level_chosen)

    self.iSelector = qt.QComboBox()
    self.iSelector.setMaximumWidth(120)
    self.iSelector.addItems(["-",'1','2','3','4','5','6'])

    blank = qt.QLabel("  ")
    blank.setMaximumWidth(30)

    redSliceText = qt.QLabel( 'Red Slice Angle:' )
    self.sliderRed = ctk.ctkSliderWidget()
    self.sliderRed.connect('valueChanged(double)', self.redsliderValueChanged)
    self.sliderRed.minimum = -100
    self.sliderRed.maximum = 100

    yellowSliceText = qt.QLabel( 'Yellow Slice Angle:' )
    self.sliderYellow = ctk.ctkSliderWidget()
    self.sliderYellow.connect('valueChanged(double)', self.yellowsliderValueChanged)
    self.sliderYellow.minimum = -100
    self.sliderYellow.maximum = 100

    greenSliceText = qt.QLabel( 'Green Slice Angle:' )
    self.sliderGreen = ctk.ctkSliderWidget()
    self.sliderGreen.connect('valueChanged(double)', self.greensliderValueChanged)
    self.sliderGreen.minimum = -100
    self.sliderGreen.maximum = 100

    self.vertebraeGridBox = qt.QFormLayout()
    self.vertebraeGridBox.addRow(vertLabel)
    self.vertebraeGridBox.addRow(fileText,self.labelFile)
    self.vertebraeGridBox.addRow(labelText,self.identify)
    self.vertebraeGridBox.addRow(viewLabel)
    #self.vertebraeGridBox.addRow(regionText,self.rSelector)
    self.vertebraeGridBox.addRow(vText,self.vSelector)
    self.vertebraeGridBox.addRow(redSliceText,self.sliderRed)
    self.vertebraeGridBox.addRow(yellowSliceText,self.sliderYellow)
    self.vertebraeGridBox.addRow(greenSliceText,self.sliderGreen)



    self.__layout.addRow(self.vertebraeGridBox)

    #self.__layout.addRow("Starting Instrumented Vertebra:",self.vSelector)
    #self.__layout.addRow("Number of Vertebrae to Instrument:",self.iSelector)
    '''
    reconCollapsibleButton = ctk.ctkCollapsibleButton()
    reconCollapsibleButton.text = "Change Slice Reconstruction"
    self.__layout.addWidget(reconCollapsibleButton)
    reconCollapsibleButton.collapsed = True
    # Layout
    reconLayout = qt.QFormLayout(reconCollapsibleButton)
    #label for ROI selector
    reconLabel = qt.QLabel( 'Recon Slice:' )
    rotationLabel = qt.QLabel( 'Rotation Angle:' )

    #creates combobox and populates it with all vtkMRMLAnnotationROINodes in the scene
    self.selector = slicer.qMRMLNodeComboBox()
    self.selector.nodeTypes = ['vtkMRMLSliceNode']
    self.selector.toolTip = "Change Slice Reconstruction"
    self.selector.setMRMLScene(slicer.mrmlScene)
    self.selector.addEnabled = 1

    #add label + combobox
    reconLayout.addRow( reconLabel, self.selector )

    #self.reconSlice = slicer.qMRMLNodeComboBox()
    #self.recon = slicer.modules.reformat.createNewWidgetRepresentation()
    # pull slice selector
    #self.selector = self.recon.findChild('qMRMLNodeComboBox')
    #self.selector.setCurrentNodeID('vtkMRMLSliceNodeRed')
    #self.__layout.addWidget(self.selector)

    self.slider = ctk.ctkSliderWidget()
    #self.slider = PythonQt.qMRMLWidgets.qMRMLLinearTransformSlider()
    #tnode = slicer.mrmlScene.GetNodeByID('vtkMRMLLinearTransformNode1')
    #self.slider.setMRMLTransformNode(tnode)
    self.slider.connect('valueChanged(double)', self.sliderValueChanged)
    self.slider.minimum = -100
    self.slider.maximum = 100
    reconLayout.addRow( rotationLabel, self.slider)
    '''
    '''
    self.crosshair = qt.QPushButton("Hide Crosshair")
    self.crosshair.connect('clicked(bool)', self.crosshairVisible)
    self.__layout.addRow(self.crosshair)
    '''
    # Hide Threshold Details
    threshCollapsibleButton = ctk.ctkCollapsibleButton()
    #roiCollapsibleButton.setMaximumWidth(320)
    threshCollapsibleButton.text = "Bone Threshold Details"
    self.__layout.addWidget(threshCollapsibleButton)
    threshCollapsibleButton.collapsed = False

    # Layout
    threshLayout = qt.QFormLayout(threshCollapsibleButton)

    self.__loadThreshButton = qt.QPushButton("Show Threshold Label")
    threshLayout.addRow(self.__loadThreshButton)
    self.__loadThreshButton.connect('clicked(bool)', self.showThreshold)
    '''
    self.__autoThreshButton = qt.QPushButton("Auto Fill Voids")
    threshLayout.addRow(self.__autoThreshButton)
    self.__loadThreshButton.connect('clicked(bool)', self.autoThreshold)
    '''

    self.__corticalRange = slicer.qMRMLRangeWidget()
    self.__corticalRange.decimals = 0
    self.__corticalRange.singleStep = 1

    cortLabel = qt.QLabel('Choose Cortical Bone Threshold:')
    self.__corticalRange.connect('valuesChanged(double,double)', self.onCorticalChanged)
    threshLayout.addRow(cortLabel)
    threshLayout.addRow(self.__corticalRange)


    # Hide ROI Details
    roiCollapsibleButton = ctk.ctkCollapsibleButton()
    #roiCollapsibleButton.setMaximumWidth(320)
    roiCollapsibleButton.text = "Advanced Options"
    self.__layout.addWidget(roiCollapsibleButton)
    roiCollapsibleButton.collapsed = True

    # Layout
    roiLayout = qt.QFormLayout(roiCollapsibleButton)

    self.__loadLandmarksButton = qt.QPushButton("Show Crop Landmarks")
    roiLayout.addRow(self.__loadLandmarksButton)
    self.__loadLandmarksButton.connect('clicked(bool)', self.showLandmarks)

    #label for ROI selector
    roiLabel = qt.QLabel( 'Select ROI:' )
    font = roiLabel.font
    font.setBold(True)
    roiLabel.setFont(font)

    #creates combobox and populates it with all vtkMRMLAnnotationROINodes in the scene
    self.__roiSelector = slicer.qMRMLNodeComboBox()
    self.__roiSelector.nodeTypes = ['vtkMRMLAnnotationROINode']
    self.__roiSelector.toolTip = "ROI defining the structure of interest"
    self.__roiSelector.setMRMLScene(slicer.mrmlScene)
    self.__roiSelector.addEnabled = 1

    #add label + combobox
    roiLayout.addRow( roiLabel, self.__roiSelector )

    self.__roiSelector.connect("currentNodeChanged(vtkMRMLNode*)", self.onROIChanged)

    # the ROI parameters
    # GroupBox to hold ROI Widget
    voiGroupBox = qt.QGroupBox()
    voiGroupBox.setTitle( 'Define VOI' )
    roiLayout.addRow( voiGroupBox )

    # create form layout for GroupBox
    voiGroupBoxLayout = qt.QFormLayout( voiGroupBox )

    # create ROI Widget and add it to the form layout of the GroupBox
    self.__roiWidget = PythonQt.qSlicerAnnotationsModuleWidgets.qMRMLAnnotationROIWidget()
    voiGroupBoxLayout.addRow( self.__roiWidget )

    # Hide VR Details
    vrCollapsibleButton = ctk.ctkCollapsibleButton()
    #roiCollapsibleButton.setMaximumWidth(320)
    vrCollapsibleButton.text = "Rendering Details"
    #self.__layout.addWidget(vrCollapsibleButton)
    vrCollapsibleButton.collapsed = True

    # Layout
    vrLayout = qt.QFormLayout(vrCollapsibleButton)

    # the ROI parameters
    # GroupBox to hold ROI Widget
    vrGroupBox = qt.QGroupBox()
    vrGroupBox.setTitle( 'Define Rendering' )
    vrLayout.addRow( vrGroupBox )

    # create form layout for GroupBox
    vrGroupBoxLayout = qt.QFormLayout( vrGroupBox )

    # create ROI Widget and add it to the form layout of the GroupBox
    self.__vrWidget = PythonQt.qSlicerVolumeRenderingModuleWidgets.qSlicerPresetComboBox()
    #self.__vrWidget = PythonQt.qSlicerVolumeRenderingModuleWidgets.qMRMLVolumePropertyNodeWidget()
    vrGroupBoxLayout.addRow( self.__vrWidget )

    # initialize VR
    self.__vrLogic = slicer.modules.volumerendering.logic()


    lm = slicer.app.layoutManager()
    redWidget = lm.sliceWidget('Red')
    self.__redController = redWidget.sliceController()
    self.__redLogic = redWidget.sliceLogic()

    yellowWidget = lm.sliceWidget('Yellow')
    self.__yellowController = yellowWidget.sliceController()
    self.__yellowLogic = yellowWidget.sliceLogic()

    greenWidget = lm.sliceWidget('Green')
    self.__greenController = greenWidget.sliceController()
    self.__greenLogic = greenWidget.sliceLogic()

    qt.QTimer.singleShot(0, self.killButton)

    self.stopWatchTimer = qt.QTimer();
    self.stopWatchTimer.connect('timeout()',self.stopWatchTimerCount)
    self.timerCount = 0
    self.timerFreq = 100

  #def autoThreshold(self):


  def showThreshold(self):
    if self.labelOn == 0:
        print "ON"
        lm = slicer.app.layoutManager()
        redWidget = lm.sliceWidget('Red')
        redController = redWidget.sliceController()

        yellowWidget = lm.sliceWidget('Yellow')
        yellowController = yellowWidget.sliceController()

        greenWidget = lm.sliceWidget('Green')
        greenController = greenWidget.sliceController()

        yellowController.setLabelMapHidden(True)
        greenController.setLabelMapHidden(True)
        redController.setLabelMapHidden(True)
        self.labelOn = 1
        self.__loadThreshButton.setText("Show Threshold Label")
    else:
        lm = slicer.app.layoutManager()
        print "OFF"
        redWidget = lm.sliceWidget('Red')
        redController = redWidget.sliceController()

        yellowWidget = lm.sliceWidget('Yellow')
        yellowController = yellowWidget.sliceController()

        greenWidget = lm.sliceWidget('Green')
        greenController = greenWidget.sliceController()

        yellowController.setLabelMapHidden(False)
        greenController.setLabelMapHidden(False)
        redController.setLabelMapHidden(False)
        self.labelOn = 0
        self.__loadThreshButton.setText("Hide Threshold Label")

  def showLandmarks(self):
    markup = slicer.modules.markups.logic()
    if self.lmOn == 0:
        b = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode2')
        markup.SetAllMarkupsVisibility(b,1)
        self.__loadLandmarksButton.setText("Hide Crop Landmarks")
        self.lmOn = 1
    else:
        b = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode2')
        markup.SetAllMarkupsVisibility(b,0)
        self.__loadLandmarksButton.setText("Show Crop Landmarks")
        self.lmOn = 0

  def removeIslandsMorphologyDecruft(self,image,foregroundLabel,backgroundLabel,iterations=1):
    #
    # make binary mask foregroundLabel->1, backgroundLabel->0
    #
    binThresh = vtk.vtkImageThreshold()
    if vtk.VTK_MAJOR_VERSION <= 5:
      binThresh.SetInput( image )
    else:
      binThresh.SetInputData( image )
    binThresh.ThresholdBetween(foregroundLabel,backgroundLabel)
    binThresh.SetInValue( 1 )
    binThresh.SetOutValue( 0 )
    binThresh.ReplaceOutOn()
    binThresh.ReplaceInOn()
    binThresh.Update()

    #
    # first, erode iterations number of times
    #
    eroder = slicer.vtkImageErode()
    eroderImage = vtk.vtkImageData()
    eroderImage.DeepCopy(binThresh.GetOutput())
    if vtk.VTK_MAJOR_VERSION <= 5:
      eroder.SetInput(eroderImage)
    else:
      eroder.SetInputData(eroderImage)
    for iteration in range(iterations):
      eroder.SetForeground( 1 )
      eroder.SetBackground( 0 )
      eroder.SetNeighborTo8()
      eroder.Update()
      eroderImage.DeepCopy(eroder.GetOutput())


    #
    # now save only islands bigger than a specified size
    #

    # note that island operation happens in unsigned long space
    # but the slicer editor works in Short
    castIn = vtk.vtkImageCast()
    if vtk.VTK_MAJOR_VERSION <= 5:
      castIn.SetInput( eroderImage )
    else:
      castIn.SetInputConnection( eroder.GetInputConnection(0,0) )
    castIn.SetOutputScalarTypeToShort()

    # now identify the islands in the inverted volume
    # and find the pixel that corresponds to the background
    islandMath = vtkITK.vtkITKIslandMath()
    if vtk.VTK_MAJOR_VERSION <= 5:
      islandMath.SetInput( castIn.GetOutput() )
    else:
      islandMath.SetInputConnection( castIn.GetOutputPort() )
    islandMath.SetFullyConnected( True )
    islandMath.SetMinimumSize( 10 )

    # note that island operation happens in unsigned long space
    # but the slicer editor works in Short
    castOut = vtk.vtkImageCast()
    if vtk.VTK_MAJOR_VERSION <= 5:
      castOut.SetInput( islandMath.GetOutput() )
    else:
      castOut.SetInputConnection( islandMath.GetOutputPort() )
    castOut.SetOutputScalarTypeToShort()

    castOut.Update()
    islandCount = islandMath.GetNumberOfIslands()
    islandOrigCount = islandMath.GetOriginalNumberOfIslands()
    ignoredIslands = islandOrigCount - islandCount
    print( "%d islands created (%d ignored)" % (islandCount, ignoredIslands) )

    #
    # now map everything back to 0 and 1
    #

    thresh = vtk.vtkImageThreshold()
    if vtk.VTK_MAJOR_VERSION <= 5:
      thresh.SetInput( castIn.GetOutput() )
    else:
      thresh.SetInputConnection( castIn.GetOutputPort() )
    thresh.ThresholdByUpper(1)
    thresh.SetInValue( 1 )
    thresh.SetOutValue( 0 )
    thresh.Update()

    #
    # now, dilate back (erode background) iterations_plus_one number of times
    #
    dilater = slicer.vtkImageErode()
    dilaterImage = vtk.vtkImageData()
    dilaterImage.DeepCopy(thresh.GetOutput())
    if vtk.VTK_MAJOR_VERSION <= 5:
      dilater.SetInput(dilaterImage)
    else:
      dilater.SetInputData(dilaterImage)
    for iteration in range(iterations):
      dilater.SetForeground( 0 )
      dilater.SetBackground( 1 )
      dilater.SetNeighborTo8()
      dilater.Update()
      dilaterImage.DeepCopy(dilater.GetOutput())

    castOut = vtk.vtkImageCast()
    if vtk.VTK_MAJOR_VERSION <= 5:
      castOut.SetInput( dilaterImage )
    else:
      castOut.SetInputConnection( dilater.GetInputConnection(0,0) )
    castOut.SetOutputScalarTypeToShort()

    thresh2 = vtk.vtkImageThreshold()
    if vtk.VTK_MAJOR_VERSION <= 5:
      thresh2.SetInput( castOut.GetOutput() )
    else:
      thresh2.SetInputConnection( castOut.GetOutputPort() )
    thresh2.ThresholdByUpper(1)
    thresh2.SetInValue( 1 )
    thresh2.SetOutValue( 0 )
    thresh2.Update()

    boneThresh = vtk.vtkImageThreshold()
    if vtk.VTK_MAJOR_VERSION <= 5:
      boneThresh.SetInput( image )
    else:
      boneThresh.SetInputData( image )
    boneThresh.ThresholdBetween(160,backgroundLabel)
    boneThresh.SetInValue( 1 )
    boneThresh.SetOutValue( 0 )
    boneThresh.ReplaceOutOn()
    boneThresh.ReplaceInOn()
    boneThresh.Update()
    #
    # only keep pixels in both original and dilated result
    #

    logic = vtk.vtkImageLogic()
    if vtk.VTK_MAJOR_VERSION <= 5:
      logic.SetInput1(thresh2.GetOutput())
      logic.SetInput2(boneThresh.GetOutput())
    else:
      logic.SetInputConnection(0, thresh2.GetOutputPort())
      logic.SetInputConnection(1, boneThresh.GetOutputPort())
    #if foregroundLabel == 0:
    #  logic.SetOperationToNand()
    #else:
    logic.SetOperationToOr()
    logic.SetOutputTrueValue(1)
    logic.Update()

    #
    # convert from binary mask to 1->foregroundLabel, 0->backgroundLabel
    #
    unbinThresh = vtk.vtkImageThreshold()
    if vtk.VTK_MAJOR_VERSION <= 5:
      unbinThresh.SetInput( logic.GetOutput() )
    else:
      unbinThresh.SetInputConnection( logic.GetOutputPort() )
    unbinThresh.ThresholdBetween( 1,1 )
    unbinThresh.SetInValue( foregroundLabel )
    unbinThresh.SetOutValue( backgroundLabel )
    unbinThresh.Update()

    image.DeepCopy(unbinThresh.GetOutput())

    if self.__corticalNode != None:
        self.__corticalNode.SetAndObserveImageData(logic.GetOutput())

  def onCorticalChanged(self):
    r = self.__redLogic.GetSliceOffset()
    y = self.__yellowLogic.GetSliceOffset()
    g = self.__greenLogic.GetSliceOffset()



    #self.__cancellousRange.maximumValue = self.__corticalRange.minimumValue

    range0 = self.__corticalRange.minimumValue
    range1 = self.__corticalRange.maximumValue

    self.boneThreshold = range0
    #RemoveIslandsEffect.removeIslandsMorphologyDecruft(self.__roiVolume.GetImageData(),0,self.__corticalNode)
    #RemoveIslandsEffect.removeIslandsMorphologyDecruft(self.__roiVolume.GetImageData(),self.__corticalNode,0)

    #self.removeIslandsMorphologyDecruft(self.__roiVolume.GetImageData(),range0,range1)

    # update the label volume accordingly
    thresh = vtk.vtkImageThreshold()
    thresh.SetInputData(self.__roiVolume.GetImageData())
    thresh.ThresholdBetween(range0, range1)
    thresh.SetInValue(10)
    thresh.SetOutValue(0)
    thresh.ReplaceOutOn()
    thresh.ReplaceInOn()
    thresh.Update()

    if self.__corticalNode != None:
        self.__corticalNode.SetAndObserveImageData(thresh.GetOutput())
    '''
    dilater = slicer.vtkImageErode()
    dilaterImage = vtk.vtkImageData()
    dilaterImage.DeepCopy(thresh.GetOutput())
    dilater.SetInputData(dilaterImage)
    for i in range(3):
        dilater.SetForeground( 0 )
        dilater.SetBackground( 1 )
        dilater.SetNeighborTo8()
        dilater.Update()
        dilaterImage.DeepCopy(dilater.GetOutput())

    eroder = slicer.vtkImageErode()
    eroderImage = vtk.vtkImageData()
    eroderImage.DeepCopy(dilaterImage.GetOutput())
    eroder.SetInputData(eroderImage)
    for i in range(3):
        eroder.SetForeground( 1 )
        eroder.SetBackground( 0 )
        eroder.SetNeighborTo8()
        eroder.Update()
        eroderImage.DeepCopy(eroder.GetOutput())
    '''


    self.__yellowController.setSliceOffsetValue(y)
    self.__greenController.setSliceOffsetValue(g)
    self.__redController.setSliceOffsetValue(r)


  '''
  def onCancellousChanged(self):
    r = self.__redLogic.GetSliceOffset()
    y = self.__yellowLogic.GetSliceOffset()
    g = self.__greenLogic.GetSliceOffset()

    #Helper.SetLabelVolume(None)
    labelsColorNode = slicer.modules.colors.logic().GetColorTableNodeID(10)
    self.__cancellousNode.GetDisplayNode().SetAndObserveColorNodeID(labelsColorNode)
    Helper.SetLabelVolume(self.__cancellousNode.GetID())

    #self.__corticalRange.minimumValue = self.__cancellousRange.maximumValue

    range0 = self.__cancellousRange.minimumValue
    range1 = self.__cancellousRange.maximumValue



    # update the label volume accordingly
    thresh = vtk.vtkImageThreshold()
    thresh.SetInputData(self.__roiVolume.GetImageData())
    thresh.ThresholdBetween(range0, range1)
    thresh.SetInValue(1)
    thresh.SetOutValue(0)
    thresh.ReplaceOutOn()
    thresh.ReplaceInOn()
    thresh.Update()

    self.__cancellousNode.SetAndObserveImageData(thresh.GetOutput())

    self.__yellowController.setSliceOffsetValue(y)
    self.__greenController.setSliceOffsetValue(g)
    self.__redController.setSliceOffsetValue(r)
  '''
  def redsliderValueChanged(self, value):
      #print value
      #print self.oldPosition

      transform = vtk.vtkTransform()
      redSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeRed')
      transform.SetMatrix(redSlice.GetSliceToRAS())
      transform.RotateX(value - self.oldRedPosition)
      redSlice.GetSliceToRAS().DeepCopy(transform.GetMatrix())
      redSlice.UpdateMatrices()
      self.oldRedPosition = value

  def yellowsliderValueChanged(self, value):
      #print value
      #print self.oldPosition

      transform = vtk.vtkTransform()
      redSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeYellow')
      transform.SetMatrix(redSlice.GetSliceToRAS())
      transform.RotateY(value - self.oldYellowPosition)
      redSlice.GetSliceToRAS().DeepCopy(transform.GetMatrix())
      redSlice.UpdateMatrices()
      self.oldYellowPosition = value

  def greensliderValueChanged(self, value):
      #print value
      #print self.oldPosition

      transform = vtk.vtkTransform()
      redSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeGreen')
      transform.SetMatrix(redSlice.GetSliceToRAS())
      transform.RotateZ(value - self.oldGreenPosition)
      redSlice.GetSliceToRAS().DeepCopy(transform.GetMatrix())
      redSlice.UpdateMatrices()
      self.oldGreenPosition = value

  def stopWatchTimerCount(self):

    self.timerCount = self.timerCount + 1
    #print self.timerCount
    #v=slicer.app.layoutManager().threeDWidget(0).threeDView()
    #v=slicer.app.layoutManager().sliceWidget('Red').sliceView()
    #v.cornerAnnotation().SetText(0,str(c).strip('[]'))
    #v.cornerAnnotation().SetText(2,"Time Remaining: " + str(120.0 - float(self.timerCount/10.0)) + " s")
    #v.forceRender()

  def addFiducials(self):
      if self.startCount == 0:
        self.begin()
        self.startCount = 1
        self.identify.setText("Stop Identifying")
        #self.resetButton.enabled = True
        #msgOne = qt.QMessageBox()
        #msgOne.setText('Click on Known Vertebral Body')
        #msgOne.show()
        msgOne = qt.QMessageBox.warning( self, 'Identify Levels', 'Click on Known Vertebral Body' )

      elif self.startCount == 1:
        self.stop()
        self.startCount = 0
        self.identify.setText("Label Vertebrae")
        markup = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode1')
        markup.SetLocked(1)
        #self.identify.enabled(False)

  def loadLandmarks(self):
    a = slicer.mrmlScene.GetNodesByName('Vertebrae Labels')
    b = a.GetItemAsObject(0)
    b.RemoveAllMarkups()
    rownum = 0
    if (slicer.modules.PedicleScrewSimulatorWidget.loginStep.savedWorkflow == 1 and 
       slicer.modules.PedicleScrewSimulatorWidget.loginStep.savedVertebraLabels and
       slicer.modules.PedicleScrewSimulatorWidget.loginStep.savedVertebraPositions):
        self.boneThreshold = float(slicer.modules.PedicleScrewSimulatorWidget.loginStep.savedThreshold)
        self.__corticalRange.minimum = 0
        self.__corticalRange.minimumValue = self.boneThreshold
        self.labelFile.setText(slicer.modules.PedicleScrewSimulatorWidget.loginStep.savedCasePath)
        self.identify.enabled = False
        labels = slicer.modules.PedicleScrewSimulatorWidget.loginStep.savedVertebraLabels
        positions = slicer.modules.PedicleScrewSimulatorWidget.loginStep.savedVertebraPositions
        for number in range(len(labels)):
            c = positions[number]
            c = c.split(",")
            b.AddFiducial(float(c[0]),float(c[1]),float(c[2]))
            b.SetNthFiducialLabel(rownum,labels[number])
            b.SetNthMarkupLocked(rownum,1)
            self.vSelector.addItem(labels[number])

            if rownum != 0:
                if float(c[2]) > self.highCoord[2]:
                    self.highCoord = (float(c[0]),float(c[1]),float(c[2]))
                elif float(c[2]) < self.lowCoord[2]:
                    self.lowCoord = (float(c[0]),float(c[1]),float(c[2]))
            else:
                self.highCoord = (float(c[0]),float(c[1]),float(c[2]))
                self.lowCoord = (float(c[0]),float(c[1]),float(c[2]))
            rownum += 1
        self.level_chosen()
        self.addCropFiducials()
        self.autoROI()
    else:
        #path = qt.QFileDialog.getOpenFileName(self, "Open Data File", "", "CSV data files (*.csv)")
        #path = "D:\\Images\\SpineChallenge\\case1-2\\case1_lm2.csv"
        imageName = self.__baselineVolume.GetName()
        caseLandmarksPath = os.path.join(os.path.dirname(__file__), 'cases/' + imageName + "_lm.csv")
        print os.path.isfile(caseLandmarksPath)
        #path = "E:\\Research\\Imaging\\case1-2\\" + imageName + "_lm.csv"
        rownum = 0
        #self.highCoord = 0
        #self.lowCoord = 0
        if os.path.isfile(caseLandmarksPath) != False:
            self.labelFile.setText(caseLandmarksPath)
            self.identify.enabled = False
            with open(caseLandmarksPath) as stream:
                reader = csv.reader(stream)
                for row in reader:
                    #rowdata = []
                    #print i
                    #print row
                    name = row[0]
                    print name
                    if name == 'Threshold':
                        self.boneThreshold = int(row[1])
                        self.__corticalRange.minimum = 0
                        self.__corticalRange.minimumValue = self.boneThreshold
                    #position = (row[1],row[2],row[3])
                    else:
                        b.AddFiducial(float(row[1]),float(row[2]),float(row[3]))
                        b.SetNthFiducialLabel(rownum,name)
                        b.SetNthMarkupLocked(rownum,1)
                        self.vSelector.addItem(name)

                        if rownum != 0:
                            if float(row[3]) > self.highCoord[2]:
                                self.highCoord = (float(row[1]),float(row[2]),float(row[3]))
                            elif float(row[3]) < self.lowCoord[2]:
                                self.lowCoord = (float(row[1]),float(row[2]),float(row[3]))
                        else:
                            self.highCoord = (float(row[1]),float(row[2]),float(row[3]))
                            self.lowCoord = (float(row[1]),float(row[2]),float(row[3]))
                        rownum += 1
                    '''
                    if name == 'L3':
                        markup = slicer.modules.markups.logic()
                        markup.AddNewFiducialNode('Vertebra Reference')
                        slicer.modules.markups.logic().AddFiducial()
                        vr = slicer.mrmlScene.GetNodesByName('Vertebra Reference')
                        c = vr.GetItemAsObject(0)
                        dNode = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsDisplayNode3')
                        dNode.SetSelectedColor(0, 1, 0.2)
                        dNode.SetGlyphScale(5)
                        dNode.SetTextScale(5)
                        c.SetNthFiducialLabel(0,"L3")
                        c.SetNthFiducialPosition(0,float(row[1]),float(row[2])-50,float(row[3]))
                    '''
                    '''
                    for column in reader:
                        item = self.screwTable.item(row, column)
                        #print item
                        if item is not None:
                            rowdata.append(unicode(item.text()).encode('utf8'))
                        else:
                            rowdata.append('')
                    #x = range(0, 10)
                    screwValues = self.screwContact[row]
                    for j in range(len(self.screwContact[row])):
                        rowdata.append(unicode(int(screwValues[j])).encode('utf8'))
                    writer.writerow(rowdata)
                    '''
            self.level_chosen()
            self.addCropFiducials()
            self.autoROI()
        else:
            self.labelFile.setText("No File Found")
            #self.fidObserve = self.fidNode.AddObserver(self.fidNode.PointModifiedEvent, self.addIdentityFiducial)
            print "observer added"
            #fiducialNode = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode1')
            self.fidObserve = self.fidNode.AddObserver(self.fidNode.MarkupAddedEvent, self.addIdentityFiducial)
            #self.fidObserve = fiducialNode.AddObserver('ModifiedEvent', self.addIdentityFiducial)

  def addVertebraeLabels(self):
    if self.labels == 0:
        self.labels = 1
        slicer.modules.markups.logic().AddFiducial()
        slicer.modules.markups.logic().AddFiducial()
        slicer.modules.markups.logic().AddFiducial()
        slicer.modules.markups.logic().AddFiducial()
        slicer.modules.markups.logic().AddFiducial()
        slicer.modules.markups.logic().AddFiducial()
        slicer.modules.markups.logic().AddFiducial()
        slicer.modules.markups.logic().AddFiducial()
        slicer.modules.markups.logic().AddFiducial()
        slicer.modules.markups.logic().AddFiducial()
        slicer.modules.markups.logic().AddFiducial()
        slicer.modules.markups.logic().AddFiducial()
        slicer.modules.markups.logic().AddFiducial()
        #slicer.modules.markups.logic().AddFiducial()
        a = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode1')
        a.SetNthFiducialLabel(0,"T6")
        a.SetNthFiducialLabel(1,"T7")
        a.SetNthFiducialLabel(2,"T8")
        a.SetNthFiducialLabel(3,"T9")
        a.SetNthFiducialLabel(4,"T10")
        a.SetNthFiducialLabel(5,"T11")
        a.SetNthFiducialLabel(6,"T12")
        a.SetNthFiducialLabel(7,"L1")
        a.SetNthFiducialLabel(8,"L2")
        a.SetNthFiducialLabel(9,"L3")
        a.SetNthFiducialLabel(10,"L4")
        a.SetNthFiducialLabel(11,"L5")
        a.SetNthFiducialLabel(12,"Sacrum")
        a.SetNthFiducialPosition(12,-7.7,93.2,-773.3)
        a.SetNthFiducialPosition(11,-7.7,130.9,-746.6)
        a.SetNthFiducialPosition(10,-7.7,141.2,-709.8)
        a.SetNthFiducialPosition(9,-7.7,138.5,-672.6)
        a.SetNthFiducialPosition(8,-7.7,126.6,-635.5)
        a.SetNthFiducialPosition(7,-7.7,114.2,-603.2)
        a.SetNthFiducialPosition(6,-7.7,103.5,-571.9)
        a.SetNthFiducialPosition(5,-7.7,95.4,-544.5)
        a.SetNthFiducialPosition(4,-7.7,90.5,-515.9)
        a.SetNthFiducialPosition(3,-7.7,86.8,-488.9)
        a.SetNthFiducialPosition(2,-7.7,83.5,-465.8)
        a.SetNthFiducialPosition(1,-7.7,82.5,-441.1)
        a.SetNthFiducialPosition(0,-7.7,83.5,-417.4)

        self.addCropFiducials()

  def begin(self):
      selectionNode = slicer.mrmlScene.GetNodeByID("vtkMRMLSelectionNodeSingleton")
      # place rulers
      selectionNode.SetReferenceActivePlaceNodeClassName("vtkMRMLMarkupsFiducialNode")
      # to place ROIs use the class name vtkMRMLAnnotationROINode
      interactionNode = slicer.mrmlScene.GetNodeByID("vtkMRMLInteractionNodeSingleton")
      placeModePersistence = 1
      interactionNode.SetPlaceModePersistence(placeModePersistence)
      # mode 1 is Place, can also be accessed via slicer.vtkMRMLInteractionNode().Place
      interactionNode.SetCurrentInteractionMode(1)

  def stop(self):
      selectionNode = slicer.mrmlScene.GetNodeByID("vtkMRMLSelectionNodeSingleton")
      # place rulers
      selectionNode.SetReferenceActivePlaceNodeClassName("vtkMRMLMarkupsFiducialNode")
      # to place ROIs use the class name vtkMRMLAnnotationROINode
      interactionNode = slicer.mrmlScene.GetNodeByID("vtkMRMLInteractionNodeSingleton")
      placeModePersistence = 1
      interactionNode.SetPlaceModePersistence(placeModePersistence)
      # mode 1 is Place, can also be accessed via slicer.vtkMRMLInteractionNode().Place
      interactionNode.SetCurrentInteractionMode(2)

  def addIdentityFiducial(self, observer, event):

        #print event
        #print "MODIFIED"
        #labels = slicer.mrmlScene.GetNodesByName('Vertebrae Labels')
        #self.fiducial = labels.GetItemAsObject(0)
        #self.fiducial = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode1')
        self.fidNumber = self.fidNode.GetNumberOfFiducials()
        self.fidPosition = [0,0,0]
        self.fidNode.GetNthFiducialPosition(self.fidNumber-1,self.fidPosition)
        print "Label added"
        print self.fidNumber
        print self.fidPosition
        if self.fidNumber == 1 and self.idenCount == 0:
            self.fidNode.SetNthFiducialLabel(0, "Level Chosen")
            qW = qt.QWidget()
            msgTwo = qt.QInputDialog(qW)
            startLevel = msgTwo.getItem(qW,"Identify Levels","Which level did you identify?",["C1","C2","C3","C4","C5","C6","C7","T1","T2","T3","T4","T5","T6","T7","T8","T9","T10","T11","T12","L1", "L2", "L3", "L4", "L5","S1"],0)
            self.idenCount = 1
            self.levelIndex = self.levels.index(startLevel)
            #print self.levelIndex
            self.fidNode.SetNthFiducialLabel(0, startLevel)
            self.vSelector.addItem(startLevel)
            self.fidNode.GetNthFiducialPosition(0,self.highCoord)
            self.fidNode.GetNthFiducialPosition(0,self.lowCoord)
            self.highIndex = self.levelIndex
            self.lowIndex = self.levelIndex

        elif self.fidPosition[2] < self.lowCoord[2]:
            #print self.lowIndex
            self.fidNode.SetNthFiducialLabel(self.fidNumber - 1, self.levels[self.lowIndex + 1])
            self.vSelector.addItem(self.levels[self.lowIndex + 1])
            self.lowIndex += 1
            self.lowCoord = self.fidPosition

            #print self.lowIndex
        elif self.fidPosition[2] > self.highCoord[2]:
            #print self.highCoord
            #print self.fidPosition
            self.fidNode.SetNthFiducialLabel(self.fidNumber - 1, self.levels[self.highIndex - 1])
            self.vSelector.addItem(self.levels[self.highIndex - 1])
            self.highIndex -= 1
            self.highCoord = self.fidPosition

        #self.modCount = 0
        self.autoROI()
        #self.addCropFiducials()

  def clipBody(self,inputModel):
        roi = self.__roiSelector.currentNode()
        collectionS = slicer.mrmlScene.GetNodesByName('clipped_model')
        clippedModel = collectionS.GetItemAsObject(0)
        if clippedModel != None:
            slicer.mrmlScene.RemoveNode(clippedModel)

        clip = vtk.vtkClipPolyData()
        clip.SetInputData(inputModel.GetPolyData())
        clipBox = vtk.vtkBox()
        #clipROI = slicer.mrmlScene.GetNodeByID('vtkMRMLAnnotationROINode1')
        bounds=[0,0,0,0,0,0]
        roi.GetRASBounds(bounds)
        clipBox.SetBounds(bounds)
        clip.SetClipFunction(clipBox)
        clip.InsideOutOff()
        clip.Update()
        clipModel = slicer.vtkMRMLModelNode()
        clipModel.SetName('clipped_model')
        clipModel.SetAndObservePolyData(clip.GetOutput())
        slicer.mrmlScene.AddNode(clipModel)
        clipModelDisplay = slicer.vtkMRMLModelDisplayNode()
        slicer.mrmlScene.AddNode(clipModelDisplay)
        clipModel.SetAndObserveDisplayNodeID(clipModelDisplay.GetID())
        clipModelDisplay.SetColor(0.69, 0.47, 0.39)
        #roi.SetDisplayVisibility(0)

  def level_chosen(self):
    coords = [0,0,0]
    level = self.vSelector.currentIndex
    self.fidNode.GetNthFiducialPosition(level,coords)

    lm = slicer.app.layoutManager()
    redWidget = lm.sliceWidget('Red')
    redController = redWidget.sliceController()

    yellowWidget = lm.sliceWidget('Yellow')
    yellowController = yellowWidget.sliceController()

    greenWidget = lm.sliceWidget('Green')
    greenController = greenWidget.sliceController()

    yellowController.setSliceOffsetValue(coords[0])
    greenController.setSliceOffsetValue(coords[1])
    redController.setSliceOffsetValue(coords[2])

    for i in range(0,3):
        transform = vtk.vtkTransform()
        transform2 = vtk.vtkMatrix4x4()
        #transform2 = screwTransform.GetMatrixTransformToParent()

        #print "Fiducial Transform"
        #print transform
        if i == 0:
            viewSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeRed')
            transform.SetMatrix(viewSlice.GetSliceToRAS())
            #transform.RotateX(-x + self.oldX)
            transform2.DeepCopy(transform.GetMatrix())
            transform2.SetElement(0,3,coords[0])
            transform2.SetElement(1,3,coords[1])
            transform2.SetElement(2,3,coords[2])
            transform.SetMatrix(transform2)

        elif i == 1:
            viewSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeYellow')
            transform.SetMatrix(viewSlice.GetSliceToRAS())
            #transform.RotateY(-z + self.oldZ)
            transform2.DeepCopy(transform.GetMatrix())
            transform2.SetElement(0,3,coords[0])
            transform2.SetElement(1,3,coords[1])
            transform2.SetElement(2,3,coords[2])
            transform.SetMatrix(transform2)

        elif i == 2:
            viewSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeGreen')
            transform.SetMatrix(viewSlice.GetSliceToRAS())
            #transform.RotateX(x - self.oldX)
            #transform.RotateY(z - self.oldZ)
            transform2.DeepCopy(transform.GetMatrix())
            transform2.SetElement(0,3,coords[0])
            transform2.SetElement(1,3,coords[1])
            transform2.SetElement(2,3,coords[2])
            transform.SetMatrix(transform2)

        #print "Slice Transform"
        #print viewSlice.GetSliceToRAS()

        #transform.Translate(value - self.oldPosition)
        viewSlice.GetSliceToRAS().DeepCopy(transform.GetMatrix())
        viewSlice.UpdateMatrices()

  def region_chosen(self):
    roi = self.__roiSelector.currentNode()
    '''
    #inputModel = slicer.mrmlScene.GetNodeByID('vtkMRMLModelNode4')
    #print self.rSelector.currentText
    if self.rSelector.currentText == 'Upper Thoracic':
        self.vSelector.setCurrentIndex(1)
        roi.SetXYZ([17,-13,1371])
        roi.SetRadiusXYZ([37,60,78])
    elif self.rSelector.currentText == 'Lower Thoracic':
        self.vSelector.setCurrentIndex(6)
        roi.SetXYZ([9,-2,1206])
        roi.SetRadiusXYZ([31,60,90])
    elif self.rSelector.currentText == 'Lumbar':
        #self.vSelector.setCurrentIndex(13)
        #roi.SetXYZ([7,12,1142])
        #roi.SetRadiusXYZ([29,60,90])
        roi.SetXYZ([-5.8,77.6,-593.2])
        roi.SetRadiusXYZ([67,70,216])
    elif self.rSelector.currentText == 'Sacral':
        self.vSelector.setCurrentIndex(18)
    '''
    #self.clipBody(inputModel)
    #inputModel.SetDisplayVisibility(0)

  def autoROI(self):
    volume = slicer.mrmlScene.GetNodeByID('vtkMRMLScalarVolumeNode1')
    bounds = [0,0,0,0,0,0]
    volume.GetRASBounds(bounds)
    roi = self.__roiSelector.currentNode()
    #roi.SetXYZ([(bounds[0]+bounds[1])/2,(bounds[2]+bounds[3])/2,(bounds[4]+bounds[5])/2])
    #roi.SetRadiusXYZ([abs(bounds[0]+bounds[1])/2,abs(bounds[2]+bounds[3])/2,abs(bounds[4]+bounds[5])/2])
    '''
    roiCenter = [0,0,0]
    roiRadius = [0,0,0]

    #highCoord = slicer.modules.PedicleScrewSimulator_v3Widget.identifyLevelsStep.highCoord
    #lowCoord = slicer.modules.PedicleScrewSimulator_v3Widget.identifyLevelsStep.lowCoord
    highCoord = [0,-60,20]
    lowCoord = [0, -55, -100]
    '''
    xCenter = self.highCoord[0]
    yCenter = self.highCoord[1]
    zCenter = (self.highCoord[2]+self.lowCoord[2])/2
    xRadius = 50
    yRadius = 50
    zRadius = (self.highCoord[2]-zCenter) + 10

    roiCenter = [xCenter,yCenter,zCenter]
    roiRadius = [xRadius,yRadius,zRadius]

    #roiCenter = [-19,-80,-40]
    #roiRadius = [36,40,70]

    roi.SetXYZ(roiCenter)
    roi.SetRadiusXYZ(roiRadius)

  #called when ROI bounding box is altered
  def onROIChanged(self):
    #read ROI node from combobox
    roi = self.__roiSelector.currentNode()
    #print "altered"
    #self.volume = slicer.mrmlScene.GetNodeByID('vtkMRMLScalarVolumeNode2')
    #self.volume.SetAndObserveTransformNodeID(self.transformBody.GetID())

    if roi != None:
      self.__roi = roi

      # create VR node first time a valid ROI is selected
      self.InitVRDisplayNode()

      # update VR settings each time ROI changes
      pNode = self.parameterNode()
      # get scalar volume node loaded in previous step
      v = slicer.mrmlScene.GetNodeByID(pNode.GetParameter('baselineVolumeID'))

      #set parameters for VR display node
      self.__vrDisplayNode.SetAndObserveROINodeID(roi.GetID())
      self.__vrDisplayNode.SetCroppingEnabled(1)
      self.__vrDisplayNode.VisibilityOn()

      #transform ROI
      #roi.SetAndObserveTransformNodeID(self.__roiTransformNode.GetID())


      if self.__roiObserverTag != None:
        self.__roi.RemoveObserver(self.__roiObserverTag)

      #add observer to ROI. call self.processROIEvents if ROI is altered
      self.__roiObserverTag = self.__roi.AddObserver('ModifiedEvent', self.processROIEvents)

      #enable click and drag functions on ROI
      #roi.SetInteractiveMode(1)

      #connect ROI widget to ROI
      self.__roiWidget.setMRMLAnnotationROINode(roi)
      self.__roi.SetDisplayVisibility(1)

      #inputModel = slicer.mrmlScene.GetNodeByID('vtkMRMLModelNode4')
      #self.clipBody(inputModel)
      roiCenter=[0,0,0]
      roi.GetXYZ(roiCenter)
      camera = slicer.mrmlScene.GetNodeByID('vtkMRMLCameraNode1')
      camera.SetFocalPoint(roiCenter)
      camera.SetPosition(roiCenter[0],-600,roiCenter[2])
      camera.SetViewUp([-1,0,0])

      if self.entry == 1:
        #self.autoROI()
        self.__roiWidget.setInteractiveMode(0)
        self.entry = 2


      #self.__roiWidget.setInteractiveMode(1)


  def markupsInROIBox(self):
    # determine the markups in the ROI box
    roiBounds = [0,0,0,0,0,0]
    self.__roi.GetRASBounds(roiBounds)
    upperBound = roiBounds[5]
    lowerBound = roiBounds[4]
    #print upperBound
    #print lowerBound
    upperLabel = "C1"
    lowerLabel = "C1"
    position = [0,0,0]
    indexs = []
    positions = []
    #self.fiducial = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode1')
    #self.fidNumber = self.fiducial.GetNumberOfFiducials()
    if self.fidNumber > 0:
        for i in range (0,self.fidNumber):
            self.fidNode.GetNthFiducialPosition(i,position)
            if position[2] < upperBound and position[2] > lowerBound:
                indexs.append(self.fidNode.GetNthFiducialLabel(i))
                positions.append(position[2])

        maxMarkup = positions.index(max(positions))
        minMarkup = positions.index(min(positions))
        upperLabel = indexs[maxMarkup]
        lowerLabel = indexs[minMarkup]
        #print "upper" + upperLabel
        #print "lower" + lowerLabel
    #self.uSelector.setCurrentIndex(self.levels.index(lowerLabel))
    #self.lSelector.setCurrentIndex(self.levels.index(upperLabel))

  def adjustFidCenter(self, center):
    position = [0,0,0]
    #self.fiducial = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode1')
    #self.fidNumber = self.fiducial.GetNumberOfFiducials()
    '''
    for i in range (0,self.fidNumber):
      self.fiducial.GetNthFiducialPosition(i,position)
      self.fiducial.SetNthFiducialPosition(i,center,position[1], position[2])
    '''

  def processROIEvents(self,node,event):
    # get the range of intensities inside the ROI

    # get the IJK bounding box of the voxels inside ROI
    roiCenter = [0,0,0]
    roiRadius = [0,0,0]

    #get center coordinate
    self.__roi.GetXYZ(roiCenter)
    #print roiCenter


    #change slices to center of ROI
    lm = slicer.app.layoutManager()
    redWidget = lm.sliceWidget('Red')
    redController = redWidget.sliceController()

    yellowWidget = lm.sliceWidget('Yellow')
    yellowController = yellowWidget.sliceController()

    greenWidget = lm.sliceWidget('Green')
    greenController = greenWidget.sliceController()

    #yellowController.setSliceOffsetValue(roiCenter[0])
    #greenController.setSliceOffsetValue(roiCenter[1])
    #redController.setSliceOffsetValue(roiCenter[2])

    fidNumber = self.fidNode.GetNumberOfFiducials()
    fidPosition = [0,0,0]
    self.fidNode.GetNthFiducialPosition(fidNumber-1,fidPosition)

    yellowController.setSliceOffsetValue(fidPosition[0])
    greenController.setSliceOffsetValue(fidPosition[1])
    redController.setSliceOffsetValue(fidPosition[2])

    #get radius
    self.__roi.GetRadiusXYZ(roiRadius)

    #get IJK coordinates of 8 corners of ROI
    roiCorner1 = [roiCenter[0]+roiRadius[0],roiCenter[1]+roiRadius[1],roiCenter[2]+roiRadius[2],1]
    roiCorner2 = [roiCenter[0]+roiRadius[0],roiCenter[1]+roiRadius[1],roiCenter[2]-roiRadius[2],1]
    roiCorner3 = [roiCenter[0]+roiRadius[0],roiCenter[1]-roiRadius[1],roiCenter[2]+roiRadius[2],1]
    roiCorner4 = [roiCenter[0]+roiRadius[0],roiCenter[1]-roiRadius[1],roiCenter[2]-roiRadius[2],1]
    roiCorner5 = [roiCenter[0]-roiRadius[0],roiCenter[1]+roiRadius[1],roiCenter[2]+roiRadius[2],1]
    roiCorner6 = [roiCenter[0]-roiRadius[0],roiCenter[1]+roiRadius[1],roiCenter[2]-roiRadius[2],1]
    roiCorner7 = [roiCenter[0]-roiRadius[0],roiCenter[1]-roiRadius[1],roiCenter[2]+roiRadius[2],1]
    roiCorner8 = [roiCenter[0]-roiRadius[0],roiCenter[1]-roiRadius[1],roiCenter[2]-roiRadius[2],1]

    #get RAS transformation matrix of scalar volume and convert it to IJK matrix
    ras2ijk = vtk.vtkMatrix4x4()
    self.__baselineVolume.GetRASToIJKMatrix(ras2ijk)

    roiCorner1ijk = ras2ijk.MultiplyPoint(roiCorner1)
    roiCorner2ijk = ras2ijk.MultiplyPoint(roiCorner2)
    roiCorner3ijk = ras2ijk.MultiplyPoint(roiCorner3)
    roiCorner4ijk = ras2ijk.MultiplyPoint(roiCorner4)
    roiCorner5ijk = ras2ijk.MultiplyPoint(roiCorner5)
    roiCorner6ijk = ras2ijk.MultiplyPoint(roiCorner6)
    roiCorner7ijk = ras2ijk.MultiplyPoint(roiCorner7)
    roiCorner8ijk = ras2ijk.MultiplyPoint(roiCorner8)

    lowerIJK = [0, 0, 0]
    upperIJK = [0, 0, 0]

    lowerIJK[0] = min(roiCorner1ijk[0],roiCorner2ijk[0],roiCorner3ijk[0],roiCorner4ijk[0],roiCorner5ijk[0],roiCorner6ijk[0],roiCorner7ijk[0],roiCorner8ijk[0])
    lowerIJK[1] = min(roiCorner1ijk[1],roiCorner2ijk[1],roiCorner3ijk[1],roiCorner4ijk[1],roiCorner5ijk[1],roiCorner6ijk[1],roiCorner7ijk[1],roiCorner8ijk[1])
    lowerIJK[2] = min(roiCorner1ijk[2],roiCorner2ijk[2],roiCorner3ijk[2],roiCorner4ijk[2],roiCorner5ijk[2],roiCorner6ijk[2],roiCorner7ijk[2],roiCorner8ijk[2])

    upperIJK[0] = max(roiCorner1ijk[0],roiCorner2ijk[0],roiCorner3ijk[0],roiCorner4ijk[0],roiCorner5ijk[0],roiCorner6ijk[0],roiCorner7ijk[0],roiCorner8ijk[0])
    upperIJK[1] = max(roiCorner1ijk[1],roiCorner2ijk[1],roiCorner3ijk[1],roiCorner4ijk[1],roiCorner5ijk[1],roiCorner6ijk[1],roiCorner7ijk[1],roiCorner8ijk[1])
    upperIJK[2] = max(roiCorner1ijk[2],roiCorner2ijk[2],roiCorner3ijk[2],roiCorner4ijk[2],roiCorner5ijk[2],roiCorner6ijk[2],roiCorner7ijk[2],roiCorner8ijk[2])

    #get image data of scalar volume
    image = self.__baselineVolume.GetImageData()

    #create image clipper
    clipper = vtk.vtkImageClip()
    clipper.ClipDataOn()
    clipper.SetOutputWholeExtent(int(lowerIJK[0]),int(upperIJK[0]),int(lowerIJK[1]),int(upperIJK[1]),int(lowerIJK[2]),int(upperIJK[2]))
    clipper.SetInputData(image)
    clipper.Update()
    
    #read upper and lower threshold values from clipped volume
    roiImageRegion = clipper.GetOutput()
    intRange = roiImageRegion.GetScalarRange()
    print intRange[0]
    threshDifference = -1000 - intRange[0]
    self.lThresh = intRange[0] + 1000
    self.uThresh = intRange[1] + threshDifference
    print "lower thresh" + str(int(self.lThresh))


    # finally, update the focal point to be the center of ROI
    '''

    '''
    #self.markupsInROIBox()
    self.adjustFidCenter(roiCenter[0])

  #set up VR
  def InitVRDisplayNode(self):
    #If VR node exists, load it from saved ID
    if self.__vrDisplayNode == None:
      pNode = self.parameterNode()
      vrNodeID = pNode.GetParameter('vrDisplayNodeID')
      if vrNodeID == '':
        self.__vrDisplayNode = slicer.modules.volumerendering.logic().CreateVolumeRenderingDisplayNode()
        #set rendering type
        #self.__vrDisplayNode = slicer.vtkMRMLGPUTextureMappingVolumeRenderingDisplayNode()
        self.__vrDisplayNode = slicer.vtkMRMLGPURayCastVolumeRenderingDisplayNode()
        slicer.mrmlScene.AddNode(self.__vrDisplayNode)
        self.__vrDisplayNode.SetExpectedFPS(15)
        self.__vrDisplayNode.UnRegister(slicer.modules.volumerendering.logic())
        v = slicer.mrmlScene.GetNodeByID(self.parameterNode().GetParameter('baselineVolumeID'))
        Helper.InitVRDisplayNode(self.__vrDisplayNode, v.GetID(), self.__roi.GetID())
        v.AddAndObserveDisplayNodeID(self.__vrDisplayNode.GetID())
      else:
        self.__vrDisplayNode = slicer.mrmlScene.GetNodeByID(vrNodeID)

    viewNode = slicer.util.getNode('vtkMRMLViewNode1')
    self.__vrDisplayNode.AddViewNodeID(viewNode.GetID())

    slicer.modules.volumerendering.logic().CopyDisplayToVolumeRenderingDisplayNode(self.__vrDisplayNode)
    '''
    #update opacity and color map
    self.__vrOpacityMap = self.__vrDisplayNode.GetVolumePropertyNode().GetVolumeProperty().GetScalarOpacity()
    self.__vrColorMap = self.__vrDisplayNode.GetVolumePropertyNode().GetVolumeProperty().GetRGBTransferFunction()

    #create new opacity map with voxels falling between upper and lower threshold values at 100% opacity. All others at 0%
    self.__vrOpacityMap.RemoveAllPoints()
    
    self.__vrOpacityMap.AddPoint(-3024 + self.lThresh,0)
    self.__vrOpacityMap.AddPoint(-86.9767+ self.lThresh,0)
    self.__vrOpacityMap.AddPoint(45.3791+ self.lThresh,0.169643)
    self.__vrOpacityMap.AddPoint(139.919+ self.lThresh,0.589286)
    self.__vrOpacityMap.AddPoint(347.907+ self.lThresh,0.607143)
    self.__vrOpacityMap.AddPoint(1224.16+ self.lThresh,0.607143)
    self.__vrOpacityMap.AddPoint(3071+ self.lThresh,0.616071)

    # setup color transfer function once
    # two points at 0 and 500 force all voxels to be same color (any two points will work)
    self.__vrColorMap.RemoveAllPoints()
    
    self.__vrColorMap.AddRGBPoint(-3024+ self.lThresh, 0,0,0)
    self.__vrColorMap.AddRGBPoint(-86.9767+ self.lThresh, 0,0.25098,1)
    self.__vrColorMap.AddRGBPoint(45.3791+ self.lThresh, 1,0,0)
    self.__vrColorMap.AddRGBPoint(139.919+ self.lThresh, 1,0.894893,0.894893)
    self.__vrColorMap.AddRGBPoint(347.907+ self.lThresh, 1,1,0.25098)
    self.__vrColorMap.AddRGBPoint(1224.16+ self.lThresh, 1,1,1)
    self.__vrColorMap.AddRGBPoint(3071+ self.lThresh, 0.827451,0.658824,1)

    '''
    presetsScene = slicer.modules.volumerendering.logic().GetPresetsScene()
    ctAAA = presetsScene.GetFirstNodeByName('CT-AAA2')
    volumeRenderingWidgetRep = slicer.modules.volumerendering.widgetRepresentation()
    volumeRenderingWidgetRep.applyPreset(ctAAA)
    

  def onEntry(self,comingFrom,transitionType):
    super(ApproachStep, self).onEntry(comingFrom, transitionType)

    # setup the interface
    lm = slicer.app.layoutManager()
    if lm == None:
        return
    #lm.setLayout(slicer.vtkMRMLLayoutNode.SlicerLayoutSideBySideView)

    customLayout = ("<layout type=\"horizontal\" split=\"true\" >"
        " <item>"
        "  <view class=\"vtkMRMLSliceNode\" singletontag=\"Red\">"
        "    <property name=\"orientation\" action=\"default\">Axial</property>"
        "    <property name=\"viewlabel\" action=\"default\">R</property>"
        "    <property name=\"viewcolor\" action=\"default\">#F34A33</property>"
        "  </view>"
        " </item>"
        " <item>"
        "  <view class=\"vtkMRMLSliceNode\" singletontag=\"Yellow\">"
        "    <property name=\"orientation\" action=\"default\">Axial</property>"
        "    <property name=\"viewlabel\" action=\"default\">Y</property>"
        "    <property name=\"viewcolor\" action=\"default\">#EDD54C</property>"
        "  </view>"
        " </item>"
        " <item>"
        "  <view class=\"vtkMRMLSliceNode\" singletontag=\"Green\">"
        "    <property name=\"orientation\" action=\"default\">Axial</property>"
        "    <property name=\"viewlabel\" action=\"default\">G</property>"
        "    <property name=\"viewcolor\" action=\"default\">#6EB04B</property>"
        "  </view>"
        " </item>"
        "</layout>")

    customLayoutId=502

    #layoutManager = slicer.app.layoutManager()
    lm.layoutLogic().GetLayoutNode().AddLayoutDescription(customLayoutId, customLayout)
    lm.setLayout(customLayoutId)

    #lm.setLayout(slicer.vtkMRMLLayoutNode.SlicerLayoutConventionalWidescreenView)
    # setup the interface



    markup = slicer.modules.markups.logic()
    markup.AddNewFiducialNode('Vertebrae Labels')
    landmarks = slicer.mrmlScene.GetNodesByName('Vertebrae Labels')
    self.fidNode = landmarks.GetItemAsObject(0)



    #fiducialNode = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode1')
    #self.fidObserve = fiducialNode.AddObserver('ModifiedEvent', self.addIdentityFiducial)

    slicer.app.applicationLogic().PropagateVolumeSelection(1)

    #create progress bar dialog
    self.progress = qt.QProgressDialog(slicer.util.mainWindow())
    self.progress.minimumDuration = 0
    self.progress.show()
    self.progress.setValue(0)
    self.progress.setMaximum(0)
    self.progress.setCancelButton(0)
    self.progress.setMinimumWidth(500)
    self.progress.setWindowModality(2)

    self.progress.setLabelText('Generating Volume Rendering...')
    slicer.app.processEvents(qt.QEventLoop.ExcludeUserInputEvents)
    self.progress.repaint()

    #read scalar volume node ID from previous step
    pNode = self.parameterNode()
    baselineVolume = Helper.getNodeByID(pNode.GetParameter('baselineVolumeID'))
    self.__baselineVolume = baselineVolume

    #print baselineVolume

    #if ROI was created previously, get its transformation matrix and update current ROI
    roiTransformID = pNode.GetParameter('roiTransformID')
    roiTransformNode = None

    if roiTransformID != '':
      roiTransformNode = Helper.getNodeByID(roiTransformID)
    else:
      roiTransformNode = slicer.vtkMRMLLinearTransformNode()
      slicer.mrmlScene.AddNode(roiTransformNode)
      pNode.SetParameter('roiTransformID', roiTransformNode.GetID())

    dm = vtk.vtkMatrix4x4()
    baselineVolume.GetIJKToRASDirectionMatrix(dm)
    dm.SetElement(0,3,0)
    dm.SetElement(1,3,0)
    dm.SetElement(2,3,0)
    dm.SetElement(0,0,abs(dm.GetElement(0,0)))
    dm.SetElement(1,1,abs(dm.GetElement(1,1)))
    dm.SetElement(2,2,abs(dm.GetElement(2,2)))
    roiTransformNode.SetAndObserveMatrixTransformToParent(dm)


    Helper.SetBgFgVolumes(pNode.GetParameter('baselineVolumeID'))
    Helper.SetLabelVolume(None)

    # use this transform node to align ROI with the axes of the baseline
    # volume
    roiTfmNodeID = pNode.GetParameter('roiTransformID')
    if roiTfmNodeID != '':
      self.__roiTransformNode = Helper.getNodeByID(roiTfmNodeID)
    else:
      Helper.Error('Internal error! Error code CT-S2-NRT, please report!')

    # get the roiNode from parameters node, if it exists, and initialize the
    # GUI
    self.updateWidgetFromParameterNode(pNode)
    #self.updateThresholdFromParameterNode(pNode)

    # start VR
    if self.__roi != None:
      self.__roi.SetDisplayVisibility(0)
      self.InitVRDisplayNode()

    #close progress bar
    self.progress.setValue(2)
    self.progress.repaint()
    slicer.app.processEvents(qt.QEventLoop.ExcludeUserInputEvents)
    self.progress.close()
    self.progress = None

    #turn off rendering to save memory
    slicer.mrmlScene.GetNodesByName('GPURayCastVolumeRendering').GetItemAsObject(0).SetVisibility(0)
    #pNode.SetParameter('currentStep', self.stepid)

    # Enable Slice Intersections
    viewNodes = slicer.mrmlScene.GetNodesByClass('vtkMRMLSliceCompositeNode')
    viewNodes.UnRegister(slicer.mrmlScene)
    viewNodes.InitTraversal()
    viewNode = viewNodes.GetNextItemAsObject()
    while viewNode:
        viewNode.SetSliceIntersectionVisibility(1)
        viewNode = viewNodes.GetNextItemAsObject()

    self.redTransform = vtk.vtkTransform()
    viewSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeRed')
    self.redTransform.SetMatrix(viewSlice.GetSliceToRAS())
    #print "Red Transform"
    #print self.redTransform
    self.yellowTransform = vtk.vtkTransform()
    viewSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeYellow')
    self.yellowTransform.SetMatrix(viewSlice.GetSliceToRAS())
    #print "Yellow Transform"
    #print self.yellowTransform
    self.greenTransform = vtk.vtkTransform()
    viewSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeGreen')
    self.greenTransform.SetMatrix(viewSlice.GetSliceToRAS())
    #print "Green Transform"
    #print self.greenTransform

    n =  self.__baselineVolume
    for color in ['Red', 'Yellow', 'Green']:
        a = slicer.app.layoutManager().sliceWidget(color).sliceLogic().GetSliceNode().GetFieldOfView()
        slicer.app.layoutManager().sliceWidget(color).sliceLogic().GetSliceCompositeNode().SetBackgroundVolumeID(n.GetID())
        slicer.app.layoutManager().sliceWidget(color).sliceLogic().GetSliceNode().SetFieldOfView(a[0],a[1],a[2])
        if color == 'Yellow' or color == 'Green':
            slicer.app.layoutManager().sliceWidget(color).sliceLogic().GetSliceNode().SetFieldOfView(a[0]*0.5,a[1]*0.5,a[2])
        else:
            slicer.app.layoutManager().sliceWidget(color).sliceLogic().GetSliceNode().SetFieldOfView(a[0]*0.7,a[1]*0.7,a[2])

    qt.QTimer.singleShot(0, self.killButton)

    self.stopWatchTimer.start(self.timerFreq)

    #self.addVertebraeLabels()
    '''
    self.rSelector.setCurrentIndex(3)
    self.region_chosen()
    self.vSelector.setCurrentIndex(8)
    self.level_chosen()
    self.rSelector.enabled = 0
    '''
    self.showThreshold()

    self.loadLandmarks()
    #print self.highCoord
    #self.autoROI()


    qt.QMessageBox.question(slicer.util.mainWindow(),
         'Step 2: Review Imaging', 'Use the mouse wheel to scroll through the image views. Jump to specific vertebral levels using the dropdown box.',
         qt.QMessageBox.Ok)


  def validate( self, desiredBranchId ):

    self.__parent.validate( desiredBranchId )
    roi = self.__roiSelector.currentNode()
    if roi == None:
      self.__parent.validationFailed(desiredBranchId, 'Error', 'Please define ROI!')

    volCollection = slicer.mrmlScene.GetNodesByClass('vtkMRMLScalarVolumeNode')
    volCheck = volCollection.GetItemAsObject(0)
    if volCheck != None:
      self.__parent.validationSucceeded('pass')
    else:
      self.__parent.validationSucceeded('fail')
      slicer.mrmlScene.Clear(0)


  def onExit(self, goingTo, transitionType):

    login = LoginStepModule.LoginStep(PedicleScrewSimulatorStep)
    filePath = login.saveFilePath
    if filePath != '':
      login.transposeSaveFile(filePath)
      login.addToSaveFile(filePath,[self.boneThreshold],1)
      labelFids = slicer.util.getNode('Vertebrae Labels')
      vertLabelList, vertPositionList, ras = [], [], [0,0,0]
      [vertLabelList.append(labelFids.GetNthFiducialLabel(i)) for i in range(labelFids.GetNumberOfFiducials())]
      for i in range(labelFids.GetNumberOfFiducials()):
        labelFids.GetNthFiducialPosition(i,ras)
        vertPositionList.append(str(ras).replace(' ', '').strip('[').strip(']'))
      login.addToSaveFile(filePath,vertLabelList,2)
      login.addToSaveFile(filePath,vertPositionList,3)
      login.transposeSaveFile(filePath)

    inputModel = slicer.mrmlScene.GetNodeByID('vtkMRMLModelNode4')
    #self.clipBody(inputModel)

    if goingTo.id() != 'Screw' and goingTo.id() != 'Login': # Change to next step
      return

    pNode = self.parameterNode()
    # TODO: add storeWidgetStateToParameterNode() -- move all pNode-related stuff
    # there?
    if self.__roi != None:
        self.__roi.RemoveObserver(self.__roiObserverTag)
        self.__roi.SetDisplayVisibility(0)

    if self.__roiSelector.currentNode() != None:
        pNode.SetParameter('roiNodeID', self.__roiSelector.currentNode().GetID())

    if self.__vrDisplayNode != None:
        #self.__vrDisplayNode.VisibilityOff()
        pNode.SetParameter('vrDisplayNodeID', self.__vrDisplayNode.GetID())

    if goingTo.id() == 'Screw': # Change to next step
        self.doStepProcessing()

    super(ApproachStep, self).onExit(goingTo, transitionType)
    '''
    presetsScene = slicer.modules.volumerendering.logic().GetPresetsScene()
    ctAAA = presetsScene.GetFirstNodeByName('CT-AAA2')
    volumeRenderingWidgetRep = slicer.modules.volumerendering.widgetRepresentation()
    volumeRenderingWidgetRep.applyPreset(ctAAA)
    '''
    print "approach time " + str(float(self.timerCount/10.0)) + "s"
    self.stopWatchTimer.stop()

    # Enable Slice Intersections
    viewNodes = slicer.mrmlScene.GetNodesByClass('vtkMRMLSliceCompositeNode')
    viewNodes.UnRegister(slicer.mrmlScene)
    viewNodes.InitTraversal()
    viewNode = viewNodes.GetNextItemAsObject()
    while viewNode:
        viewNode.SetSliceIntersectionVisibility(0)
        viewNode = viewNodes.GetNextItemAsObject()

    rulers = slicer.mrmlScene.GetNodesByClass('vtkMRMLAnnotationRulerNode')
    for x in range(0,rulers.GetNumberOfItems()):
        rulerX = rulers.GetItemAsObject(x)
        rulerX.SetDisplayVisibility(0)

    viewSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeRed')
    viewSlice.GetSliceToRAS().DeepCopy(self.redTransform.GetMatrix())
    viewSlice.UpdateMatrices()
    viewSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeYellow')
    viewSlice.GetSliceToRAS().DeepCopy(self.yellowTransform.GetMatrix())
    viewSlice.UpdateMatrices()
    viewSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeGreen')
    viewSlice.GetSliceToRAS().DeepCopy(self.greenTransform.GetMatrix())
    viewSlice.UpdateMatrices()

  def updateWidgetFromParameterNode(self, parameterNode):
    roiNodeID = parameterNode.GetParameter('roiNodeID')

    if roiNodeID != '':
      self.__roi = slicer.mrmlScene.GetNodeByID(roiNodeID)
      self.__roiSelector.setCurrentNode(Helper.getNodeByID(self.__roi.GetID()))
    else:
      roi = slicer.vtkMRMLAnnotationROINode()
      roi.Initialize(slicer.mrmlScene)
      parameterNode.SetParameter('roiNodeID', roi.GetID())
      self.__roiSelector.setCurrentNode(roi)

    #self.onROIChanged()

    baselineROIVolume = Helper.getNodeByID(parameterNode.GetParameter('baselineVolumeID'))
    baselineROIRange = baselineROIVolume.GetImageData().GetScalarRange()
    self.__roiVolume = baselineROIVolume

    #self.__corticalRange.minimum = baselineROIRange[0]
    #self.__corticalRange.minimum = 160
    self.__corticalRange.minimum = 0
    self.__corticalRange.maximum = baselineROIRange[1]

    #self.__cancellousRange.minimum = baselineROIRange[0]
    #self.__cancellousRange.maximum = baselineROIRange[1]


    vl = slicer.modules.volumes.logic()
    self.__corticalNode = vl.CreateAndAddLabelVolume(slicer.mrmlScene, baselineROIVolume, 'corticalROI_segmentation')
    #self.__cancellousNode = vl.CreateAndAddLabelVolume(slicer.mrmlScene, baselineROIVolume, 'cancellousROI_segmentation')

    Helper.SetLabelVolume(None)
    labelsColorNode = slicer.modules.colors.logic().GetColorTableNodeID(10)
    self.__corticalNode.GetDisplayNode().SetAndObserveColorNodeID(labelsColorNode)
    Helper.SetLabelVolume(self.__corticalNode.GetID())

    thresholdRange = str(0.5*(baselineROIRange[0]+baselineROIRange[1]))+','+str(baselineROIRange[1])
    if thresholdRange != '':
        rangeArray = string.split(thresholdRange, ',')

        #self.__corticalRange.minimumValue = float(rangeArray[0])
        self.__corticalRange.minimumValue = 150
        self.__corticalRange.maximumValue = float(rangeArray[1])

        #self.__cancellousRange.minimumValue = float(rangeArray[0])
        #self.__cancellousRange.maximumValue = float(rangeArray[1])
    else:
        Helper.Error('Unexpected parameter values!')

    #self.onCancellousChanged()
    self.onCorticalChanged()
    #print "label should be made"

  def grayModel(self, volumeNode):
    parameters = {}
    parameters["InputVolume"] = volumeNode.GetID()
    parameters["Threshold"] = self.boneThreshold
    print "Bone Threshold = " + str(self.boneThreshold)
    #parameters["InputVolume"] = self.__corticalNode
    #parameters["Threshold"] = 1
    #parameters["Name"] = "spine"
    outModel = slicer.vtkMRMLModelNode()
    slicer.mrmlScene.AddNode( outModel )
    parameters["OutputGeometry"] = outModel.GetID()
    grayMaker = slicer.modules.grayscalemodelmaker
    #outModel.GetDisplayNode().SetColor(0.95, 0.84, 0.57)
    return (slicer.cli.run(grayMaker, None, parameters, wait_for_completion=True))

  def addCropFiducials(self):
    landmarks = slicer.mrmlScene.GetNodesByName('Crop Landmarks')
    cropLandmarks = landmarks.GetItemAsObject(0)

    if cropLandmarks == None:
        markup = slicer.modules.markups.logic()
        markup.AddNewFiducialNode('Crop Landmarks')
    else:
        cropLandmarks.RemoveAllMarkups()
    self.fiducial = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode1')
    self.fidNumber = self.fiducial.GetNumberOfFiducials()
    for i in range(0,self.fidNumber):
        coords = [0,0,0]
        self.fiducial.GetNthFiducialPosition(i,coords)
        label = self.fiducial.GetNthFiducialLabel(i)
        if label[0] == 'L' or label[0] == 'S':
            if label[1] == '1' or label[1] == '2':
                slicer.modules.markups.logic().AddFiducial(coords[0],coords[1]-25,coords[2])
                slicer.modules.markups.logic().AddFiducial(coords[0]-25,coords[1]-30,coords[2])
                slicer.modules.markups.logic().AddFiducial(coords[0]+25,coords[1]-30,coords[2])
                slicer.modules.markups.logic().AddFiducial(coords[0]-45,coords[1]-200,coords[2])
                slicer.modules.markups.logic().AddFiducial(coords[0]+45,coords[1]-200,coords[2])
            else:
                slicer.modules.markups.logic().AddFiducial(coords[0],coords[1]-25,coords[2])
                slicer.modules.markups.logic().AddFiducial(coords[0]-35,coords[1]-30,coords[2])
                slicer.modules.markups.logic().AddFiducial(coords[0]+35,coords[1]-30,coords[2])
                slicer.modules.markups.logic().AddFiducial(coords[0]-50,coords[1]-200,coords[2])
                slicer.modules.markups.logic().AddFiducial(coords[0]+50,coords[1]-200,coords[2])
        elif label[0] == 'T':
            slicer.modules.markups.logic().AddFiducial(coords[0],coords[1]+10,coords[2])
            slicer.modules.markups.logic().AddFiducial(coords[0]-25,coords[1]-30,coords[2])
            slicer.modules.markups.logic().AddFiducial(coords[0]+25,coords[1]-30,coords[2])
            slicer.modules.markups.logic().AddFiducial(coords[0]-45,coords[1]-200,coords[2])
            slicer.modules.markups.logic().AddFiducial(coords[0]+45,coords[1]-200,coords[2])
    #a = slicer.modules.markups.logic()
    b = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode2')
    markup.SetAllMarkupsVisibility(b,0)

  def cropVolumeFromLandmarks(self, name, clipOutside):
    # Input Volume
    inputVolume = slicer.mrmlScene.GetNodeByID('vtkMRMLScalarVolumeNode1')

    # Create empty model node
    clippingModel = slicer.vtkMRMLModelNode()
    clippingModel.SetName('clipModel')
    slicer.mrmlScene.AddNode(clippingModel)

    # Create output volume
    outputVolume = slicer.vtkMRMLScalarVolumeNode()
    outputVolume.SetName(name)
    slicer.mrmlScene.AddNode(outputVolume)

    # Get crop landmarks
    landmarks = slicer.mrmlScene.GetNodesByName('Crop Landmarks')
    cropLandmarks = landmarks.GetItemAsObject(0)

    # Clip volume
    #logic = VolumeClipWithModelLogic()
    logic = VolumeClipWithModel.VolumeClipWithModelLogic()
    clipOutsideSurface = clipOutside
    fillValue = -1000
    logic.updateModelFromMarkup(cropLandmarks, clippingModel)
    logic.clipVolumeWithModel(inputVolume, clippingModel, clipOutsideSurface, fillValue, outputVolume)
    logic.showInSliceViewers(outputVolume, ["Red", "Yellow", "Green"])
    #clippingModel.SetDisplayVisibility(0)

  def doStepProcessing(self):
    landmarks = slicer.mrmlScene.GetNodesByName('Crop Landmarks')
    cropLandmarks = landmarks.GetItemAsObject(0)
    if cropLandmarks == None:
        self.addCropFiducials()
    '''
    prepare roi image for the next step
    '''
    #create progress bar dialog
    self.progress2 = qt.QProgressDialog(slicer.util.mainWindow())
    self.progress2.minimumDuration = 0
    self.progress2.show()
    self.progress2.setValue(0)
    self.progress2.setMaximum(0)
    self.progress2.setCancelButton(0)
    self.progress2.setMinimumWidth(500)
    self.progress2.setWindowModality(2)

    self.progress2.setLabelText('Generating Surface Model...')
    slicer.app.processEvents(qt.QEventLoop.ExcludeUserInputEvents)
    self.progress2.repaint()

    self.cropVolumeFromLandmarks('opening', True)
    self.cropVolumeFromLandmarks('body', False)
    vols = slicer.mrmlScene.GetNodesByName('opening')
    cropVolume = vols.GetItemAsObject(0)
    self.grayModel(cropVolume)
    outModel = slicer.mrmlScene.GetNodeByID('vtkMRMLModelNode6')
    outModel.GetDisplayNode().SetColor(1, 0.97, 0.79)

    logic = slicer.modules.volumerendering.logic()
    a = slicer.mrmlScene.GetNodesByName('body')
    b = a.GetItemAsObject(0)
    c = slicer.mrmlScene.GetNodesByName('GPURayCastVolumeRendering').GetItemAsObject(0)
    logic.UpdateDisplayNodeFromVolumeNode(c,b)
    d = slicer.mrmlScene.GetNodeByID('vtkMRMLAnnotationROINode1')

    c.SetAndObserveROINodeID(d.GetID())
    c.SetVisibility(1)

    #close progress bar
    self.progress2.setValue(2)
    self.progress2.repaint()
    slicer.app.processEvents(qt.QEventLoop.ExcludeUserInputEvents)
    self.progress2.close()
    self.progress2 = None

    '''
    presetsScene = slicer.modules.volumerendering.logic().GetPresetsScene()
    ctAAA = presetsScene.GetFirstNodeByName('CT-AAA2')
    volumeRenderingWidgetRep = slicer.modules.volumerendering.widgetRepresentation()
    volumeRenderingWidgetRep.applyPreset(ctAAA)
    '''
    #roi = slicer.mrmlScene.GetNodeByID('vtkMRMLAnnotationROINode2')
    #roi.SetDisplayVisibility(0)

    self.InitVRDisplayNode()

    #crop scalar volume
    pNode = self.parameterNode()

    slicer.mrmlScene.RemoveNode(self.__corticalNode)
    #slicer.mrmlScene.RemoveNode(self.__cancellousNode)

    #startVert = self.lSelector.currentText
    #instLength = str(self.levels.index(self.uSelector.currentText) - self.levels.index(self.lSelector.currentText))
    #approach = "Posterior"

    #pNode.SetParameter('vertebra', startVert)
    #pNode.SetParameter('inst_length', instLength)
    #pNode.SetParameter('approach', approach)

    #pNode.SetParameter('vertebra', self.vSelector.currentText)
    #pNode.SetParameter('inst_length', self.iSelector.currentText)
    #pNode.SetParameter('approach', self.aSelector.currentText)
    '''
    cropVolumeNode = slicer.vtkMRMLCropVolumeParametersNode()
    cropVolumeNode.SetScene(slicer.mrmlScene)
    cropVolumeNode.SetName('CropVolume_node')
    cropVolumeNode.SetIsotropicResampling(True)
    cropVolumeNode.SetSpacingScalingConst(0.5)
    slicer.mrmlScene.AddNode(cropVolumeNode)
    # TODO hide from MRML tree

    cropVolumeNode.SetInputVolumeNodeID(pNode.GetParameter('baselineVolumeID'))
    cropVolumeNode.SetROINodeID(pNode.GetParameter('roiNodeID'))
    # cropVolumeNode.SetAndObserveOutputVolumeNodeID(outputVolume.GetID())



    cropVolumeLogic = slicer.modules.cropvolume.logic()
    cropVolumeLogic.Apply(cropVolumeNode)

    # TODO: cropvolume error checking
    outputVolume = slicer.mrmlScene.GetNodeByID(cropVolumeNode.GetOutputVolumeNodeID())
    outputVolume.SetName("baselineROI")
    pNode.SetParameter('croppedBaselineVolumeID',cropVolumeNode.GetOutputVolumeNodeID())

    self.grayModel(outputVolume)


    '''
    #self.__vrDisplayNode = None

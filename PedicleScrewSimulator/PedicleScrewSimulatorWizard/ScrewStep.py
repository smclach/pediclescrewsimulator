from __main__ import qt, ctk, vtk, slicer

from PedicleScrewSimulatorStep import *
from Helper import *
import PythonQt
import math
import os
import time
import inspect
import csv
import LoginStep as LoginStepModule

class StopWatch:
	
	def __init__(self, parent=None):

		self.reset()

	def start(self):

		if (not self.running):

			self.startTime = time.time()
			#print("START: " + str(self.startTime))

		self.running = True

	def stop(self):

		if (self.running):

			self.time += time.time() - self.startTime
			#print("DELTA TIME: " + str(time.time() - self.startTime))

		self.running = False

	def reset(self):

		self.running = False

		self.startTime = 0.0
		self.time = 0.0
		
# Das class.

class ScrewStep(PedicleScrewSimulatorStep):
    
    
    def __init__( self, stepid ):
      

      self.initialize( stepid )
      self.setName( 'Step 3. Entry Point and Trajectory Selection' )
      self.setDescription( 'Use the screw insertion workflow panel to (1) add entry point locations followed by (2) insertion probe trajectory selection using the sliders to make angular adjustments. Once defined, insert the tool and proceed to the next entry point. The "active" entry point can be dragged to a new location if the tool is not inserted.' )
      self.screwPath = None
      self.screwName = None
      self.coords = [0,0,0]
      self.matrix1 = vtk.vtkMatrix3x3()
      self.matrix2 = vtk.vtkMatrix3x3()
      self.matrix3 = vtk.vtkMatrix3x3()
      self.matrixScrew = vtk.vtkMatrix4x4()
      self.fiduciallist = []
      self.screwInsertSummary = []
      
      self.screwList = []
      self.screwTrajectoryList = []
      self.currentFidIndex = 0
      self.currentFidLabel = None
      
      self.viewDirection = "Right"
      #self.views = "Right"
      
      self.startCount = 0
      self.addCount = 0
      self.fidCount = 0
      self.fidObserve = None
      self.fidObserve2 = None         
      self.fidNode = None
      
      self.valueTemp1 = 0
      self.valueTemp2 = 0
      self.driveTemp = 0
      
      self.entryCount = 0
      self.sideCount = 0
      
      self.__loadScrewButton = None
      self.__parent = super( ScrewStep, self )
      
      self.timer = qt.QTimer()
      self.timer.setInterval(0)
      self.timer.connect('timeout()', self.driveScrew)
      self.timer2 = qt.QTimer()
      self.timer2.setInterval(0)
      self.timer2.connect('timeout()', self.reverseScrew)
      self.screwInsert = 0.0
      
      self.navOn = 0
      self.oldX = 0
      self.oldY = 0
      self.oldZ = 0

    def killButton(self):
      # hide useless button
      bl = slicer.util.findChildren(text='Final')
      if len(bl):
        bl[0].hide()

    def createUserInterface( self ):

      self.__layout = self.__parent.createUserInterface() 
      
      viewText = qt.QLabel("Viewpoint:")
      self.views = ctk.ctkComboBox()
      self.views.toolTip = "Select your view point."
      self.views.addItem("Left")
      self.views.addItem("Right")
      self.connect(self.views, PythonQt.QtCore.SIGNAL('activated(QString)'), self.cameraSide)
      
      #self.QHBox0 = qt.QHBoxLayout()
      #self.QHBox0.addWidget(viewText)
      #self.QHBox1.addWidget(self.fiducial)
      #self.QHBox0.addWidget(self.views)
      #self.__layout.addRow(self.QHBox0)
      
      workText = qt.QLabel("Screw Insertion Workflow")
      self.QHBox = qt.QHBoxLayout()
      self.QHBox.addWidget(workText)
      self.__layout.addRow(self.QHBox)
      
      ''' 
      # Input fiducials node selector
      self.inputFiducialsNodeSelector = slicer.qMRMLNodeComboBox()
      self.inputFiducialsNodeSelector.toolTip = "Select a fiducial to define an insertion point for a screw."
      self.inputFiducialsNodeSelector.nodeTypes = (("vtkMRMLMarkupsFiducialNode"), "")
      self.inputFiducialsNodeSelector.addEnabled = False
      self.inputFiducialsNodeSelector.removeEnabled = False
      self.inputFiducialsNodeSelector.setMRMLScene( slicer.mrmlScene ) 
      self.inputFiducialsNodeSelector.connect('currentNodeChanged(vtkMRMLNode*)', self.fidChanged)
      '''
      self.startMeasurements = qt.QPushButton("Click to Add")
      self.startMeasurements.connect('clicked(bool)', self.addFiducials)
      #self.__layout.addRow(self.startMeasurements)
      
      eText = qt.QLabel("1. Add Entry Point:")
      
            
      #self.__layout.addRow("Select Insertion Point:", self.inputFiducialsNodeSelector)
      
      self.fiducial = ctk.ctkComboBox()
      self.fiducial.toolTip = "Select an insertion site."
      self.fiducial.addItems(self.fiduciallist)
      self.connect(self.fiducial, PythonQt.QtCore.SIGNAL('activated(QString)'), self.fiducial_chosen)
      self.connect(self.fiducial, PythonQt.QtCore.SIGNAL('activated(QString)'), self.fidChanged)
      #self.__layout.addRow("Insertion Site:", self.fiducial)
      
      self.QHBox1 = qt.QHBoxLayout()
      self.QHBox1.addWidget(eText)
      #self.QHBox1.addWidget(self.fiducial)
      self.QHBox1.addWidget(self.startMeasurements)
      self.__layout.addRow(self.QHBox1)
      
      sText = qt.QLabel("2. Active Entry Point:")
      self.QHBox2 = qt.QHBoxLayout()
      self.QHBox2.addWidget(sText)
      self.QHBox2.addWidget(self.fiducial)
      #self.QHBox2.addWidget(self.startMeasurements)
      self.__layout.addRow(self.QHBox2)
      
      #self.fid = self.inputFiducialsNodeSelector.currentNode()
      #self.sliceChange()
      
      self.levels = ("C1","C2","C3","C4","C5","C6","C7","T1","T2","T3","T4","T5","T6","T7","T8","T9","T10","T11","T12","L1", "L2", "L3", "L4", "L5","S1")
      '''
      # Input model selector
      self.inputScrewSelector = ctk.ctkComboBox()
      self.inputScrewSelector.toolTip = "Select a screw to insert."
      screwList = ['Select a screw','35x30', '475x35', '475x45', '550x30', '550x40', '550x45', '625x35', '625x40', '625x45', '625x50', '700x35', '700x40', '700x45', '700x50']
      self.inputScrewSelector.addItems(screwList)
      self.connect(self.inputScrewSelector, PythonQt.QtCore.SIGNAL('activated(QString)'), self.combo_chosen)
      self.__layout.addRow("Choose Screw:", self.inputScrewSelector)
      
       vText = qt.QLabel("1st Instrumented Level:")
       iText = qt.QLabel("# to Instrument:")
       aText = qt.QLabel("Approach Direction:")
       self.vSelector = qt.QComboBox()
       self.vSelector.setMaximumWidth(120)
       
       self.vSelector.addItems(self.levels)
       self.iSelector = qt.QComboBox()
       self.iSelector.setMaximumWidth(120)
       self.iSelector.addItems(['1','2','3','4','5','6','7','8','9','10','11','12'])
       self.aSelector = qt.QComboBox()
       self.aSelector.setMaximumWidth(120)
       self.aSelector.addItems(['Posterior','Anterior','Left','Right'])
       blank = qt.QLabel("  ")
       blank.setMaximumWidth(30)
       #self.__layout.addWidget(vText)
       #self.__layout.addWidget(self.vSelector)
       #self.__layout.addWidget(iText)
       #self.__layout.addWidget(self.iSelector)
    
       self.vertebraeGridBox = qt.QGridLayout()
       self.vertebraeGridBox.addWidget(vText,0,0)
       self.vertebraeGridBox.addWidget(self.vSelector,1,0)
       self.vertebraeGridBox.addWidget(blank,0,1)
       self.vertebraeGridBox.addWidget(iText,0,2)
       self.vertebraeGridBox.addWidget(blank,1,1)
       self.vertebraeGridBox.addWidget(self.iSelector,1,2)
       self.vertebraeGridBox.addWidget(blank,0,3)
       self.vertebraeGridBox.addWidget(aText,0,4)
       self.vertebraeGridBox.addWidget(blank,1,3)
       self.vertebraeGridBox.addWidget(self.aSelector,1,4)
       self.__layout.addRow(self.vertebraeGridBox)
      
      
      
      '''
      #self.screwGridLayout.addWidget(self.fiducial,0,0)
      
      
      
      
      '''
      self.__fiducial = ''
      measuredText1 = qt.QLabel("     Measured:")
      measuredText2 = qt.QLabel("     Measured:")
      lengthText = qt.QLabel("Screw Length:   ") 
      widthText = qt.QLabel("Screw Width:    ")            
      self.length = ctk.ctkComboBox()
      self.length.toolTip = "Select a screw to insert."
      screwList = ['Select a length (mm)','475', '550','625','700']
      self.length.addItems(screwList)
      self.connect(self.length, PythonQt.QtCore.SIGNAL('activated(QString)'), self.length_chosen)
      self.lengthMeasure = qt.QLineEdit()
      #self.__layout.addRow("Screw Length:", self.length)
      #self.__layout.addRow("Measured Pedicle Length:", self.lengthMeasure)
      self.__length = ''
      
      self.QHBox1 = qt.QHBoxLayout()
      self.QHBox1.addWidget(lengthText)
      self.QHBox1.addWidget(self.length)
      self.QHBox1.addWidget(measuredText1)
      self.QHBox1.addWidget(self.lengthMeasure)
      self.__layout.addRow(self.QHBox1)
      
      self.diameter = ctk.ctkComboBox()
      self.diameter.toolTip = "Select a screw to insert."
      screwList = ['Select a diameter (mm)','30', '35', '45', '50']
      self.diameter.addItems(screwList)
      self.widthMeasure = qt.QLineEdit()
      self.connect(self.diameter, PythonQt.QtCore.SIGNAL('activated(QString)'), self.diameter_chosen)
      #self.__layout.addRow("Screw Diameter:", self.diameter)
      #self.__layout.addRow("Measured Pedicle Width:", self.widthMeasure)
      self.__diameter = ''
      
      self.QHBox2 = qt.QHBoxLayout()
      self.QHBox2.addWidget(widthText)
      self.QHBox2.addWidget(self.diameter)
      self.QHBox2.addWidget(measuredText2)
      self.QHBox2.addWidget(self.widthMeasure)
      self.__layout.addRow(self.QHBox2)
      
      # Load Screw Button
      self.__loadScrewButton = qt.QPushButton("Load Screw")
      self.__loadScrewButton.enabled = False
      #self.__layout.addWidget(self.__loadScrewButton)
      self.__loadScrewButton.connect('clicked(bool)', self.loadScrew)
      
      
      
      self.QHBox3 = qt.QHBoxLayout()
      self.QHBox3.addWidget(self.__loadScrewButton)
      self.QHBox3.addWidget(self.__delScrewButton)
      self.__layout.addRow(self.QHBox3)
      
      # Input model node selector
      self.modelNodeSelector = slicer.qMRMLNodeComboBox()
      self.modelNodeSelector.toolTip = "."
      self.modelNodeSelector.nodeTypes = (("vtkMRMLModelNode"), "")
      self.modelNodeSelector.addEnabled = False
      self.modelNodeSelector.removeEnabled = False
      self.modelNodeSelector.setMRMLScene( slicer.mrmlScene ) 
      #self.__layout.addRow("Current Screws:", self.modelNodeSelector)
      '''
      self.transformGrid = qt.QGridLayout()
      vText = qt.QLabel("Transverse Orientation:")
      iText = qt.QLabel("Sagittal Orientation:")
      self.transformGrid.addWidget(vText, 0,0)
      self.transformGrid.addWidget(iText, 0,2)
      
      self.b = ctk.ctkDoubleSpinBox()
      self.b.minimum = -90
      self.b.maximum = 90
      
      self.transformGrid.addWidget(self.b, 1,0)
      
      # Transform Sliders
      self.transformSlider1 = ctk.ctkDoubleSlider()
      self.transformSlider1.minimum = -45
      self.transformSlider1.maximum = 45
      self.transformSlider1.connect('valueChanged(double)', self.transformSlider2ValueChanged)
      self.transformSlider1.connect('valueChanged(double)', self.b.setValue)
      self.transformSlider1.setMinimumHeight(120)
      #self.__layout.addRow("Rotate IS", self.transformSlider1)
      self.transformGrid.addWidget(self.transformSlider1, 1,1)
      
      self.b.connect('valueChanged(double)', self.transformSlider1.setValue)
      
      # Transform Sliders
      self.transformSlider2 = ctk.ctkSliderWidget()
      self.transformSlider2.minimum = -45
      self.transformSlider2.maximum = 45
      self.transformSlider2.connect('valueChanged(double)', self.transformSlider1ValueChanged)
      self.transformSlider2.setMaximumWidth(200)
      #self.__layout.addRow("Rotate LR", self.transformSlider2)
      self.transformGrid.addWidget(self.transformSlider2, 1,2)
      self.__layout.addRow(self.transformGrid)
      '''
      # Transfors Sliders
      self.transformSlider3 = ctk.ctkSliderWidget()
      self.transformSlider3.minimum = 0
      self.transformSlider3.maximum = 100
      self.transformSlider3.connect('valueChanged(double)', self.transformSlider3ValueChanged)
      self.__layout.addRow("Drive Screw", self.transformSlider3)
      '''
      # Load Screw Button
      #self.loadScrewButton = qt.QPushButton("Load Screw")
      #self.loadScrewButton.enabled = True
      #self.__layout.addWidget(self.__loadScrewButton)
      #self.loadScrewButton.connect('clicked(bool)', self.fiducial_chosen)
      #self.loadScrewButton.connect('clicked(bool)', self.loadScrew)
      
      # Insert Screw Button
      self.insertScrewButton = qt.QPushButton("Insert Screw")
      self.insertScrewButton.enabled = True
      #self.__layout.addWidget(self.__loadScrewButton)
      self.insertScrewButton.connect('clicked(bool)', self.insertScrew)
      
      # Backout Screw Button
      self.backoutScrewButton = qt.QPushButton("Backout Screw")
      self.backoutScrewButton.enabled = False
      #self.__layout.addWidget(self.__delScrewButton)
      self.backoutScrewButton.connect('clicked(bool)', self.backoutScrew)
      
      # Reset Screw Button
      self.resetScrewButton = qt.QPushButton("Reset Screw")
      self.resetScrewButton.enabled = True
      #self.__layout.addWidget(self.__delScrewButton)
      self.resetScrewButton.connect('clicked(bool)', self.resetScrew)
      
      # Delete Screw Button
      self.delScrewButton = qt.QPushButton("Remove Entry Pt")
      self.delScrewButton.enabled = False # this button breaks other functionality
      #self.__layout.addWidget(self.__delScrewButton)
      self.delScrewButton.connect('clicked(bool)', self.delScrew)
      
      self.QHBox4 = qt.QHBoxLayout()
      #self.QHBox4.addWidget(self.loadScrewButton)
      self.QHBox4.addWidget(self.insertScrewButton)
      #self.QHBox4.addWidget(self.backoutScrewButton)
      self.QHBox4.addWidget(self.resetScrewButton)
      self.QHBox4.addWidget(self.delScrewButton)
      self.__layout.addRow(self.QHBox4)
      
      # Load Trajectories Button
      self.loadTrajButton = qt.QPushButton("Load Trajectories")
      self.loadTrajButton.enabled = True
      #self.__layout.addWidget(self.__delScrewButton)
      self.loadTrajButton.connect('clicked(bool)', self.loadTrajectories)
      #self.__layout.addRow(self.loadTrajButton)

      # Hide ROI Details
      #measurementsTable = ctk.ctkCollapsibleButton()
      #measurementsTable.text = "Measurements Table"
      #self.__layout.addWidget(measurementsTable)
      #measurementsTable.collapsed = True
      # Hide ROI Details
      navCollapsibleButton = ctk.ctkCollapsibleButton()
      #roiCollapsibleButton.setMaximumWidth(320)
      navCollapsibleButton.text = "Navigation"
      self.__layout.addWidget(navCollapsibleButton)
      navCollapsibleButton.collapsed = True

      # Layout
      navLayout = qt.QFormLayout(navCollapsibleButton)
    
      self.navButton = qt.QPushButton("Enable Tool Navigation")
      navLayout.addRow(self.navButton)
      self.navButton.connect('clicked(bool)', self.enableNavigation)
      
      self.resetSliceButton = qt.QPushButton("Reset Slice Views")
      navLayout.addRow(self.resetSliceButton)
      self.resetSliceButton.connect('clicked(bool)', self.resetSliceViews)
      
      self.renderingButton = qt.QPushButton("Volume Rendering ON/OFF")
      navLayout.addRow(self.renderingButton)
      self.renderingButton.connect('clicked(bool)', self.renderingOnOff)
      
      self.templateButton = qt.QPushButton("Screw Template ON/OFF")
      navLayout.addRow(self.templateButton)
      self.templateButton.connect('clicked(bool)', self.showTemplate)
      '''
      self.view = qt.QTableView()
      self.model = qt.QStandardItemModel()
      self.view.setModel(self.model)
      item = qt.QStandardItem()
      item.setText("item")
      self.model.setItem(0,0,item)
      self.__layout.addWidget(self.view)
      '''
      #self.__layout.addRow(self.screwGridLayout)
      # self.updateWidgetFromParameters(self.parameterNode())
      qt.QTimer.singleShot(0, self.killButton)
      
      
      #self.updateMeasurements()
      #self.cameraFocus(self.coords)
      #self.screwLandmarks()
    
    def loadTrajectories(self):
        a = slicer.mrmlScene.GetNodesByName('InsertionLandmarks')
        b = a.GetItemAsObject(0)
        b.RemoveAllMarkups()
        
        trajectoriesPath = qt.QFileDialog.getOpenFileName(self, "Open Data File", "", "CSV data files (*.csv)")
        #path = "D:\\Images\\SpineChallenge\\case1-2\\case1_lm2.csv"
        #imageName = self.__baselineVolume.GetName()
        #caseLandmarksPath = os.path.join(os.path.dirname(__file__), 'cases/' + imageName + "_lm.csv") 
        #print os.path.isfile(caseLandmarksPath)
        #path = "E:\\Research\\Imaging\\case1-2\\" + imageName + "_lm.csv"
        rownum = 0
        #self.highCoord = 0
        #self.lowCoord = 0                   
        if os.path.isfile(trajectoriesPath) != False: 
            
                                                      
            with open(trajectoriesPath) as stream:
                reader = csv.reader(stream)
                for row in reader:
                    #rowdata = []
                    #print i
                    #print row
                    name = row[0]
                    entrypoint = row[1]
                    angles = row[2]
                    entrypoint = entrypoint.split(",")
                    angles = angles.split(",")
                    print name
                    print entrypoint
                    print angles                               
                    print entrypoint[0]
                    
                    b.AddFiducial(float(entrypoint[0]),float(entrypoint[1]),float(entrypoint[2]))
                    b.SetNthFiducialLabel(rownum,name)
                    b.SetNthMarkupLocked(rownum,1)
                    
                    self.currentFidLabel = name
                    self.updateComboBox()
                    self.loadScrew()
                    
                    self.transformSlider1.setValue(float(angles[0]))
                    self.transformSlider2.setValue(float(angles[1]))
                    
                    self.insertScrew()
                    
                    self.sliceChange()
                    
                    rownum += 1
                    
                    
                    '''
                    if name == 'Threshold':
                        self.boneThreshold = int(row[1])
                        self.__corticalRange.minimum = 0
                        self.__corticalRange.minimumValue = self.boneThreshold
                    #position = (row[1],row[2],row[3])
                    else:
                        b.AddFiducial(float(row[1]),float(row[2]),float(row[3]))
                        b.SetNthFiducialLabel(rownum,name)
                        b.SetNthMarkupLocked(rownum,1)
                        self.vSelector.addItem(name)
                        
                        if rownum != 0:
                            if float(row[3]) > self.highCoord[2]:
                                self.highCoord = (float(row[1]),float(row[2]),float(row[3]))
                            elif float(row[3]) < self.lowCoord[2]:
                                self.lowCoord = (float(row[1]),float(row[2]),float(row[3]))
                        else:
                            self.highCoord = (float(row[1]),float(row[2]),float(row[3]))
                            self.lowCoord = (float(row[1]),float(row[2]),float(row[3]))
                        rownum += 1
                    '''
                    '''
                    if name == 'L3':
                        markup = slicer.modules.markups.logic()
                        markup.AddNewFiducialNode('Vertebra Reference')
                        slicer.modules.markups.logic().AddFiducial()
                        vr = slicer.mrmlScene.GetNodesByName('Vertebra Reference')
                        c = vr.GetItemAsObject(0)
                        dNode = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsDisplayNode3')
                        dNode.SetSelectedColor(0, 1, 0.2)
                        dNode.SetGlyphScale(5)
                        dNode.SetTextScale(5)
                        c.SetNthFiducialLabel(0,"L3")
                        c.SetNthFiducialPosition(0,float(row[1]),float(row[2])-50,float(row[3]))
                    '''
                    '''
                    for column in reader:
                        item = self.screwTable.item(row, column)
                        #print item
                        if item is not None:
                            rowdata.append(unicode(item.text()).encode('utf8'))
                        else:
                            rowdata.append('')
                    #x = range(0, 10)
                    screwValues = self.screwContact[row]
                    for j in range(len(self.screwContact[row])):
                        rowdata.append(unicode(int(screwValues[j])).encode('utf8'))
                    writer.writerow(rowdata)    
                    '''
        #self.level_chosen()
        #self.addCropFiducials()
        #self.autoROI()
    
    def enableNavigation(self):
        
        if self.navOn == 0:
            lm = slicer.app.layoutManager()
            if lm == None: 
                return 
            lm.setLayout(slicer.vtkMRMLLayoutNode.SlicerLayoutConventionalWidescreenView)
            
            # Enable Slice Intersections
            viewNodes = slicer.mrmlScene.GetNodesByClass('vtkMRMLSliceCompositeNode')
            viewNodes.UnRegister(slicer.mrmlScene)
            viewNodes.InitTraversal()
            viewNode = viewNodes.GetNextItemAsObject()
            while viewNode:
                viewNode.SetSliceIntersectionVisibility(1)
                viewNode = viewNodes.GetNextItemAsObject()
                
                self.navButton.setText("Disable Tool Navigation") 
                self.navOn = 1
            
            n =  self.__baselineVolume
            for color in ['Red', 'Yellow', 'Green']:
                a = slicer.app.layoutManager().sliceWidget(color).sliceLogic().GetSliceNode().GetFieldOfView()
                slicer.app.layoutManager().sliceWidget(color).sliceLogic().GetSliceCompositeNode().SetBackgroundVolumeID(n.GetID())
                #slicer.app.layoutManager().sliceWidget(color).sliceLogic().GetSliceNode().SetFieldOfView(a[0],a[1],a[2])
                slicer.app.layoutManager().sliceWidget(color).sliceLogic().GetSliceNode().SetFieldOfView(150,150,a[2]) 
        else:
            lm = slicer.app.layoutManager()
            if lm == None: 
                return 
            lm.setLayout(slicer.vtkMRMLLayoutNode.SlicerLayoutOneUp3DView)

            
            self.navButton.setText("Enable Tool Navigation") 
            self.navOn = 0  
            
    def resetSliceViews(self):
        for i in range(0,3):
            #transform = vtk.vtkTransform()
            #transform2 = vtk.vtkMatrix4x4()
            #transform2 = screwTransform.GetMatrixTransformToParent()
                    
            #print "Fiducial Transform"
            #print transform
            if i == 0:
                viewSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeRed')
                viewSlice.GetSliceToRAS().DeepCopy(self.redTransform.GetMatrix())
                
            elif i == 1:
                viewSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeYellow')
                viewSlice.GetSliceToRAS().DeepCopy(self.yellowTransform.GetMatrix())
                
            elif i == 2:
                viewSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeGreen')
                viewSlice.GetSliceToRAS().DeepCopy(self.greenTransform.GetMatrix())
            
            viewSlice.UpdateMatrices()    
            
    def renderingOnOff(self):
        slicer.mrmlScene.GetNodesByName('GPURayCastVolumeRendering').GetItemAsObject(0).SetVisibility(0)
    
    def showTemplate(self):
        collectionS = slicer.mrmlScene.GetNodesByName('template model-%s' % self.currentFidLabel)
        screwCheck = collectionS.GetItemAsObject(0)
        
        if screwCheck == None:
            #screwDescrip = ["0","0","0","0","0","0"]
            #filename = inspect.getframeinfo(inspect.currentframe()).filename
            #path = os.path.dirname(os.path.abspath(filename))
            #screwPath = os.path.join(path, '/screws/scaled_475x30.vtk')
            templateModelPath = os.path.join(os.path.dirname(__file__), os.path.pardir, 'Resources/ScrewModels/45x30.vtk')
            #print needleModelPath
            #screwModel = slicer.modules.models.logic().AddModel("C:\\Users\\Stewart\\Dropbox\\Research\\SB_2013_Slicer Pedicle Screw Sim\\Code\\Slicer 4_5\PedicleScrewSimulatorWizard_v4\\needle2.vtk")
            screwModel = slicer.modules.models.logic().AddModel(templateModelPath)
            if screwModel != None:
              #fid.AddObserver('ModifiedEvent', self.fidMove)
            
              matrix = vtk.vtkMatrix4x4()
              matrix.DeepCopy((1, 0, 0, 0,
                               0, 1, 0, -30,
                               0, 0, 1, 0,
                               0, 0, 0, 1))
            
              transformScrewTemp = slicer.vtkMRMLLinearTransformNode()
              transformScrewTemp.SetName("Template Transform-%s" % self.currentFidLabel)
              slicer.mrmlScene.AddNode(transformScrewTemp)
              transformScrewTemp.ApplyTransformMatrix(matrix)
            
            
              screwModel.SetName('template model-%s' % self.currentFidLabel)
              screwModel.SetAndObserveTransformNodeID(transformScrewTemp.GetID())
              
              #n = getNode('Bone')
              logic = slicer.vtkSlicerTransformLogic()
              logic.hardenTransform(screwModel)
           
              #self.toolTransform = transformScrewTemp
	      #self.toolTransformMatrix = self.toolTransform.GetMatrixTransformToParent()
	      
	      collectionT = slicer.mrmlScene.GetNodesByName('Transform-%s' % self.currentFidLabel)
              transformFid = collectionT.GetItemAsObject(0)
	      
	      screwModel.SetAndObserveTransformNodeID(transformFid.GetID())
	      
              modelDisplay = screwModel.GetDisplayNode()
              modelDisplay.SetColor(0.12,0.73,0.91)
              modelDisplay.SetDiffuse(0.90)
              modelDisplay.SetAmbient(0.10)
              modelDisplay.SetSpecular(0.20)
              modelDisplay.SetPower(10.0)
              modelDisplay.SetSliceIntersectionVisibility(True)
              screwModel.SetAndObserveDisplayNodeID(modelDisplay.GetID())
              
              #self.screwState = ScrewState.free	
				
	      # Start its timer.
				
	      #self.screwTimerList[self.currentFidIndex].start()
              
              
              #screwDescrip[0] = self.currentFidLabel
              #screwDescrip[1] = self.__length
              #screwDescrip[2] = self.__diameter
              
              #self.screwList.append(screwDescrip)
              # Mark state.

	     
            
              
            else:
                return            
                                        
    def insertScrew(self):
      #print "insert"
      self.timer.start()
      self.backoutScrewButton.enabled = True
      self.insertScrewButton.enabled = False
      self.b.enabled = False
      self.transformSlider1.enabled = False
      self.transformSlider2.enabled = False
      collectionS = slicer.mrmlScene.GetNodesByName('probe model-%s' % self.currentFidLabel)
      screwModel = collectionS.GetItemAsObject(0)
      d = screwModel.GetDisplayNode()
      d.SetColor(0.1,0.77,0.0)
      if self.fidCount < 36:
        self.confirmDialog()
      #self.transformSlider3ValueChanged(int(self.__diameter))
      
      temp = self.screwTrajectoryList[self.currentFidIndex]
      temp[0] = self.transformSlider1.value
      temp[1] = self.transformSlider2.value
      self.screwTrajectoryList[self.currentFidIndex] = temp
      self.screwInsertSummary[self.currentFidIndex] = 1
      
      self.fidNode.SetNthMarkupLocked(self.currentFidIndex,1)
      
      
    def backoutScrew(self):
      #print "backout"
      self.timer2.start()
      self.backoutScrewButton.enabled = False
      self.insertScrewButton.enabled = True
      self.b.enabled = True
      self.transformSlider1.enabled = True
      self.transformSlider2.enabled = True
      collectionS = slicer.mrmlScene.GetNodesByName('probe model-%s' % self.currentFidLabel)
      screwModel = collectionS.GetItemAsObject(0)
      d = screwModel.GetDisplayNode()
      d.SetColor(0.12,0.73,0.91)
      #temp = self.screwList[self.currentFidIndex]
      #temp[3] = "0"
      #self.screwList[self.currentFidIndex] = temp
      
            
    def resetScrew(self):
      #print "reset"  
      self.resetOrientation()
      collectionS = slicer.mrmlScene.GetNodesByName('probe model-%s' % self.currentFidLabel)
      screwModel = collectionS.GetItemAsObject(0)
      d = screwModel.GetDisplayNode()
      d.SetColor(0.12,0.73,0.91)
      self.fidNode.SetNthMarkupLocked(self.currentFidIndex,0)
      self.screwInsertSummary[self.currentFidIndex] = 0
      #temp = self.screwList[self.currentFidIndex]
      #temp[3] = "0"
      #self.screwList[self.currentFidIndex] = temp
      
    
    def updateMeasurements(self):
      pedicleLength = slicer.modules.PedicleScrewSimulator_v3Widget.measurementsStep.angleTable.cellWidget(self.currentFidIndex,3).currentText
      pedicleWidth = slicer.modules.PedicleScrewSimulator_v3Widget.measurementsStep.angleTable.cellWidget(self.currentFidIndex,4).currentText
      self.lengthMeasure.setText(pedicleLength + " mm")
      self.widthMeasure.setText(pedicleWidth + " mm")
      #print pedicleLength
      
    def screwLandmarks(self):
      #self.fiducial = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode2')
      self.fidNumber = self.fiducial.GetNumberOfFiducials()
      self.fidLabels = []
      self.fidLevels = []
      self.fidSides = []
      self.fidLevelSide = []
      
      for i in range(0,self.fidNumber):
          self.fidLabels.append(slicer.modules.PedicleScrewSimulator_v3Widget.measurementsStep.angleTable.item(i,0).text())
          self.fidLevels.append(slicer.modules.PedicleScrewSimulator_v3Widget.measurementsStep.angleTable.cellWidget(i,1).currentText)
          self.fidSides.append(slicer.modules.PedicleScrewSimulator_v3Widget.measurementsStep.angleTable.cellWidget(i,2).currentText)
          #self.fidLevelSide.append(self.fidLevels[i] + " " + self.fidSides[i])
      
      
    
    def sliceChange(self):
        #print "sliceChange"
        coords = [0,0,0]
        if self.fidNode != None:
          self.fidNode.GetNthFiducialPosition(self.currentFidIndex,coords)
               
          lm = slicer.app.layoutManager()
          redWidget = lm.sliceWidget('Red')
          redController = redWidget.sliceController()
        
          yellowWidget = lm.sliceWidget('Yellow')
          yellowController = yellowWidget.sliceController()
        
          greenWidget = lm.sliceWidget('Green')
          greenController = greenWidget.sliceController()
        
          yellowController.setSliceOffsetValue(coords[0])
          greenController.setSliceOffsetValue(coords[1])
          redController.setSliceOffsetValue(coords[2])
          #print pos[0]
          #print pos[1]
          #print pos[2]
          self.fidNode.UpdateScene(slicer.mrmlScene)
          
          fidName = self.fidNode.GetNthFiducialLabel(self.currentFidIndex)
          print "sliceChange" + str(fidName)
          
          #fidName = self.fiducial.GetNthFiducialLabel(self.currentFid)
          #self.fiducial.GetNthFiducialPosition(self.currentFidIndex,self.coords)
          collectionT = slicer.mrmlScene.GetNodesByName('Transform-%s' % fidName)
          screwTransform = collectionT.GetItemAsObject(0)
            
          matrixScrew = vtk.vtkMatrix4x4()
          matrixScrew = screwTransform.GetMatrixTransformToParent()
            
          x = math.atan2(matrixScrew.GetElement(2,1), matrixScrew.GetElement(2,2))
          y = math.atan2(-matrixScrew.GetElement(2,0), math.sqrt((matrixScrew.GetElement(2,1) * matrixScrew.GetElement(2,1)) + (matrixScrew.GetElement(2,2) * matrixScrew.GetElement(2,2))))
          z = math.atan2(matrixScrew.GetElement(1,0), matrixScrew.GetElement(0,0))
            
            
          		
          # The above ranges will be between pi.
          # Convert them to degrees.
          		
          x = 180 - x * (180.0 / math.pi) # -pi to +pi.
          y = y * (180.0 / math.pi) # -(pi / 2.0) to +(pi / 2.0).
          z = z * (180.0 / math.pi) # -pi to +pi.
            
          #print (self.oldX,self.oldY,self.oldZ)
          #print (x,y,z)
          
          #x = self.transformSlider1.value
          #z = self.transformSlider2.value
            
          for i in range(0,3):
                transform = vtk.vtkTransform()
                transform2 = vtk.vtkMatrix4x4()
                #transform2 = screwTransform.GetMatrixTransformToParent()
                        
                #print "Fiducial Transform"
                #print transform
                if i == 0:
                    viewSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeRed')
                    transform.SetMatrix(viewSlice.GetSliceToRAS())
                    #transform.RotateX(-z + self.oldZ)
                    transform.RotateX(-x + self.oldX)
                    transform2.DeepCopy(transform.GetMatrix())
                    transform2.SetElement(0,3,coords[0])
                    transform2.SetElement(1,3,coords[1])
                    transform2.SetElement(2,3,coords[2])
                    transform.SetMatrix(transform2)
    
                elif i == 1:
                    viewSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeYellow')
                    transform.SetMatrix(viewSlice.GetSliceToRAS())
                    #transform.RotateY(x - self.oldX)
                    transform.RotateY(-z + self.oldZ)
                    transform2.DeepCopy(transform.GetMatrix())
                    transform2.SetElement(0,3,coords[0])
                    transform2.SetElement(1,3,coords[1])
                    transform2.SetElement(2,3,coords[2])
                    transform.SetMatrix(transform2)
                    
                elif i == 2:
                    viewSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeGreen')
                    transform.SetMatrix(viewSlice.GetSliceToRAS())
                    #transform.RotateX(z - self.oldZ)
                    #transform.RotateY(x - self.oldX)
                    transform.RotateX(x - self.oldX)
                    transform.RotateY(z - self.oldZ)
                    transform2.DeepCopy(transform.GetMatrix())
                    transform2.SetElement(0,3,coords[0])
                    transform2.SetElement(1,3,coords[1])
                    transform2.SetElement(2,3,coords[2])
                    transform.SetMatrix(transform2)
                    
                #print "Slice Transform" 
                #print viewSlice.GetSliceToRAS()
                
                #transform.Translate(value - self.oldPosition)
                viewSlice.GetSliceToRAS().DeepCopy(transform.GetMatrix())
                viewSlice.UpdateMatrices()
                
          self.oldX = x
          #self.oldY = y
          self.oldZ = z
          '''  
          collectionT = slicer.mrmlScene.GetNodesByName('Transform-%s' % fidName)
          screwTransform = collectionT.GetItemAsObject(0)
          transform = vtk.vtkTransform()
          transform2 = vtk.vtkMatrix4x4()
          transform2 = screwTransform.GetMatrixTransformToParent()
          transform.SetMatrix(transform2)
          redSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeRed')
          #transform.SetMatrix(redSlice.GetSliceToRAS())
          #transform.RotateX(value - self.oldPosition)
          #transform.Translate(value - self.oldPosition)
          redSlice.GetSliceToRAS().DeepCopy(transform.GetMatrix())
          redSlice.UpdateMatrices()
          '''
        else:
            return
        
    def fidChanged(self, fid):
        print "fidChanged"
        #print fid
        self.valueTemp1 = 0
        self.valueTemp2 = 0
        self.driveTemp = 0
        
        #self.transformSlider3.reset()
        
        collectionS = slicer.mrmlScene.GetNodesByName('probe model-%s' % self.currentFidLabel)
        screwCheck = collectionS.GetItemAsObject(0)
        
        if screwCheck == None:
            self.loadScrew()
            self.transformSlider1.setValue(0)
            self.transformSlider2.reset()
            self.insertScrewButton.enabled = True
            self.b.enabled = True
            self.transformSlider1.enabled = True
            self.transformSlider2.enabled = True
            
            
        else:
            print "reset"
            temp = self.screwTrajectoryList[self.currentFidIndex]
            self.transformSlider1.setValue(temp[0])
            self.transformSlider2.setValue(temp[1])
            if self.screwInsertSummary[self.currentFidIndex] == 1:
                #self.backoutScrewButton.enabled = False
                self.insertScrewButton.enabled = False
                self.b.enabled = False
                self.transformSlider1.enabled = False
                self.transformSlider2.enabled = False
            else:
                self.insertScrewButton.enabled = True
                self.b.enabled = True
                self.transformSlider1.enabled = True
                self.transformSlider2.enabled = True
            #self.screwTrajectoryList[self.currentFidIndex] = temp
            #temp = self.screwList[self.currentFidIndex]
            #vertOrt = float(temp[4])
            #horzOrt = float(temp[5])
            #self.resetScrew()
            #self.transformSlider1.setValue(vertOrt)
            #self.transformSlider2.setValue(horzOrt)
        
        
        self.sliceChange()
        #self.updateMeasurements()
        self.cameraFocus(self.coords)
        
        
        
    def fiducial_chosen(self):
        print "fiducial_chosen"
        if self.fiducial.currentText != "Select an insertion landmark":
            self.currentFidLabel = self.fiducial.currentText
            self.currentFidIndex = self.fiducial.currentIndex
            #self.currentFidLabel = self.fiducial.currentText
            self.fidNode.GetNthFiducialPosition(self.currentFidIndex,self.coords)
            
            
            
            #self.combo_chosen()
            

    def length_chosen(self, text):
        if text != "Select a length (mm)":
            self.__length = text
            self.combo_chosen()
        
    def diameter_chosen(self, text):
        if text != "Select a diameter (mm)":
            self.__diameter = text
            self.combo_chosen()

    def updateComboBox(self):
        print "update combo box"
        #print self.fidNode
        #print self.fidCount
        fidLabel = self.fidNode.GetNthFiducialLabel(self.fidCount - 1)
        #print fidLabel
        self.fiducial.addItem(fidLabel)
        self.fiducial.setCurrentIndex(self.fidCount - 1)
        self.fidChanged(fidLabel)
        #self.currentFidIndex
        self.screwList.append(fidLabel)
        self.screwTrajectoryList.append([0,0])
        self.screwInsertSummary.append(0)
        
        
    def combo_chosen(self):
        
          
        
        if self.__length != "Select a length (mm)" and self.__diameter != "Select a diameter (mm)":
          self.screwPath = os.path.join(os.path.dirname(__file__), os.path.pardir, 'Resources/ScrewMcrews/' + self.__length + 'x' + self.__diameter + '.vtk')
          #print(self.screwPath)
          self.__loadScrewButton.enabled = True
          #self.screwName = 'scaled_' + text
          #self.transformSlider3.maximum = int(self.__diameter)
        
    def nameScrew(self, fidIndex):
        print "name screw"
        vertPosition = [0,0,0]
        vertUpperPosition = [0,0,0]
        vertLowerPosition = [0,0,0]
        fidPosition = [0,0,0]
        #fidIndex = self.fidCount - 1
        vertPositions = []
        vertText = ""
        #verts = 0
        vertLabels = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode1')
        verts = vertLabels.GetNumberOfFiducials()
        #self.fidNode = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode2')
        self.fidNode.GetNthFiducialPosition(fidIndex,fidPosition)
        #print self.coords
        #print self.fidNode.GetNthFiducialLabel(self.fidCount - 1)
        #print str(fidIndex) + " " + str(fidPosition)
        for j in range(0,verts):
            #print j
            #print "vertPosition" + str(vertPosition[2]) + "fidPos" + str(fidPosition[2])
            vertText = vertLabels.GetNthFiducialLabel(j)
            if j == 0:
                vertLabels.GetNthFiducialPosition(j,vertPosition)
                vertLabels.GetNthFiducialPosition(j+1,vertLowerPosition)
                vertRange = [vertPosition[2]+15, (vertPosition[2]+vertLowerPosition[2])/2]
            elif j == verts-1:
                vertLabels.GetNthFiducialPosition(j,vertPosition)
                vertLabels.GetNthFiducialPosition(j-1,vertUpperPosition)
                vertRange = [(vertPosition[2]+vertUpperPosition[2])/2, vertPosition[2]]
            else:
                vertLabels.GetNthFiducialPosition(j,vertPosition)
                vertLabels.GetNthFiducialPosition(j+1,vertLowerPosition)
                vertLabels.GetNthFiducialPosition(j-1,vertUpperPosition)
                vertRange = [(vertPosition[2]+vertUpperPosition[2])/2, (vertPosition[2]+vertLowerPosition[2])/2]                   
            
            #print vertRange
            #print fidPosition[2]
            if vertRange[1] < fidPosition[2] <= vertRange[0]:
                #print "yes"
                nameCount = 0
                if fidPosition[0] > vertPosition[0]:
                    for items in self.screwList:
                        nameCount += items.count("Right-" + vertText)
                    #nameCount = self.screwList.count("Right-" + vertText)
                    if nameCount == 0:
                        self.fidNode.SetNthFiducialLabel(fidIndex,"Right-" + vertText)
                        self.currentFidLabel = "Right-" + vertText
                    else:
                        self.fidNode.SetNthFiducialLabel(fidIndex,"Right-" + vertText + str(nameCount))
                        self.currentFidLabel = "Right-" + vertText + str(nameCount)
                    
                    #self.cameraSide('Right')
                else:
                    for items in self.screwList:
                        nameCount += items.count("Left-" + vertText)
                    #nameCount = self.screwList.count("Left-" + vertText)
                    if nameCount == 0:
                        self.fidNode.SetNthFiducialLabel(fidIndex,"Left-" + vertText)
                        self.currentFidLabel = "Left-" + vertText
                    else:
                        self.fidNode.SetNthFiducialLabel(fidIndex,"Left-" + vertText + str(nameCount))
                        self.currentFidLabel = "Left-" + vertText + str(nameCount)
                    
                    #self.cameraSide('Left')
            
                

    def loadScrew(self):    
        print "load screw"
        #print self.coords
        #self.fidChanged
        
        
        #fid = self.fidNode
        
        self.currentFidIndex = self.fiducial.currentIndex
        print "current index" + str(self.currentFidIndex)
        self.currentFidLabel = self.fiducial.currentText
        print "current label" + str(self.currentFidLabel)
        self.fidNode.GetNthFiducialPosition(self.currentFidIndex,self.coords)
        print "current coords" + str(self.coords)
        
        collectionS = slicer.mrmlScene.GetNodesByName('probe model-%s' % self.currentFidLabel)
        screwCheck = collectionS.GetItemAsObject(0)
        
        if screwCheck == None:
            #screwDescrip = ["0","0","0","0","0","0"]
            #filename = inspect.getframeinfo(inspect.currentframe()).filename
            #path = os.path.dirname(os.path.abspath(filename))
            #screwPath = os.path.join(path, '/screws/scaled_475x30.vtk')
            needleModelPath = os.path.join(os.path.dirname(__file__), os.path.pardir, 'Resources/ScrewModels/needle3.vtk')
            #print needleModelPath
            #screwModel = slicer.modules.models.logic().AddModel("C:\\Users\\Stewart\\Dropbox\\Research\\SB_2013_Slicer Pedicle Screw Sim\\Code\\Slicer 4_5\PedicleScrewSimulatorWizard_v4\\needle2.vtk")
            screwModel = slicer.modules.models.logic().AddModel(needleModelPath)
            if screwModel != None:
              #fid.AddObserver('ModifiedEvent', self.fidMove)
            
              matrix = vtk.vtkMatrix4x4()
              matrix.DeepCopy((1, 0, 0, self.coords[0],
                               0, -1, 0, self.coords[1],
                               0, 0, -1, self.coords[2],
                               0, 0, 0, 1))
            
              transformScrewTemp = slicer.vtkMRMLLinearTransformNode()
              transformScrewTemp.SetName("Transform-%s" % self.currentFidLabel)
              slicer.mrmlScene.AddNode(transformScrewTemp)
              transformScrewTemp.ApplyTransformMatrix(matrix)
            
            
              screwModel.SetName('probe model-%s' % self.currentFidLabel)
              screwModel.SetAndObserveTransformNodeID(transformScrewTemp.GetID())
           
              self.toolTransform = transformScrewTemp
	      self.toolTransformMatrix = self.toolTransform.GetMatrixTransformToParent()
	      
	      
              modelDisplay = screwModel.GetDisplayNode()
              modelDisplay.SetColor(0.12,0.73,0.91)
              modelDisplay.SetDiffuse(0.90)
              modelDisplay.SetAmbient(0.10)
              modelDisplay.SetSpecular(0.20)
              modelDisplay.SetPower(10.0)
              modelDisplay.SetSliceIntersectionVisibility(True)
              screwModel.SetAndObserveDisplayNodeID(modelDisplay.GetID())
              
              #self.showTemplate()
              
              #self.screwState = ScrewState.free	
				
	      # Start its timer.
				
	      #self.screwTimerList[self.currentFidIndex].start()
              
              
              #screwDescrip[0] = self.currentFidLabel
              #screwDescrip[1] = self.__length
              #screwDescrip[2] = self.__diameter
              
              #self.screwList.append(screwDescrip)
              # Mark state.

	     
            
              
            else:
                return
        
                     
        else:
            #print "screw found"
            
            #coords = [0,0,0]  
            #observer.GetFiducialCoordinates(coords)
    
            collectionT = slicer.mrmlScene.GetNodesByName('Transform-%s' % self.currentFidLabel)
            transformFid = collectionT.GetItemAsObject(0)
            
            matrixScrew = vtk.vtkMatrix4x4()
            matrixScrew = transformFid.GetMatrixTransformToParent()
            
            #transformFid.GetMatrixTransformToParent().SetElement(0,3,self.coords[0])
            matrixScrew.SetElement(0,3,self.coords[0])
            matrixScrew.SetElement(1,3,self.coords[1])
            matrixScrew.SetElement(2,3,self.coords[2]) 
            transformFid.SetAndObserveMatrixTransformToParent(matrixScrew)
            
            #transformFid.UpdateScene(slicer.mrmlScene)
            self.sliceChange()
            
        

    def delScrew(self):
        #fidName = self.inputFiducialsNodeSelector.currentNode().GetName()
        
        collectionT = slicer.mrmlScene.GetNodesByName('Transform-%s' % self.currentFidLabel)
        transformFid = collectionT.GetItemAsObject(0)
            
        collectionS = slicer.mrmlScene.GetNodesByName('probe model-%s' % self.currentFidLabel)
        screwModel = collectionS.GetItemAsObject(0)
            
        if screwModel != None:
            slicer.mrmlScene.RemoveNode(transformFid)
            slicer.mrmlScene.RemoveNode(screwModel)
            self.fidNode.RemoveMarkup(self.currentFidIndex)
            
        else:
            return
        
    def fidMove(self, observer, event): 
        print "fidMove"
        #pos = [0,0,0]
        fidNode = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode4')
        fidNode.GetNthFiducialPosition(0,self.coords)
        #print pos   
        #print self.coords
        #print observer.GetName()
        number = fidNode.GetNumberOfFiducials()
        for i in range(0,number):
            #print i
            collectionS = slicer.mrmlScene.GetNodesByName('probe model-%s' % observer.GetName())
            screwCheck = collectionS.GetItemAsObject(0)
            
            if screwCheck != None:
                #print "screw found"
                #coords = [0,0,0]  
                #observer.GetFiducialCoordinates(coords)
        
                collectionT = slicer.mrmlScene.GetNodesByName('Transform-%s' % observer.GetName())
                transformFid = collectionT.GetItemAsObject(0)
                
                transformFid.GetMatrixTransformToParent().SetElement(0,3,self.coords[0])
                transformFid.GetMatrixTransformToParent().SetElement(1,3,self.coords[1])
                transformFid.GetMatrixTransformToParent().SetElement(2,3,self.coords[2]) 
                
                transformFid.UpdateScene(slicer.mrmlScene)
                self.sliceChange()
            else:
                return
            
    def cameraSide(self, text):
      camera = slicer.mrmlScene.GetNodeByID('vtkMRMLCameraNode1')
      if text == "Right":
          camera.SetViewUp([-1,0,0])
          self.viewDirection = "Right"
      else:
          camera.SetViewUp([1,0,0]) 
          self.viewDirection = "Left"
               
    def cameraFocus(self, position):
      #self.viewDirection = self.views.currentText  
      #self.viewDirection = self.views  
      #print self.viewDirection
      camera = slicer.mrmlScene.GetNodeByID('vtkMRMLCameraNode1')
      camera.SetFocalPoint(*position)
      camera.SetPosition(position[0],-400,position[2])
      if self.viewDirection == "Right":
          camera.SetViewUp([-1,0,0])
      else:
          camera.SetViewUp([1,0,0])
      '''
      if self.approach == 'Posterior':
          
          camera.SetFocalPoint(*position)
          camera.SetPosition(position[0],-400,position[2])
          camera.SetViewUp([0,0,1])
               
      elif self.approach == 'Anterior':
          
          camera.SetFocalPoint(*position)
          camera.SetPosition(position[0],400,position[2])
          camera.SetViewUp([0,0,1])
               
      elif self.approach == 'Left':
          
          camera.SetFocalPoint(*position)
          camera.SetPosition(-400,position[1],position[2])
          camera.SetViewUp([0,0,1])
                
      elif self.approach == 'Right':
          
          camera.SetFocalPoint(*position)
          camera.SetPosition(400,position[1],position[2])
          camera.SetViewUp([0,0,1])
      '''
      camera.ResetClippingRange()
          
    def transformSlider1ValueChanged(self, value):
        #print(value)
        #print "transform slider1"
        if self.viewDirection == "Left":
            value = -value
        newValue = value - self.valueTemp1
        
        angle1 = math.pi / 180.0 * newValue * -1 # Match screw direction
        self.angleOne = angle1
		
        matrix1 = vtk.vtkMatrix3x3()
        matrix1.DeepCopy([ 1, 0, 0,
                          0, math.cos(angle1), -math.sin(angle1),
                          0, math.sin(angle1), math.cos(angle1)])

        try:
          self.transformScrewComposite(matrix1)
        except:
          pass
        
        self.valueTemp1 = value 
        
        self.sliceChange()
        
        #temp = self.screwList[self.currentFidIndex]
        #temp[4] = str(value)
        #self.screwList[self.currentFidIndex] = temp
        #print self.screwList
        
    def transformSlider2ValueChanged(self, value):
        #print(value)
        #print "slider 2 changed"
        if self.viewDirection == "Right":
            value = -value
        newValue = value - self.valueTemp2
        
        angle2 = math.pi / 180.0 * newValue * -1 # Match screw direction
        self.angleTwo = angle2
        
        matrix2 = vtk.vtkMatrix3x3()
        matrix2.DeepCopy([ math.cos(angle2), -math.sin(angle2), 0,
                          math.sin(angle2), math.cos(angle2), 0,
                          0, 0, 1])
        
        try:
          self.transformScrewComposite(matrix2)
        except:
          pass
        
        self.valueTemp2 = value
        
        self.sliceChange()
        
        #temp = self.screwList[self.currentFidIndex]
        #temp[5] = str(value)
        #self.screwList[self.currentFidIndex] = temp
        #print self.screwList
    
    def nothing():
        print "nothing"
    
    def delayDisplay(self,action, msec=1000):
	"""This utility method displays a small dialog and waits.
	This does two things: 1) it lets the event loop catch up
	to the state of the test so that rendering and widget updates
	have all taken place before the test continues and 2) it
	shows the user/developer/tester the state of the test
	so that we'll know when it breaks.
	"""
	#print(message)
	#self.info = qt.QDialog()
	#self.infoLayout = qt.QVBoxLayout()
	#self.info.setLayout(self.infoLayout)
	#self.label = qt.QLabel(message,self.info)
	#self.infoLayout.addWidget(self.label)
	#qt.QTimer.singleShot(msec, action)
	#self.info.exec_()    
    
                            
    def driveScrew(self):
        if self.screwInsert < 51:
            
            value = self.screwInsert
            #print(value)
            # attempt to rotate with driving        
            '''
            angle3 = math.pi / 180.0 * 50 #((360/2.5)*self.screwInsert) 
        
            matrix3 = vtk.vtkMatrix3x3()
            matrix3.DeepCopy([ math.cos(angle3), 0, -math.sin(angle3),
                          0, 1, 0,
                          math.sin(angle3), 0, math.cos(angle3)])
        
            self.transformScrewComposite(matrix3)
            '''

            value = value*-1
            collectionT = slicer.mrmlScene.GetNodesByName('Transform-%s' % self.currentFidLabel)
            transformFid = collectionT.GetItemAsObject(0)
        
            matrixScrew = vtk.vtkMatrix4x4()
            matrixScrew = transformFid.GetMatrixTransformToParent()
        
            newVal = value - self.driveTemp
            #print(newVal)
        
            drive1 = matrixScrew.GetElement(0,1)
            drive2 = matrixScrew.GetElement(1,1)
            drive3 = matrixScrew.GetElement(2,1)
        
            coord1 = drive1 * newVal + matrixScrew.GetElement(0,3)
            coord2 = drive2 * newVal + matrixScrew.GetElement(1,3)
            coord3 = drive3 * newVal + matrixScrew.GetElement(2,3)
        
            matrixScrew.SetElement(0,3,coord1)
            matrixScrew.SetElement(1,3,coord2)
            matrixScrew.SetElement(2,3,coord3)
            
            transformFid.SetAndObserveMatrixTransformToParent(matrixScrew)
                
            #transformFid.UpdateScene(slicer.mrmlScene)
            #self.delayDisplay(transformFid.UpdateScene(slicer.mrmlScene), 2000)
            self.driveTemp = value
            self.screwInsert += 10
        else:
            self.timer.stop()  
            self.screwInsert = 0.0 
            self.driveTemp = 0
              
        '''    
        angle3 = math.pi / 180.0 * (value*50) 
        
        matrix3 = vtk.vtkMatrix3x3()
        matrix3.DeepCopy([ math.cos(angle3), 0, -math.sin(angle3),
                          0, 1, 0,
                          math.sin(angle3), 0, math.cos(angle3)])
        
        self.transformScrewComposite(matrix3)


        value = value*-1
        collectionT = slicer.mrmlScene.GetNodesByName('Transform-%s' % self.currentFidLabel)
        transformFid = collectionT.GetItemAsObject(0)
        
        matrixScrew = vtk.vtkMatrix4x4()
        matrixScrew = transformFid.GetMatrixTransformToParent()
        
        newVal = value - self.driveTemp
        
        drive1 = matrixScrew.GetElement(0,1)
        drive2 = matrixScrew.GetElement(1,1)
        drive3 = matrixScrew.GetElement(2,1)
        
        coord1 = drive1 * newVal + matrixScrew.GetElement(0,3)
        coord2 = drive2 * newVal + matrixScrew.GetElement(1,3)
        coord3 = drive3 * newVal + matrixScrew.GetElement(2,3)
        
        matrixScrew.SetElement(0,3,coord1)
        matrixScrew.SetElement(1,3,coord2)
        matrixScrew.SetElement(2,3,coord3)

        
        
        transformFid.UpdateScene(slicer.mrmlScene)
        
        self.driveTemp = value
        '''   
    def reverseScrew(self):
        if self.screwInsert < 51:
            
            value = self.screwInsert
            #print(value)
            # attempt to rotate with driving        
            '''
            angle3 = math.pi / 180.0 * -50 #((360/2.5)*self.screwInsert) 
        
            matrix3 = vtk.vtkMatrix3x3()
            matrix3.DeepCopy([ math.cos(angle3), 0, -math.sin(angle3),
                          0, 1, 0,
                          math.sin(angle3), 0, math.cos(angle3)])
        
            self.transformScrewComposite(matrix3)
            '''

            collectionT = slicer.mrmlScene.GetNodesByName('Transform-%s' % self.currentFidLabel)
            transformFid = collectionT.GetItemAsObject(0)
        
            matrixScrew = vtk.vtkMatrix4x4()
            matrixScrew = transformFid.GetMatrixTransformToParent()
        
            newVal = value - self.driveTemp
            #print(newVal)
        
            drive1 = matrixScrew.GetElement(0,1)
            drive2 = matrixScrew.GetElement(1,1)
            drive3 = matrixScrew.GetElement(2,1)
        
            coord1 = drive1 * newVal + matrixScrew.GetElement(0,3)
            coord2 = drive2 * newVal + matrixScrew.GetElement(1,3)
            coord3 = drive3 * newVal + matrixScrew.GetElement(2,3)
        
            matrixScrew.SetElement(0,3,coord1)
            matrixScrew.SetElement(1,3,coord2)
            matrixScrew.SetElement(2,3,coord3)
            
            transformFid.SetAndObserveMatrixTransformToParent(matrixScrew)
                
            #transformFid.UpdateScene(slicer.mrmlScene)
            #self.delayDisplay(transformFid.UpdateScene(slicer.mrmlScene), 2000)
            self.driveTemp = value
            self.screwInsert += 5
        else:
            self.timer2.stop()  
            self.screwInsert = 0.0 
            self.driveTemp = 0 

    def resetOrientation(self):
                
        self.transformSlider1.setValue(0)
        self.transformSlider2.reset()
        
        collectionT = slicer.mrmlScene.GetNodesByName('Transform-%s' % self.currentFidLabel)
        transformFid = collectionT.GetItemAsObject(0)
        
        matrixScrew = vtk.vtkMatrix4x4()
        matrixScrew = transformFid.GetMatrixTransformToParent()
        
        matrixScrew.SetElement(0,0,1)
        matrixScrew.SetElement(0,1,0)
        matrixScrew.SetElement(0,2,0)
        
        matrixScrew.SetElement(1,0,0)
        matrixScrew.SetElement(1,1,-1)
        matrixScrew.SetElement(1,2,0)
        
        matrixScrew.SetElement(2,0,0)
        matrixScrew.SetElement(2,1,0)
        matrixScrew.SetElement(2,2,-1)
        
        matrixScrew.SetElement(0,3,self.coords[0])
        matrixScrew.SetElement(1,3,self.coords[1])
        matrixScrew.SetElement(2,3,self.coords[2])
        
        transformFid.SetAndObserveMatrixTransformToParent(matrixScrew)
       
        transformFid.UpdateScene(slicer.mrmlScene)

        self.backoutScrewButton.enabled = False
        self.insertScrewButton.enabled = True
               
        self.transformSlider1.enabled = True
        self.transformSlider2.enabled = True
        self.b.enabled = True
        #self.transformSlider3.reset()

    def transformScrewComposite(self, inputMatrix):
        #print "transform composite"
        
        if self.screwInsertSummary[self.currentFidIndex] == 0:
        
            collectionT = slicer.mrmlScene.GetNodesByName('Transform-%s' % self.currentFidLabel)
            transformFid = collectionT.GetItemAsObject(0)
            #print transformFid
            #print inputMatrix
            
            matrixScrew = vtk.vtkMatrix4x4()
            matrixScrew = transformFid.GetMatrixTransformToParent()
            
            newMatrix = vtk.vtkMatrix3x3()
            outputMatrix = vtk.vtkMatrix3x3()
            
            newMatrix.SetElement(0,0,matrixScrew.GetElement(0,0))
            newMatrix.SetElement(0,1,matrixScrew.GetElement(0,1))
            newMatrix.SetElement(0,2,matrixScrew.GetElement(0,2))
            
            newMatrix.SetElement(1,0,matrixScrew.GetElement(1,0))
            newMatrix.SetElement(1,1,matrixScrew.GetElement(1,1))
            newMatrix.SetElement(1,2,matrixScrew.GetElement(1,2))
            
            newMatrix.SetElement(2,0,matrixScrew.GetElement(2,0))
            newMatrix.SetElement(2,1,matrixScrew.GetElement(2,1))
            newMatrix.SetElement(2,2,matrixScrew.GetElement(2,2))
            
            vtk.vtkMatrix3x3.Multiply3x3(newMatrix, inputMatrix, outputMatrix)
            
            #coords = [0,0,0]  
            #self.fid.GetFiducialCoordinates(coords)
            
            matrixScrew.SetElement(0,0,outputMatrix.GetElement(0,0))
            matrixScrew.SetElement(0,1,outputMatrix.GetElement(0,1))
            matrixScrew.SetElement(0,2,outputMatrix.GetElement(0,2))
            #matrixScrew.SetElement(0,3,self.coords[0])
            
            matrixScrew.SetElement(1,0,outputMatrix.GetElement(1,0))
            matrixScrew.SetElement(1,1,outputMatrix.GetElement(1,1))
            matrixScrew.SetElement(1,2,outputMatrix.GetElement(1,2))
            #matrixScrew.SetElement(1,3,self.coords[1])
            
            matrixScrew.SetElement(2,0,outputMatrix.GetElement(2,0))
            matrixScrew.SetElement(2,1,outputMatrix.GetElement(2,1))
            matrixScrew.SetElement(2,2,outputMatrix.GetElement(2,2))
            #matrixScrew.SetElement(2,3,self.coords[2])
            
            matrixScrew.SetElement(3,0,0)
            matrixScrew.SetElement(3,1,0)
            matrixScrew.SetElement(3,2,0)
            matrixScrew.SetElement(3,3,1)
            
            transformFid.SetAndObserveMatrixTransformToParent(matrixScrew)
            #print matrixScrew
            #transformFid.ApplyTransformMatrix(matrixScrew)
            #transformFid.UpdateScene(slicer.mrmlScene)
    
    def addFiducials(self):
      if self.startCount == 0:
        self.begin()
        self.startCount = 1
        self.startMeasurements.setText("Place Entry Pt")
      elif self.startCount == 1:
        self.stop()
        self.startCount = 0
        self.startMeasurements.setText("Add Entry Pt")
        
    def begin(self):
      selectionNode = slicer.mrmlScene.GetNodeByID("vtkMRMLSelectionNodeSingleton")
      # place rulers
      selectionNode.SetReferenceActivePlaceNodeClassName("vtkMRMLMarkupsFiducialNode")
      # to place ROIs use the class name vtkMRMLAnnotationROINode
      interactionNode = slicer.mrmlScene.GetNodeByID("vtkMRMLInteractionNodeSingleton")
      #placeModePersistence = 1
      #interactionNode.SetPlaceModePersistence(placeModePersistence)
      # mode 1 is Place, can also be accessed via slicer.vtkMRMLInteractionNode().Place
      interactionNode.SetCurrentInteractionMode(1)

    def stop(self):
      selectionNode = slicer.mrmlScene.GetNodeByID("vtkMRMLSelectionNodeSingleton")
      # place rulers
      selectionNode.SetReferenceActivePlaceNodeClassName("vtkMRMLMarkupsFiducialNode")
      # to place ROIs use the class name vtkMRMLAnnotationROINode
      interactionNode = slicer.mrmlScene.GetNodeByID("vtkMRMLInteractionNodeSingleton")
      #placeModePersistence = 1
      #interactionNode.SetPlaceModePersistence(placeModePersistence)
      # mode 1 is Place, can also be accessed via slicer.vtkMRMLInteractionNode().Place
      interactionNode.SetCurrentInteractionMode(2)
    
    def fidMoved(self, node, event):
      print "fidMoved"
      #print node
      #print event
      '''
      for i in range(node.GetNumberOfFiducials()):
        p = [0,0,0]
        node.GetNthFiducialPosition(i, p)
        #print p
        self.nameScrew(i)
      '''
      if node.GetNumberOfFiducials() > self.fiducial.count:
          self.nameScrew(node.GetNumberOfFiducials() - 1)
          self.updateComboBox()
      self.loadScrew()
         
      
    
    def fidMoved2(self, node, event):
        
        vertPosition = [0,0,0]
        fidPosition = [0,0,0]
        #fidIndex = self.fidCount - 1
        vertPositions = []
        vertText = ""
        #verts = 0
        vertLabels = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode1')
        verts = vertLabels.GetNumberOfFiducials()
        #self.fidNode = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode2')
        node.GetNthFiducialPosition(0,fidPosition)
        #print self.coords
        #print self.fidNode.GetNthFiducialLabel(self.fidCount - 1)
        #print str(fidIndex) + " " + str(fidPosition)
        for j in range(0,verts):
            #print j
            vertLabels.GetNthFiducialPosition(j,vertPosition)
            vertText = vertLabels.GetNthFiducialLabel(j)
            #print "vertPosition" + str(vertPosition[2]) + "fidPos" + str(fidPosition[2])
            if vertPosition[2]-10 < fidPosition[2] <= vertPosition[2]+10:
                node.SetNthFiducialLabel(0, vertText)
                

                                        
    def addFiducialToTable(self, node, event):
      print "addFidToTable"
      self.fidNode.UpdateScene(slicer.mrmlScene)
      self.fidCount = self.fidNode.GetNumberOfFiducials()
      self.addFiducials()

      if node.GetNumberOfFiducials() > self.fiducial.count:
          self.nameScrew(node.GetNumberOfFiducials() - 1)
          self.updateComboBox()
      self.loadScrew()

      #print self.fidCount
      #print "number of fids: " + str(self.fidCount)
      #pos = [0,0,0]
      #self.fidNode.GetNthFiducialPosition(0,pos)
      #print pos
      #self.displayNode.SetVisibility(1)
      #print event
      #if self.fidCount >= self.fiducial.count:
          #self.updateComboBox()
          #self.nameScrew()
          #a = slicer.modules.markups.logic()
          #b = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode2')
          #a.SetAllMarkupsVisibility(b,0)
          #self.addFiducials()
          #a.SetAllMarkupsVisibility(b,1)   
          #self.onOffMarkupVisible()
      #a.SetAllMarkupsVisibility(b,1)
      #self.loadScrew()
      self.stop()  
          
    def validate( self, desiredBranchId ):
        
      self.__parent.validate( desiredBranchId )
      self.__parent.validationSucceeded(desiredBranchId)

    def onOffMarkupVisible(self, observer, event):
      a = slicer.modules.markups.logic()
      b = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode2')
      #print "off"    
      a.SetAllMarkupsVisibility(b,0)
      #print "on"
      a.SetAllMarkupsVisibility(b,1)
      
    def buildWalls(self):
      roi = slicer.mrmlScene.GetNodeByID('vtkMRMLAnnotationROINode1')
      bounds=[0,0,0,0,0,0]
      roi.GetRASBounds(bounds)
      roiCenter = [(bounds[1]+bounds[0])/2,(bounds[3]+bounds[2])/2,(bounds[5]+bounds[4])/2]
      roiRadius = [(bounds[1]-bounds[0])/2,(bounds[3]-bounds[2])/2,(bounds[5]-bounds[4])/2]   
      roiXLength = bounds[1] - bounds[0]   
      roiYLength = bounds[3] - bounds[2] 
      roiZLength = bounds[5] - bounds[4] 
      self.addWall('Left', 5, roiYLength, roiZLength, roiCenter[0]-roiRadius[0]+2, roiCenter[1], roiCenter[2])
      self.addWall('Right', 5, roiYLength, roiZLength, roiCenter[0]+roiRadius[0]-2, roiCenter[1], roiCenter[2])
      self.addWall('Top', roiXLength, roiYLength, 5, roiCenter[0], roiCenter[1], roiCenter[2]+roiRadius[2])
      self.addWall('Bottom', roiXLength, roiYLength, 5, roiCenter[0], roiCenter[1], roiCenter[2]-roiRadius[2])
      self.addWall('Front', roiXLength, 5, roiZLength, roiCenter[0], roiCenter[1]+roiRadius[1], roiCenter[2])
        
    def addWall(self, name, xSize, ySize, zSize, xLocation, yLocation, zLocation):
      cube = vtk.vtkCubeSource()
      cube.SetXLength( xSize )
      cube.SetYLength( ySize )
      cube.SetZLength( zSize )
      cube.Update()
    
      # Add the needle poly data to the scene as a model
    
      slicerScene = slicer.mrmlScene
    
      modelNode = slicer.vtkMRMLModelNode()
      modelNode.SetScene( slicerScene )
      modelNode.SetName( name )
      modelNode.SetAndObservePolyData( cube.GetOutput() )

      displayNode = slicer.vtkMRMLModelDisplayNode()
      displayNode.SetScene( slicerScene )
      displayNode.SetName( name + "Display" )
      #displayNode.SetColor(0.43, 0.72, 0.82)
      displayNode.SetColor(0.53, 0.0, 0.01)  
      #if name == 'Front':
      #    displayNode.SetColor(0.53, 0.0, 0.01)
      slicerScene.AddNode( displayNode )
      
      matrix = vtk.vtkMatrix4x4()
      matrix.DeepCopy((1, 0, 0, xLocation,
                       0, 1, 0, yLocation,
                       0, 0, 1, zLocation,
                       0, 0, 0, 1))
      
      transformWall = slicer.vtkMRMLLinearTransformNode()
      transformWall.SetName("Transform-%s" % name)
      slicer.mrmlScene.AddNode(transformWall)
      transformWall.ApplyTransformMatrix(matrix)
            
      modelNode.SetAndObserveTransformNodeID(transformWall.GetID())
      #transformWall.SetAndObserveTransformNodeID(self.transformBody.GetID())
    
      modelNode.SetAndObserveDisplayNodeID( displayNode.GetID() )
      #displayNode.SetPolyData( cube.GetOutput() )
    
      slicerScene.AddNode( modelNode ) 
    '''
    def buildWalls(self):
      #bounds = input.GetPolyData().GetBounds()
      roi = slicer.mrmlScene.GetNodeByID('vtkMRMLAnnotationROINode1')
      bounds=[0,0,0,0,0,0]
      roi.GetRASBounds(bounds)
      roiCenter = [0,0,0]
      roiCenter = [(bounds[1]+bounds[0])/2,(bounds[3]+bounds[2])/2,(bounds[5]+bounds[4])/2]
      roi.GetXYZ(roiCenter)
      roiRadius = [0,0,0] 
      roiRadius = [(bounds[1]-bounds[0])/2,(bounds[3]-bounds[2])/2,(bounds[5]-bounds[4])/2]   
      roi.GetRadiusXYZ(roiRadius)
      roiXLength = bounds[1] - bounds[0]   
      roiYLength = bounds[3] - bounds[2] 
      roiZLength = bounds[5] - bounds[4] 
      self.addWall('T-Left', 5, int(roiYLength/2), int(roiZLength/2), roiCenter[0]-roiRadius[0]+2, roiCenter[1], roiCenter[2]+int(roiZLength/2))
      self.addWall('T-Right', 5, int(roiYLength/2), int(roiZLength/2), roiCenter[0]+roiRadius[0]-2, roiCenter[1], roiCenter[2]+int(roiZLength/2))
      self.addWall('L-Left', 5, int(roiYLength/2), int(roiZLength/2), roiCenter[0]-roiRadius[0]+2, roiCenter[1], roiCenter[2]-int(roiZLength/2))
      self.addWall('L-Right', 5, int(roiYLength/2), int(roiZLength/2), roiCenter[0]+roiRadius[0]-2, roiCenter[1], roiCenter[2]-int(roiZLength/2))
      #self.addWall('Top', roiXLength, roiYLength, 5, roiCenter[0], roiCenter[1], roiCenter[2]+roiRadius[2])
      #self.addWall('Bottom', roiXLength, roiYLength, 5, roiCenter[0], roiCenter[1], roiCenter[2]-roiRadius[2])
      #self.addWall('Front', roiXLength, 5, roiZLength, roiCenter[0], roiCenter[1]+roiRadius[1], roiCenter[2])
        
    def addWall(self, name, xSize, ySize, zSize, xLocation, yLocation, zLocation):
      cube = vtk.vtkCubeSource()
      cube.SetXLength( xSize )
      cube.SetYLength( ySize )
      cube.SetZLength( zSize )
      cube.Update()
    
      # Add the needle poly data to the scene as a model
    
      slicerScene = slicer.mrmlScene
    
      modelNode = slicer.vtkMRMLModelNode()
      modelNode.SetScene( slicerScene )
      modelNode.SetName( name )
      modelNode.SetAndObservePolyData( cube.GetOutput() )

      displayNode = slicer.vtkMRMLModelDisplayNode()
      displayNode.SetScene( slicerScene )
      displayNode.SetName( name + "Display" )
      displayNode.SetColor(0.43, 0.72, 0.82)
      
      slicerScene.AddNode( displayNode )
      if name == 'T-Left':
          displayNode.SetColor(0.53, 0.0, 0.01)
          matrix = vtk.vtkMatrix4x4()
          matrix.DeepCopy((0.82, 0.57, 0, -35,
                            -0.57, 0.82, 0, -19,
                            0, 0, 1, 1264,
                            0, 0, 0, 1))
      elif name == 'T-Right':
          displayNode.SetColor(0.53, 0.0, 0.01)
          matrix = vtk.vtkMatrix4x4()
          matrix.DeepCopy((0.82, -0.57, 0, 35,
                            0.57, 0.82, 0, -19,
                            0, 0, 1, 1264,
                            0, 0, 0, 1))

      elif name == 'L-Left':
          displayNode.SetColor(0.53, 0.0, 0.01)
          matrix = vtk.vtkMatrix4x4()
          matrix.DeepCopy((0.82, 0.57, 0, -42,
                            -0.57, 0.82, 0, 35.7,
                            0, 0, 1, 1074,
                            0, 0, 0, 1))
      elif name == 'L-Right':
          displayNode.SetColor(0.53, 0.0, 0.01)
          matrix = vtk.vtkMatrix4x4()
          matrix.DeepCopy((0.82, -0.57, 0, 40,
                            0.57, 0.82, 0, 30,
                            0, 0, 1, 1074,
                            0, 0, 0, 1))

      transformWall = slicer.vtkMRMLLinearTransformNode()
      transformWall.SetName("Transform-%s" % name)
      slicer.mrmlScene.AddNode(transformWall)
      transformWall.ApplyTransformMatrix(matrix)
            
      modelNode.SetAndObserveTransformNodeID(transformWall.GetID())
      
      #transformSpine = slicer.mrmlScene.GetNodeByID('vtkMRMLLinearTransformNode6')
      #transformWall.SetAndObserveTransformNodeID(transformSpine.GetID())
      #transformWall.SetAndObserveTransformNodeID(self.transformBody.GetID())
    
      modelNode.SetAndObserveDisplayNodeID( displayNode.GetID() )
      #displayNode.SetPolyData( cube.GetOutput() )
    
      slicerScene.AddNode( modelNode )   
    '''
    def onEntry(self, comingFrom, transitionType):
      if self.fidNode == None:
        
        lNode = slicer.mrmlScene.GetNodesByName('Vertebrae Labels')
        labels = lNode.GetItemAsObject(0)
        #self.fiducial = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode1')
        labelsNumber = labels.GetNumberOfFiducials()
                
        markup = slicer.modules.markups.logic()
        markup.AddNewFiducialNode('Vertebra Reference')
        slicer.modules.markups.logic().AddFiducial()
        vr = slicer.mrmlScene.GetNodesByName('Vertebra Reference')
        c = vr.GetItemAsObject(0)
        dNode = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsDisplayNode3')
        dNode.SetSelectedColor(0, 1, 0.2)
        dNode.SetGlyphScale(5)
        dNode.SetTextScale(5)
        
        c.SetNthFiducialLabel(0,"Reference")
        labels.GetNthFiducialPosition(0,self.coords)
        c.SetNthFiducialPosition(0,self.coords[0]+5,self.coords[1]-50,self.coords[2]-5)
        
        #c.SetNthFiducialLabel(0,"L3")
        #c.SetNthFiducialPosition(0,float(row[1]),float(row[2])-50,float(row[3]))
        
        #self.fidObserve3 = b.AddObserver(b.PointModifiedEvent, self.fidMoved2)
        markup = slicer.modules.markups.logic()
        markup.AddNewFiducialNode('InsertionLandmarks')
        landmarks = slicer.mrmlScene.GetNodesByName('InsertionLandmarks')
        self.fidNode = landmarks.GetItemAsObject(0)
        self.displayNode = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsDisplayNode3')
        self.displayNode.SetSelectedColor(1.0, 0, 0.2)
        self.displayNode.SetGlyphScale(5)
        self.displayNode.SetTextScale(0)
        self.fidObserve = self.fidNode.AddObserver(self.fidNode.MarkupAddedEvent, self.addFiducialToTable)
        self.fidObserve2 = self.fidNode.AddObserver(self.fidNode.PointModifiedEvent, self.fidMoved)
        #self.fidObserve2 = self.fidNode.AddObserver(self.fidNode.MarkupAddedEvent, self.loadScrew)
        
      #self.fidNode = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode2')
      
      
      #self.fidNode.SetLocked(1)
      
      slicer.modules.models.logic().SetAllModelsVisibility(1)
      a = slicer.mrmlScene.GetNodesByName('clipModel').GetItemAsObject(0).SetDisplayVisibility(0)
      b = slicer.mrmlScene.GetNodesByName('clipModel').GetItemAsObject(1).SetDisplayVisibility(0)
      #c = slicer.mrmlScene.GetNodesByName('Sacrum-T5-Model2').GetItemAsObject(0).SetDisplayVisibility(0)
     
      #
      
      #for x in range (0,self.fidNode.GetNumberOfFiducials()):
        #print x
        #label = self.fidNode.GetNthFiducialLabel(x)
        #level = slicer.modules.PedicleScrewSimulator_v3Widget.landmarksStep.table2.cellWidget(x,1).currentText
        #side = slicer.modules.PedicleScrewSimulator_v3Widget.landmarksStep.table2.cellWidget(x,2).currentText
        #self.fiduciallist.append(label + " / " + level + " / " + side)
        
        #modelX = slicer.mrmlScene.GetNodeByID('vtkMRMLModelDisplayNode' + str(x + 4))
        #modelX.SetSliceIntersectionVisibility(1)
     
      
      #self.fiducial.clear()
      #self.fiducial.addItem("Select an insertion site")
      #self.fiducial.addItems(self.fiduciallist)    
      
      super(ScrewStep, self).onEntry(comingFrom, transitionType)
      
      lm = slicer.app.layoutManager()
      if lm == None: 
        return 
      lm.setLayout(slicer.vtkMRMLLayoutNode.SlicerLayoutOneUp3DView)
      
      self.redTransform = vtk.vtkTransform()
      viewSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeRed')
      self.redTransform.SetMatrix(viewSlice.GetSliceToRAS())
      #print "Red Transform"
      #print self.redTransform
      self.yellowTransform = vtk.vtkTransform()
      viewSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeYellow')
      self.yellowTransform.SetMatrix(viewSlice.GetSliceToRAS())
      #print "Yellow Transform"
      #print self.yellowTransform
      self.greenTransform = vtk.vtkTransform()
      viewSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeGreen')
      self.greenTransform.SetMatrix(viewSlice.GetSliceToRAS())
      #print "Green Transform"
      #print self.greenTransform
      
      pNode = self.parameterNode()
      pNode.SetParameter('currentStep', self.stepid)
      #print(pNode.GetParameter('currentStep'))
      self.approach = str(pNode.GetParameter('approach'))
      #pNode = self.parameterNode()
      baselineVolume = Helper.getNodeByID(pNode.GetParameter('baselineVolumeID'))
      self.__baselineVolume = baselineVolume
      
      inputModel = slicer.mrmlScene.GetNodeByID('vtkMRMLModelNode4')
      inputModel.SetDisplayVisibility(0)
      
      #outModel = slicer.mrmlScene.GetNodeByID('vtkMRMLModelNode5')
      #outModel.GetDisplayNode().SetColor(1, 0.97, 0.79)
      
      clipROI = slicer.mrmlScene.GetNodeByID('vtkMRMLAnnotationROINode1')
      clipROI.GetXYZ(self.coords)
      
      #self.buildWalls()
      
      
      self.confirmDialog()
      #a = qt.QMessageBox()
      #leftView = a.addButton('left',a.YesRole)
      #rightView = a.addButton('right',a.NoRole)
      #a.show()
      #a.question(slicer.util.mainWindow(),'Entry Point Step','Welcome',qt.QMessageBox.Ok)
      '''
      if a.clickedButton() == leftView:
          camera = slicer.mrmlScene.GetNodeByID('vtkMRMLCameraNode1')
          b = [1,0,0]
          camera.SetViewUp(b)
          #print "left"
          self.viewDirection = "Left"
      else:
          camera = slicer.mrmlScene.GetNodeByID('vtkMRMLCameraNode1')
          b = [-1,0,0]
          camera.SetViewUp(b)
          #print "right"
          self.viewDirection = "Right"
      
      '''
      qt.QTimer.singleShot(0, self.killButton)
      self.loadEntryPoints()

    def loadEntryPoints(self):
      a = slicer.mrmlScene.GetNodesByName('InsertionLandmarks')
      b = a.GetItemAsObject(0)
      b.RemoveAllMarkups()
      rownum = 0
      self.highCoord = 0
      self.lowCoord = 0
      if slicer.modules.PedicleScrewSimulatorWidget.loginStep.savedWorkflow == 1:
          labels = slicer.modules.PedicleScrewSimulatorWidget.loginStep.savedEntryPointLabels
          positions = slicer.modules.PedicleScrewSimulatorWidget.loginStep.savedEntryPointPositions
          angles = slicer.modules.PedicleScrewSimulatorWidget.loginStep.savedEntryPointTrajectories
          for number in range(len(labels)):
              c = positions[number]
              c = c.split(",")
              b.AddFiducial(float(c[0]),float(c[1]),float(c[2]))
              b.SetNthFiducialLabel(rownum,labels[number])
              b.SetNthMarkupLocked(rownum,1)
              d = angles[number]
              d = d.split(",")
              e = d[1]
              if e[0] != " ":
                  d = angles[number]
                  d = d.split(",")
              else:
                  d = angles[number]
                  d = d.split(", ")


              if rownum != 0:
                  if float(c[2]) > self.highCoord[2]:
                      self.highCoord = (float(c[0]),float(c[1]),float(c[2]))
                  elif float(c[2]) < self.lowCoord[2]:
                      self.lowCoord = (float(c[0]),float(c[1]),float(c[2]))
              else:
                  self.highCoord = (float(c[0]),float(c[1]),float(c[2]))
                  self.lowCoord = (float(c[0]),float(c[1]),float(c[2]))
              rownum += 1

              self.currentFidLabel = labels[number]

              self.transformSlider1.setValue(0)
              self.transformSlider2.setValue(0)

              self.transformSlider1.setValue(float(d[0]))
              self.transformSlider2.setValue(float(d[1]))
              self.insertScrew()      
              self.sliceChange()

    def confirmDialog(self):
        vertLabels = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode1')
        refLabels = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode3')
        vertText = vertLabels.GetNthFiducialLabel(self.entryCount)
        #vertLabels.GetNthFiducialPosition(self.entryCount,self.coords)
        refLabels.GetNthFiducialPosition(0,self.coords)
        print self.coords
        self.cameraFocus(self.coords)
        if self.sideCount == 0:
            side = "left"
            s = 'L'
            #self.cameraSide('Left')
        else:
            side = "right"
            s = 'R'
            #self.cameraSide('Right')
        '''
        result = qt.QMessageBox.question(slicer.util.mainWindow(),
                        'Entry Point Selection', 'Select the entry point for the ' + side + ' ' + vertText + ' pedicle screw.',
                        qt.QMessageBox.Ok)
        '''
        result = qt.QMessageBox.question(slicer.util.mainWindow(),
                        'Entry Point Selection', 'Select the screw entry point. Do not hit OK if a screw has not been completely inserted.',
                        qt.QMessageBox.Ok)
        #self.currentFidLabel = vertText + s
                      
        if result == qt.QMessageBox.Ok:
            #self.begin()
            if self.sideCount == 0:
                self.sideCount = 1
            else:
                self.sideCount = 0
                self.entryCount += 1
    
    def onExit(self, goingTo, transitionType):
      
      login = LoginStepModule.LoginStep(PedicleScrewSimulatorStep)
      filePath = login.saveFilePath
      if filePath != '':
        login.transposeSaveFile(filePath)
        insertPoints = slicer.util.getNode('InsertionLandmarks')
        insertLabelList, insertPositionList, trajectoryList, screwSize, ras = [], [], [], [], [0,0,0]
        for i in range(insertPoints.GetNumberOfFiducials()):
          insertLabelList.append(insertPoints.GetNthFiducialLabel(i))
          insertPoints.GetNthFiducialPosition(i,ras)
          insertPositionList.append(str(ras).replace(' ', '').strip('[').strip(']'))
          screwSize.append('65x50')
        login.addToSaveFile(filePath,insertLabelList,4)
        login.addToSaveFile(filePath,insertPositionList,5)
        trajectoryCopy = list(self.screwTrajectoryList)
        [trajectoryList.append(str(trajectoryCopy[i]).replace(' ', '').strip('[').strip(']')) for i in range(len(trajectoryCopy))]
        login.addToSaveFile(filePath,trajectoryList,6)
        login.addToSaveFile(filePath,screwSize,7)
        login.transposeSaveFile(filePath)
	  
      if goingTo.id() != 'Grade' and goingTo.id() != 'Approach':
          return
      
      if goingTo.id() == 'Grade':
          '''
          fidCollection = slicer.mrmlScene.GetNodeByID('vtkMRMLAnnotationFiducialNode1')
          fidCount = fidCollection.GetNumberOfItems()
          for i in range(0, fidCount):
            
            fidName = fidCollection.GetItemAsObject(i).GetName()
            collectionS = slicer.mrmlScene.GetNodesByName('Screw at point %s' % fidName)
            screwModel = collectionS.GetItemAsObject(0)
            slicer.mrmlScene.RemoveNode(screwModel)
            
            fid = fidCollection.GetItemAsObject(i)
            slicer.mrmlScene.RemoveNode(fid)
            '''
          slicer.modules.models.logic().SetAllModelsVisibility(0)
          a = slicer.mrmlScene.GetNodesByName('Model').GetItemAsObject(0).SetDisplayVisibility(1)
          
          for x in range(0,self.fidNode.GetNumberOfFiducials()):
             modelX = slicer.mrmlScene.GetNodeByID('vtkMRMLModelDisplayNode' + str(x + 4))
             modelX.SetSliceIntersectionVisibility(0)
          
          #self.fidNode.SetLocked(0)
          self.fidNode.RemoveObserver(self.fidObserve)
          self.fidNode.RemoveObserver(self.fidObserve2)
          
          
          viewSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeRed')
          viewSlice.GetSliceToRAS().DeepCopy(self.redTransform.GetMatrix())
          viewSlice.UpdateMatrices()
          viewSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeYellow')
          viewSlice.GetSliceToRAS().DeepCopy(self.yellowTransform.GetMatrix())
          viewSlice.UpdateMatrices()
          viewSlice = slicer.mrmlScene.GetNodeByID('vtkMRMLSliceNodeGreen')
          viewSlice.GetSliceToRAS().DeepCopy(self.greenTransform.GetMatrix())
          viewSlice.UpdateMatrices()
          
      
      if goingTo.id() == 'Approach':
          
        fidNode = slicer.mrmlScene.GetNodeByID('vtkMRMLMarkupsFiducialNode4')
        number = fidNode.GetNumberOfFiducials()
        for i in range(0,number):
            #print i
            fidName = fidNode.GetNthFiducialLabel(i)
            collectionS = slicer.mrmlScene.GetNodesByName('probe model-%s' % fidName)
            screwCheck = collectionS.GetItemAsObject(0)
            collectionT = slicer.mrmlScene.GetNodesByName('Transform-%s' % fidName)
            transformFid = collectionT.GetItemAsObject(0)
            if screwCheck != None:
                slicer.mrmlScene.RemoveNode(transformFid)
                slicer.mrmlScene.RemoveNode(screwCheck)
   
      super(ScrewStep, self).onExit(goingTo, transitionType)


    def doStepProcessing(self):

        print('Done')

from __main__ import vtk, qt, ctk, slicer

import PedicleScrewSimulatorWizard

#
# Pedicle Screw Simulator
#

class PedicleScrewSimulator:
  def __init__(self, parent):
    parent.title = "Pedicle Screw Simulator"
    parent.categories = ["Simulation"]
    parent.dependencies = []
    parent.contributors = ["Brendan Polley (University of Toronto)",
                           "Stewart McLachlin (Sunnybrook Research Institute)",
                           "Cari Whyne (Sunnybrook Research Institute)"] # replace with "Firstname Lastname (Org)"
    parent.helpText = """
    Pedicle Screw Simulator
    """
    parent.acknowledgementText = """
    """ # replace with organization, grant and thanks.
    self.parent = parent

#
# qSpineGeneratorWidget
#

class PedicleScrewSimulatorWidget:
  def __init__( self, parent=None ):
    print "running pedicle sim v5"  
    if not parent:
      self.parent = slicer.qMRMLWidget()
      self.parent.setLayout( qt.QVBoxLayout() )
      self.parent.setMRMLScene( slicer.mrmlScene )
    else:
      self.parent = parent
    self.layout = self.parent.layout()

    if not parent:
      self.setup()
      self.parent.show()
      
    if slicer.mrmlScene.GetTagByClassName( "vtkMRMLScriptedModuleNode" ) != 'ScriptedModule':
      slicer.mrmlScene.RegisterNodeClass(vtkMRMLScriptedModuleNode())

  def setup( self ):
    '''
    Create and start the workflow.
    '''
    self.workflow = ctk.ctkWorkflow()

    workflowWidget = ctk.ctkWorkflowStackedWidget()
    workflowWidget.setWorkflow( self.workflow )

    # create all wizard steps
    self.loginStep = PedicleScrewSimulatorWizard.LoginStep( 'Login'  )
    self.approachStep = PedicleScrewSimulatorWizard.ApproachStep( 'Approach'  )
    #self.landmarksStep = PedicleScrewSimulatorWizard.LandmarksStep( 'Landmarks'  )
    self.screwStep = PedicleScrewSimulatorWizard.ScrewStep( 'Screw' )
    self.gradeStep = PedicleScrewSimulatorWizard.GradeStep( 'Grade' )
    self.endStep = PedicleScrewSimulatorWizard.EndStep( 'Final'  )
    
    # add the wizard steps to an array for convenience
    allSteps = []

    allSteps.append( self.loginStep )
    allSteps.append( self.approachStep )
    #allSteps.append( self.landmarksStep)
    allSteps.append( self.screwStep)
    allSteps.append( self.gradeStep)
    allSteps.append( self.endStep )
    
    
    # Add transition 
    # Check if volume is loaded
    self.workflow.addTransition( self.loginStep, self.approachStep, None, ctk.ctkWorkflow.Forward )
    
    self.workflow.addTransition( self.approachStep, self.screwStep, 'pass', ctk.ctkWorkflow.Forward )
    self.workflow.addTransition( self.approachStep, self.loginStep, 'fail', ctk.ctkWorkflow.Forward  )
    
    #self.workflow.addTransition( self.landmarksStep, self.screwStep, 'pass', ctk.ctkWorkflow.Bidirectional )
    #self.workflow.addTransition( self.landmarksStep, self.approachStep, 'fail', ctk.ctkWorkflow.Bidirectional  )
    
    self.workflow.addTransition( self.screwStep, self.gradeStep, 'pass', ctk.ctkWorkflow.Forward )
    self.workflow.addTransition( self.screwStep, self.approachStep, 'fail', ctk.ctkWorkflow.Forward )
          
    self.workflow.addTransition( self.gradeStep, self.endStep, None, ctk.ctkWorkflow.Forward )
           
    nNodes = slicer.mrmlScene.GetNumberOfNodesByClass('vtkMRMLScriptedModuleNode')

    self.parameterNode = None
    for n in xrange(nNodes):
      compNode = slicer.mrmlScene.GetNthNodeByClass(n, 'vtkMRMLScriptedModuleNode')
      nodeid = None
      if compNode.GetModuleName() == 'PedicleScrewSimulator':
        self.parameterNode = compNode
        print 'Found existing PedicleScrewSimulator parameter node'
        break
    if self.parameterNode == None:
      self.parameterNode = slicer.vtkMRMLScriptedModuleNode()
      self.parameterNode.SetModuleName('PedicleScrewSimulator')
      slicer.mrmlScene.AddNode(self.parameterNode)
 
    for s in allSteps:
        s.setParameterNode (self.parameterNode)
    
    
    # restore workflow step
    currentStep = self.parameterNode.GetParameter('currentStep')
    
    if currentStep != '':
      print 'Restoring workflow step to ', currentStep
      if currentStep == 'Login':
        self.workflow.setInitialStep(self.loginStep)
      if currentStep == 'Approach':
        self.workflow.setInitialStep(self.approachStep) 
      #if currentStep == 'Landmarks':
        #self.workflow.setInitialStep(self.landmarksStep)    
      if currentStep == 'Screw':
        self.workflow.setInitialStep(self.screwStep) 
      if currentStep == 'Grade':
        self.workflow.setInitialStep(self.gradeStep)   
      if currentStep == 'Final':
        self.workflow.setInitialStep(self.endStep)
    else:
      print 'currentStep in parameter node is empty!'
    
    
    # start the workflow and show the widget
    self.workflow.start()
    workflowWidget.visible = True
    self.layout.addWidget( workflowWidget )

    # compress the layout
      #self.layout.addStretch(1)        
 
  def enter(self):
    print "PedicleScrewSimulator: enter() called"

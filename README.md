PedicleScrewSimulator
=====================

This is a 3D Slicer module for pedicle screw insertion training.

Installing the PedicleScrewSimulator
------------------------------------

1. Clone this repository
2. Open Slicer --> Edit --> Application Settings --> add the module path (../pediclescrewsimulator/PedicleScrewSimulator)
  * the path should point to the pediclescrewsimulator.py file
3. Restart Slicer

This module has been tested against Slicer V4.6 to V4.8.1
This module has been tested against Slicer V4.9.0 and functions, but as of May 2018, this nightly build is unstable and not recommended.

### Known bugs include the following:
* bidirectional workflow does not work (back buttons have been grayed out)
* LoginStep.py --> replace izip_longest with zip_longest if Slicer ever updates to Python 3.x
* ScrewStep.py --> confirmDialog --> breaks on hitting ok before the screw is fully driven in (warning text implemented)
* ScrewStep.py --> transformScrewComposite --> indexing error, doesn't seem critical, currently passing the exception
* ScrewStep.py --> "remove entry point" breaks functionality

Using the PedicleScrewSimulator
-------------------------------

The easiest way to use this module is to load the sample data (CTA Abdomen (sample data)) and follow the workflow on this volume. However, any .nrrd file will work.

There are essentially two workflows:

  1. Saving a new case
    * input a "CSV File Write Directory" where the module will write a CSV containing useful parameters for potential future loading.
    * this is the route you should take for any new patient volume (loaded through Slicer's native functions).
  2. Loading a previous case
    * input a "NRRD Volumes Directory" where the module will search for appropriately nammed .nrrd volumes.
    * input a "Previously Saved Case" which is a .csv file containing useful parameters for loading.

### The next steps are as follows:
1. Select Vertebrae Labels
2. Edit the Cortical Bone Threshold
3. Add Entry Points  
     Edit the trajectories  
     Insert Screw
4. Update Screw Diameters and Lengths if desired  
     Hit "Grade Current Sizes" to generate a .csv file  
     Hit "Next" to save the parameters of the last step